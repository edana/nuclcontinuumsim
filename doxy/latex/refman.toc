\contentsline {section}{\numberline {1}Main Page}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Inputs}{1}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Pace2}{1}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Gammapace}{2}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}Gamble and enerord}{3}{subsubsection.1.1.3}
\contentsline {subsubsection}{\numberline {1.1.4}Gasp}{5}{subsubsection.1.1.4}
\contentsline {subsection}{\numberline {1.2}Analysis}{6}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}The EMHK file}{6}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Matrix and projections}{6}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}(I,E) selection}{7}{subsubsection.1.2.3}
\contentsline {section}{\numberline {2}Class Documentation}{7}{section.2}
\contentsline {subsection}{\numberline {2.1}ILUnfoldingProcedure Class Reference}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}MatrixPrint Class Reference}{7}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Detailed Description}{7}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Member Function Documentation}{8}{subsubsection.2.2.2}
\contentsline {paragraph}{\numberline {2.2.2.1}print2D}{8}{paragraph.2.2.2.1}
\contentsline {subsection}{\numberline {2.3}MomentsDist Class Reference}{8}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}MomentsDistNormal Class Reference}{8}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}MtxNorm Class Reference}{9}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}ReadEventFile Class Reference}{9}{subsection.2.6}
\contentsline {subsubsection}{\numberline {2.6.1}Detailed Description}{9}{subsubsection.2.6.1}
\contentsline {subsubsection}{\numberline {2.6.2}Member Function Documentation}{9}{subsubsection.2.6.2}
\contentsline {paragraph}{\numberline {2.6.2.1}EventAnalysis}{9}{paragraph.2.6.2.1}
\contentsline {paragraph}{\numberline {2.6.2.2}NeutronEventAnalysis}{11}{paragraph.2.6.2.2}
\contentsline {paragraph}{\numberline {2.6.2.3}search\_\discretionary {-}{}{}}{11}{paragraph.2.6.2.3}
\contentsline {subsection}{\numberline {2.7}RegionHK Class Reference}{11}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}RegionHk Class Reference}{11}{subsection.2.8}
\contentsline {subsubsection}{\numberline {2.8.1}Detailed Description}{12}{subsubsection.2.8.1}
\contentsline {section}{\numberline {3}File Documentation}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Analysis/trunk/main.cpp File Reference}{12}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Detailed Description}{12}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Function Documentation}{12}{subsubsection.3.1.2}
\contentsline {paragraph}{\numberline {3.1.2.1}main}{12}{paragraph.3.1.2.1}
\contentsline {subsection}{\numberline {3.2}Analysis/trunk/MatrixPrint.cpp File Reference}{13}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Detailed Description}{13}{subsubsection.3.2.1}
\contentsline {subsection}{\numberline {3.3}Analysis/trunk/MatrixPrint.h File Reference}{13}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Detailed Description}{13}{subsubsection.3.3.1}
\contentsline {subsection}{\numberline {3.4}Analysis/trunk/ReadEventFile.cpp File Reference}{13}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Detailed Description}{14}{subsubsection.3.4.1}
\contentsline {subsection}{\numberline {3.5}Analysis/trunk/ReadEventFile.h File Reference}{14}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Detailed Description}{14}{subsubsection.3.5.1}
\contentsline {subsection}{\numberline {3.6}Analysis/trunk/Region.cpp File Reference}{14}{subsection.3.6}
\contentsline {subsubsection}{\numberline {3.6.1}Detailed Description}{14}{subsubsection.3.6.1}
\contentsline {subsection}{\numberline {3.7}Analysis/trunk/Region.h File Reference}{15}{subsection.3.7}
\contentsline {subsubsection}{\numberline {3.7.1}Detailed Description}{15}{subsubsection.3.7.1}
\contentsline {subsection}{\numberline {3.8}IO\_\discretionary {-}{}{}files/154Dy\_\discretionary {-}{}{}148MeV\_\discretionary {-}{}{}gammapace.out File Reference}{15}{subsection.3.8}
\contentsline {subsubsection}{\numberline {3.8.1}Detailed Description}{15}{subsubsection.3.8.1}
\contentsline {subsection}{\numberline {3.9}IO\_\discretionary {-}{}{}files/154Dy\_\discretionary {-}{}{}148MeV\_\discretionary {-}{}{}pace.out File Reference}{17}{subsection.3.9}
\contentsline {subsubsection}{\numberline {3.9.1}Detailed Description}{18}{subsubsection.3.9.1}
