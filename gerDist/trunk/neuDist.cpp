/************************************************************************************/
/****** This program generates a list of neutron following a Maxwell-Bolzmann  ******/
/****** reading the energies of the entry states from the IE.dat file.         ******/
/************************************************************************************/

#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<cstdlib>
#include<cmath>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int factorial(int);

main(){

  ofstream outFile("outDist.dat", ios::out);
  ifstream IEFile("IE.dat", ios::in);
  int evnt=0, i=0, I=0, EENC=31789, evntMax=0,n=4, l=0, j=0;
  double Fc=0, vFc=0.99, x=0, E=0, Edif=0;
  double c=0.345698, a=1.36019, h=1.05482;
  srand ( time(NULL) );
  double *nArr;
 
  cout << "Set the number of events (wc IE.dat)" << endl;
  cin >> evntMax;

  cout << "Set the maximum excitation energy of the recoil nucleus (keV):" << endl;
  cin >> EENC;

  cout << "Set the number of neutrons:" << endl;
  cin >> n;
  n=n-1;
  nArr=new double [n];

  evntMax=evntMax*(n+1);

  for(i=0;i<=n;i++) 
    nArr[i]=0;
  
  while(IEFile) {
    IEFile >> I >> E;
    l++;
    if(E>=EENC)
      cout << "ERROR " << E << endl; 
    printf("num# %d\r",l);
    for(i=0;i<n;i++) {
      Fc=0;
      x=0;
      vFc=0.99; 
      while(vFc>0.95)
	vFc=(double)(rand()%100)/(double)100;
      while(Fc<vFc) {
	if(x<0.35)
	  Fc+=0.465939*x*0.01;
	else
	  Fc+=h/pow(a,1.5)*sqrt(x-c)*exp(-(x-c)/a)*0.01;
     	x+=0.01;
      }

      nArr[i]=x*1000;      
      
      Edif=EENC-E;
      for(j=0;j<=n;j++)
	Edif=Edif-nArr[j];
      if(i==(n-1)){
	if(Edif<=0) 
	  i--;
	else{
	  nArr[n]=Edif;
	  i++;
	}
      }
      if(Edif<=0) {
	nArr[i]=0;
      }
    }
    evnt+=(n+1);
    if(evnt<=evntMax) {
      for(j=0;j<=n;j++) 
	outFile << evnt-n+j << "\t" << nArr[j] << endl;
    }
    for(i=0;i<=n;i++)
      nArr[i]=0;
  }
  outFile.close();
  IEFile.close();
}


