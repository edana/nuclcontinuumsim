#include <string> 
using std::string; 
#include<cmath>
//#include<iostream>
//using std::cerr;
//using std::cout;
//using std::cin;
//using std::endl;
//using std::ios;
#include<fstream>
using std::ifstream;
using std::ofstream;
#include <iomanip>
using std::setprecision;

#include "matrix.h"

#ifndef _NO_NAMESPACE 
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()	try {
#  define CATCHERROR()	} catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif

typedef matrix<double> Matrix;

class Fit
{
 public:
  Fit();
  void ellipseFit(double *, double *);

 private:
  double X[100], Y[100]; 
};

Fit::Fit() 
{
}

void Fit::ellipseFit(double *X, double *Y)
{

  double sumX=0,sumY=0;
  double theta=90;
  int iter=0;
  int i=0,n=0, m=0;
  double *px, *py, *xT, *yT, *centX, *centY, Xc=0, Yc=0;
  double Qdf1dx=0, Qdf1dy=0, Qdf2dx=0, Qdf2dy=0;
  double *OCPX, *OCPY, *OCPx, *OCPy;
  
  double a=1, b=2;
  double xk1=0, yk1=0, xk2=0, yk2=0, x=0, y=0;
  double ymin=100, xmin=100;
  double f1=0, f2=0;
  
  theta=theta * 3.141592/180.;

  ofstream outF("fitLPS.gp",ios::out);
  ofstream outF7("ellipse.gnu",ios::out);
  ofstream parameters("parameters.dat",ios::out);


  for(i=0;i<100;i++)
    {
      if(X[i]!=0 || Y[i]!=0)
	{
	  n++;
	}
    }
  xT=new double [n];
  yT=new double [n];

  centX=new double [n];
  centY=new double [n];

  px=new double [n];
  py=new double [n];

  OCPX=new double [n];
  OCPY=new double [n];

  OCPx=new double [n];
  OCPy=new double [n];
  
  //Search the geometrical center, calculating Mean 
  for(i=0; i<n; i++)
    {
      sumX+=X[i];
      sumY+=Y[i];
    }
  Xc=sumX/double(n);
  Yc=sumY/double(n);
  
  //Translate the center to the origin =
  // translate the data points to the center of the 'xy' system
  for(i=0;i<n;i++)
    {
      xT[i]=(X[i]-Xc);
      yT[i]=(Y[i]-Yc);
    }

  // Rotate the data points (-theta) degrees =
  // this completes placing the points in the 'xy' system
  // i.e., (px[i],py[i])= (x_i,y_i) of Fig.3 in Ahn's paper (2001) 
  for(i=0;i<n;i++)
    {
      px[i] = xT[i]*cos(-theta) - yT[i]*sin(-theta);
      py[i] = xT[i]*sin(-theta) + yT[i]*cos(-theta);
    }
  
  if(n!=0)
    {       
      Matrix Q(2,2);
      Matrix deltaX(2,1);
      Matrix f(2,1);

      // fixing initial a,b 
      for(i=0;i<n;i++)
	{
	  if(fabs(px[i])<fabs(xmin))
	    {
	      xmin=px[i];
	      b=fabs(py[i]);
	      //cout << "i=" << i << "  b=" << b << endl;
	    }
	  if(fabs(py[i])<fabs(ymin))
	    {	
	      ymin=py[i];
	      a=fabs(px[i]);
	    }
	}
      
      //cout << "a=" << a << "\t b=" << b << endl;

      // Initial parameters from arithmetical fitting: (a,b,Xc,Yc) 
      // and proposed theta
      
      outF << "set size square" << endl
	   << "unset key" << endl
	   << "set parametric" << endl
	   << "x(t)=Xc+a*cos(t)*cos(theta)-b*sin(t)*sin(theta)" << endl
	   << "y(t)=Yc+b*sin(t)*cos(theta)+a*cos(t)*sin(theta)" << endl
	   << "x1(t)=Xc1+a1*cos(t)*cos(theta1)-b1*sin(t)*sin(theta1)" << endl
	   << "y1(t)=Yc1+b1*sin(t)*cos(theta1)+a1*cos(t)*sin(theta1)" << endl;
      outF << "Xc=" << Xc << endl
	   << "Yc=" << Yc << endl
	   << "a=" << a << endl
	   << "b=" << b << endl
	   << "theta=" << theta*180/3.1415 << "*pi/180" << endl << endl;
      
      outF7 << "set size square" << endl
	    << "unset key" << endl
	    << "set parametric" << endl
	    << "x(t)=Xc+a*cos(t)*cos(theta)-b*sin(t)*sin(theta)" << endl
	    << "y(t)=Yc+b*sin(t)*cos(theta)+a*cos(t)*sin(theta)" << endl;
      
      outF7 << "Xc=" << Xc << endl
	    << "Yc=" << Yc << endl
	    << "a=" << a << endl
	    << "b=" << b << endl
	    << "theta=" << theta*180/3.1415 << "*pi/180" << endl << endl;
      
      double A_min=9999.;
      float Xc_min, Yc_min, a_min, b_min, theta_min;
      int iter_min=0;
	
      double delta_Xc=0, delta_Yc=0, delta_a=0, delta_b=0, delta_theta=0;
      int l=0, j=0;
      int m=2*n;

      Matrix R(2,2);
      Matrix B1(2,1);
      Matrix B2(2,1);
      Matrix B3(2,1);
      Matrix B4(2,1);
      Matrix B5(2,1);
      Matrix j1(2,1);
      Matrix j2(2,1);
      Matrix j3(2,1);
      Matrix j4(2,1);
      Matrix j5(2,1);
      Matrix delta(5,1);	      
      Matrix error(m,1);
      Matrix jac(m,5);

      for(i=0;i<5;i++)
	delta(i,0)=0;
      
      for(i=0;i<m;i++)
	error(i,0)=0;

      for(i=0;i<m;i++)
	{
	  for(j=0;j<5;j++)
	    {
	      jac(i,j)=0;
	    }
	}
      
      //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      // The iteration procedure starts here
      //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      for(int k=0;k<50;k++){ 

	//Translate the center to the origin =
	// translate the data points to the center of the 'xy' system
	for(i=0;i<n;i++)
	  {
	    xT[i]=(X[i]-Xc);
	    yT[i]=(Y[i]-Yc);
	  }

	// Rotate the data points -theta degrees =
	// this completes placing the points in the 'xy' system
	for(i=0;i<n;i++)
	  {
	    px[i] = xT[i]*cos(-theta) - yT[i]*sin(-theta);
	    py[i] = xT[i]*sin(-theta) + yT[i]*cos(-theta);
	  }
	
	// fixing initial points for generalized Newton to find orthogonal
	// contacting points OCP_i = (x'_i,y'_i)
	for(int j=0;j<n;j++)
	  {
	    //Finding \vec{x}_k1 for each point
	    xk1=px[j]*a*b/(pow(b*b*px[j]*px[j]+a*a*py[j]*py[j],0.5));
	    yk1=py[j]*a*b/(pow(b*b*px[j]*px[j]+a*a*py[j]*py[j],0.5));
	    
	    //Finding \vec{x}_k2 for each point
	    if(fabs(px[j])<a)
	      {
		xk2=px[j];
		if(py[j]<0)
		  yk2=-b/a*pow(a*a-px[j]*px[j],0.5);
		else
		  yk2=b/a*pow(a*a-px[j]*px[j],0.5);
	      }
	    else
	      {
		if(px[j]<0)
		  xk2=-a;
		else
		  xk2=a;
		yk2=0;
	      }
	    // These are the first (in each iteration) approximation to the 
	    // OCP -> (x,y)
	    x=0.5*(xk1+xk2);
	    y=0.5*(yk1+yk2); 

	    //Finding (xk,y)=OCP in xy system (Generalized Newton method)
	    for(int k=0;k<10;k++)
	      {
		Qdf1dx=b*b*x;
		Qdf1dy=a*a*y;
		Qdf2dx=(a*a-b*b)*y+b*b*py[j];
		Qdf2dy=(a*a-b*b)*x-a*a*px[j];	   
		f1=0.5*(a*a*y*y+b*b*x*x-a*a*b*b);
		f2=b*b*x*(py[j]-y)-a*a*y*(px[j]-x);
		
		Q(0,0)=Qdf1dx;
		Q(0,1)=Qdf1dy;
		Q(1,0)=Qdf2dx;
		Q(1,1)=Qdf2dy;
		
		f(0,0)=f1;
		f(1,0)=f2;
		
		deltaX=-!(~Q*Q)*(~Q*f);
		
		if(fabs(deltaX(0,0))<0.001 && fabs(deltaX(1,0))<0.001)
		  goto ok;
		
		x+=deltaX(0,0);
		y+=deltaX(1,0);
	      }
	  ok:
	    
	    //Orthogonal contacting points in xy system = (x_i',y_i') = (x,y)
	    // in eqs. (26). (x,y) is the result of the iterative algorithm
	    // eqss. (27,28)
	    OCPx[j]=x;
	    OCPy[j]=y;

	    //Rotate and move back to the original position
	    OCPX[j] = x*cos(theta) - y*sin(theta) + Xc;
	    OCPY[j] = x*sin(theta) + y*cos(theta) + Yc;
	  }
	
	TRYBEGIN()
	  {	     	    
	    l=0;
	    for(i=0;i<n;i++)
	      {
		error(l,0)=X[i]-OCPX[i];
		l++;
		error(l,0)=Y[i]-OCPY[i];
		l++;
	      }
	  }
	CATCHERROR();
	
	l=0;     
	for(i=0;i<n;i++)
	  {
	    x=OCPx[i];
	    y=OCPy[i];
	    
	    TRYBEGIN()
	      {
		R(0,0)=cos(theta);
		R(0,1)=sin(theta);
		R(1,0)=-sin(theta);
		R(1,1)=cos(theta);
		
		Q(0,0)=b*b*x;
		Q(0,1)=a*a*y;		  
		Q(1,0)=(a*a-b*b)*y+b*b*py[i];
		Q(1,1)=(a*a-b*b)*x-a*a*px[i];    
		
		B1(0,0)=b*b*x*cos(theta)-a*a*y*sin(theta);
		B1(1,0)=b*b*(py[i]-y)*cos(theta)+a*a*(px[i]-x)*sin(theta);
		B2(0,0)=b*b*x*sin(theta)+a*a*y*cos(theta);
		B2(1,0)=b*b*(py[i]-y)*sin(theta)-a*a*(px[i]-x)*cos(theta);
		B3(0,0)=a*(b*b-y*y);
		B3(1,0)=2*a*y*(px[i]-x);
		B4(0,0)=b*(a*a-x*x);
		B4(1,0)=-2*b*x*(py[i]-y);
		B5(0,0)=(a*a-b*b)*x*y;
		B5(1,0)=(a*a-b*b)*(x*x-y*y-x*px[i]+y*py[i]);
		
		j1=!R*!Q*B1;
		j2=!R*!Q*B2;
		j3=!R*!Q*B3;
		j4=!R*!Q*B4;
		j5=!R*!Q*B5;
		
		jac(l,0)=j1(0,0);
		jac(l,1)=j2(0,0);
		jac(l,2)=j3(0,0);
		jac(l,3)=j4(0,0);
		jac(l,4)=j5(0,0);
		l++;
		jac(l,0)=j1(1,0);
		jac(l,1)=j2(1,0);
		jac(l,2)=j3(1,0);
		jac(l,3)=j4(1,0);
		jac(l,4)=j5(1,0);
		l++;
	      }
	    CATCHERROR();	      
	  }
	
	TRYBEGIN()
	  {
	    delta=!(~jac*jac)*(~jac*error);
	  }
	CATCHERROR();
	
	iter++;
	
	double A = delta.Norm();
	//cout << "The norm of the parameter vector is: " << A << endl;
	
	if (A<A_min){
	  iter_min = iter-1;
	  A_min=A;
	  Xc_min=Xc;
	  Yc_min=Yc;
	  a_min=a;
	  b_min=b;
	  theta_min=theta;
	}
	
	if (A<0.001)
	  goto end;
	
	Xc+=delta(0,0);
	Yc+=delta(1,0);
	a+=delta(2,0);
	b+=delta(3,0);
	theta+=delta(4,0);
	
	parameters << iter << "\t" << A << "\t" << Xc << "\t" << Yc << "\t" 
		   << a  << "\t" << b << "\t" << theta*180/3.1416 << endl;
	if (k<10){
	  outF7 << "Xc" << iter << "=" << Xc << endl
		<< "Yc" << iter << "=" << Yc << endl
		<< "a" << iter << "=" << a << endl
		<< "b" << iter << "=" << b << endl
		<< "theta" << iter << "=" << theta*180/3.1415 << "*pi/180" 
		<< endl << endl;
	  
	  outF7 << "x" << iter << "(t)=Xc" << iter << "+a" << iter 
		<< "*cos(t)*cos(theta" << iter << ")-b" << iter 
		<< "*sin(t)*sin(theta" << iter << ")" << endl;
	  
	  outF7 << "y" << iter << "(t)=Yc" << iter << "+b" << iter 
		<< "*sin(t)*cos(theta" << iter << ")+a" << iter 
		<< "*cos(t)*sin(theta" << iter 
		<< ")" << endl; 
	}
      }
    end:
      
      cout.precision(3);
      cout << endl;
      cout << "   -- results for minimum ---" << endl;
      cout << "  A_min= " << A_min << "  at iteration " << iter_min +1 << endl;
      
      
      cout << "    Xc_min=" << Xc_min << endl
	   << "    Yc_min=" << Yc_min << endl
	   << "    a_min=" << a_min << endl
	   << "    b_min=" << b_min << endl
	   << "    theta_min=" << theta_min*180/3.1415 << "*pi/180" 
	   << endl << endl;

      outF << "Xc1=" << Xc_min << endl
	   << "Yc1=" << Yc_min << endl
	   << "a1=" << a_min << endl
	   << "b1=" << b_min << endl
	   << "theta1=" << theta_min*180/3.1415 << "*pi/180" 
	   << endl << endl;
      
      outF << "plot [0:2*pi] \\" << endl;
      outF << "x(t),y(t) lt -1, \\" << endl;
      outF << "x1(t),y1(t) lt 1, \\" << endl;
      outF << "'3.dat' w p lt 1 pt 7 ps 1.5" << endl;
      
      //%%%%%%%%%%%%%%%%%%%
      outF7 << "set autoscale " << endl;
      outF7 << "plot [0:2*pi] \\" << endl;
      outF7 << "x(t),y(t) lt -1, \\" << endl;
      outF7 << "x1(t),y1(t) lt 1, \\" << endl;
      outF7 << "x2(t),y2(t), \\" << endl;
      outF7 << "x3(t),y3(t), \\" << endl;
      outF7 << "x4(t),y4(t), \\" << endl;
      outF7 << "x5(t),y5(t), \\" << endl;
      outF7 << "x6(t),y6(t), \\" << endl;
      outF7 << "x7(t),y7(t), \\" << endl;
      outF7 << "x8(t),y8(t), \\" << endl;
      outF7 << "x9(t),y9(t), \\" << endl;
      outF7 << "x10(t),y10(t), \\" << endl;
      outF7 << "'3.dat' w p lt 1 pt 7 ps 1.5" << endl;

      delete [] xT;
      delete [] yT;
      delete [] px;
      delete [] py;
    }  
}


main()
{
  double X[100], Y[100];
  for(int i=0;i<100;i++)
    {
      X[i]=0;
      Y[i]=0;
    }
  ifstream data("3.dat", ios::in);
  int j=0;
  while(data)
    {
      data >> X[j] >> Y[j];
      j++;
    }
  data.close(); 
  Fit elFit;
  elFit.ellipseFit(X,Y);
}

