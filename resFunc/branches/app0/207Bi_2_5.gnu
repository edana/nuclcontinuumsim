set table "207Bi_2_5.tbl"
set contour base
unset surface
set cntrparam levels discrete 846,752, 658,564, 470,376, 282,188, 94
splot "207Bi_2_5.mtx" matrix w l
