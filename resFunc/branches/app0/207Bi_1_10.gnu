set table "207Bi_1_10.tbl"
set contour base
unset surface
set cntrparam levels discrete 180,160, 140,120, 100,80, 60,40, 20
splot "207Bi_1_10.mtx" matrix w l
