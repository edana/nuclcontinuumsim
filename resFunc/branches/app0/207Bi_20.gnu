set table "207Bi_20.tbl"
set contour base
unset surface
set cntrparam levels discrete 85,76,66,57,47,38,29,19,10
splot "207Bi_20.mtx" matrix w l
