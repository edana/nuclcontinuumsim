set table "207Bi_1_5.tbl"
set contour base
unset surface
set cntrparam levels discrete 155,138, 121,104, 86,69, 52,35, 18
splot "207Bi_1_5.mtx" matrix w l
