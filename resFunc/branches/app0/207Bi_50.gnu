set table "207Bi_50.tbl"
set contour base
unset surface
set cntrparam levels discrete 18,16,14,12,10,8,6,4,2
splot "207Bi_50.mtx" matrix w l
