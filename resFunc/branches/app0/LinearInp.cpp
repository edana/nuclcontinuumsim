/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed; 

#include<fstream>
using std::ifstream;
using std::ofstream;

#include <string> 
using std::string; 

#include <sstream> 
using std::ostringstream;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include <iomanip> 
using std::setw; 
using std::setprecision; 
using std::showpoint; 

#include<stdlib.h>
#include<cstdlib>

#include<cmath>

#include "LinearInp.h"

using namespace std;

LinearInp::LinearInp() 
{
}

float LinearInp::interpolation(float p1, float ener1, float p2, float ener2, float ener)
{
  m=(p1-p2)/(ener1-ener2);
  b=p1-m*ener1;
  float p;
  p=m*ener+b;
  return p;
}

void LinearInp::contour(string st, int maxi)
{
  int ten=(int)(maxi-maxi*10/100);
  int twenty=(int)(maxi-maxi*20/100);
  int thirty=(int)(maxi-maxi*30/100);
  int forty=(int)(maxi-maxi*40/100);
  int fifhty=(int)(maxi-maxi*50/100);
  int sixty=(int)(maxi-maxi*60/100);
  int seventy=(int)(maxi-maxi*70/100);
  int eighty=(int)(maxi-maxi*80/100);
  int ninety=(int)(maxi-maxi*90/100);
 
  //Set the table file name just fot the gnuplot macro
  ostringstream tableBigName;
  string tableExt( ".tbl" ); 
  tableBigName << st << tableExt;
  string tableFile=tableBigName.str();

  //Set the matrix file name just fot the gnuplot macro
  ostringstream matrixBigName;
  string matrixExt( ".mtx" ); 
  matrixBigName << st << matrixExt;
  string matrixFile=matrixBigName.str();

  //Write the macro file for gnuplot 
  ostringstream gnuBigName;
  string gnuExt( ".gnu" ); 
  gnuBigName << st << gnuExt;
  string gnuFile=gnuBigName.str();
  ofstream gnuFileV(gnuFile.c_str(), ios::out);

  gnuFileV << "set table \"" << tableFile << "\"" << endl
	   << "set contour base" << endl 
	   << "unset surface" << endl
	   << "set cntrparam levels discrete " 
	   << ten << "," << twenty << ", " << thirty << "," << forty << ", " 
	   << fifhty << "," << sixty << ", "<< seventy << "," << eighty << ", " 
	   << ninety << endl
	   << "splot \"" << matrixFile << "\" matrix w l" << endl;

  //Execute the gnuplot macro file
  ostringstream gnuCommand;
  string gnu("gnuplot ");
  gnuCommand << gnu << st << gnuExt;
  string command(gnuCommand.str());
  int ret = system(command.c_str());
  gnuFileV.close();
}


void LinearInp::contourDouble(string st, double maxi)
{
  double ten=(maxi-maxi*10/100);
  double twenty=(maxi-maxi*20/100);
  double thirty=(maxi-maxi*30/100);
  double forty=(maxi-maxi*40/100);
  double fifhty=(maxi-maxi*50/100);
  double sixty=(maxi-maxi*60/100);
  double seventy=(maxi-maxi*70/100);
  double eighty=(maxi-maxi*80/100);
  double ninety=(maxi-maxi*90/100);
 
  //Set the table file name just fot the gnuplot macro
  ostringstream tableBigName;
  string tableExt( ".tbl" ); 
  tableBigName << st << tableExt;
  string tableFile=tableBigName.str();

  //Set the matrix file name just fot the gnuplot macro
  ostringstream matrixBigName;
  string matrixExt( ".mtx" ); 
  matrixBigName << st << matrixExt;
  string matrixFile=matrixBigName.str();

  //Write the macro file for gnuplot 
  ostringstream gnuBigName;
  string gnuExt( ".gnu" ); 
  gnuBigName << st << gnuExt;
  string gnuFile=gnuBigName.str();
  ofstream gnuFileV(gnuFile.c_str(), ios::out);

  gnuFileV << "set table \"" << tableFile << "\"" << endl
	   << "set contour base" << endl 
	   << "unset surface" << endl
	   << "set cntrparam levels discrete " 
	   << ten << "," << thirty << "," 
	   << fifhty << "," << seventy << "," 
	   << ninety << endl
	   << "splot \"" << matrixFile << "\" matrix w l" << endl;

  //Execute the gnuplot macro file
  ostringstream gnuCommand;
  string gnu("gnuplot ");
  gnuCommand << gnu << st << gnuExt;
  string command(gnuCommand.str());
  int ret = system(command.c_str());
  gnuFileV.close();
}

void  LinearInp::ReadCntrTable(ifstream& table1, int *cent,ofstream& table2)
{
  char c[35];
  double xa,ya,za,x2,y2,z2,x1,y1,z1,x,y,z;
  double m,b,m1,b1,m2,b2,t,c1,c2,c3;
  double xe1,xe2,ye1,ye2,phi,t_adj,cp;
  float angle=0, angle1=0;
  long pos;
  int p=0, iter=0, cnt_num=0;
  double X[185],Y[185],Z[185], lparam[5], ell1x[360], ell1y[360], ell2x[360], ell2y[360];
  int i=0,j=0,n=0, cntr=0;
  int k1=cent[0];
  int H1=cent[1];

  ofstream outF("ellipse.gnu",ios::out);

  for(i=0;i<=185;i++)
    {
      X[i]=0;
      Y[i]=0;
      Z[i]=0;
    }
    
   x2=y2=z2=x1=y1=z1=x=y=z=0;
  
  //Set the center of the new matrix
    
   outF << "#set size square" << endl
	<< "unset key" << endl
	<< "set parametric" << endl
	<< "set xlabel 'k'" << endl
	<< "set ylabel 'H(MeV)'" << endl
	<< "set term postscript eps color solid enhanced 'Helvetica' 21" << endl
	<< "set out 'app0_60Co_2_5.ps" << endl;
   
  while(table1)
    {
      table1.getline(c,35);
      table1 >> c;
      pos=table1.tellg();
      if(c[0]=='#')
	{
	  //cout << c << endl;
	  n=0;
	  angle1=0;
	  xa=ya=za=x2=y2=z2=x1=y1=z1=x=y=z=0;
	  if(X[0]!=0&&Y[0]!=0)
	    {
	      //Fit each contour to an ellipse
	      param=ellipseFit(X,Y,Z);
	      cnt_num++;

	      //Finding the intersection point of two ellipses in order to avoid overlaping
	      if(cnt_num>1)
		{
		  int g=0;
		  //while(g==0)
		  //{
		  for(i=0;i<360;i++)
		    {
		      t_adj=i*3.14159/180;		      
		      ell1x[i]=lparam[0]+lparam[2]*cos(t_adj)*cos(lparam[4])-lparam[3]*sin(t_adj)*sin(lparam[4]);
		      ell1y[i]=lparam[1]+lparam[3]*sin(t_adj)*cos(lparam[4])+lparam[2]*cos(t_adj)*sin(lparam[4]);
		      ell2x[i]=param[0]+param[2]*cos(t_adj)*cos(param[4])-param[3]*sin(t_adj)*sin(param[4]);
		      ell2y[i]=param[1]+param[3]*sin(t_adj)*cos(param[4])+param[2]*cos(t_adj)*sin(param[4]);
		    }
		  for(i=0;i<360;i++)
		    {
		      for(j=0;j<360;j++)
			{
			  if(fabs(ell1x[i]-ell2x[j])<0.001&&fabs(ell1y[i]-ell2y[j])<0.001)
			    {
			      g++;
			      cp=j;
			    }
			}
		    }
		  if(g>1)
		    {
		      cout << "Overlaping in contour " <<  cnt_num << " angle " << cp << endl;
		      /*if(cp>0||cp<45||cp>135||cp<225||cp>315||cp<360)
			param[2]=param[2]-0.01;
			else
			param[3]=param[3]-0.01;*/
		      g=0;
		    }
		  else
		    g=1;
		  //}
		}
	      lparam[0]=param[0];
	      lparam[1]=param[1];
	      lparam[2]=param[2];
	      lparam[3]=param[3];
	      lparam[4]=param[4];
	      
	      if(cnt_num%2!=0)
	      {
		table2.precision(3);
		table2 << fixed << left << showpoint  
		       << setw( 10 ) << param[0]  
		       << setw( 10 ) << param[1] 
		       << setw( 10 ) << param[2]  
		       << setw( 10 ) << param[3] 
		       << setw( 10 ) << param[4] 
		       << setw( 10 ) << Z[0] << endl;  
		
		outF << "xc" << iter << "=" << param[0] << endl
		     << "yc" << iter << "=" << param[1] << endl
		     << "a" << iter << "=" << param[2] << endl
		     << "b" << iter << "=" << param[3] << endl
		     << "theta" << iter << "=" << param[4] << endl << endl;
		
		outF << "x" << iter << "(t)=xc" << iter << "+a" << iter 
		     << "*cos(t)*cos(theta" << iter << ")-b" << iter 
		     << "*sin(t)*sin(theta" << iter << ")" << endl;
		
		outF << "y" << iter << "(t)=yc" << iter << "+b" << iter 
		     << "*sin(t)*cos(theta" << iter << ")+a" << iter 
		     << "*cos(t)*sin(theta" << iter 
		     << ")" << endl; 	  
		
	      }		
	      iter++;
	      
	    }
	      
	  for(i=0;i<=185;i++)
	    {
	      X[i]=0;
	      Y[i]=0;
	      Z[i]=0;
	    }
	   cntr=0;
	}
      else//Reads the contour 
	{
	  for(i=0;i<10;i++)
	    if(c[i]!='\0')
	      pos--;
	    else
	      i=10;
	  table1.seekg(pos);
	  table1 >> x >> y >> z;
	  //Saving one contour into an array set
	  X[cntr]=x;
	  Y[cntr]=y;
	  Z[cntr]=z;
	  cntr++;
	}
    }
  
  if(X[0]!=0&&Y[0]!=0)
    {
      param=ellipseFit(X,Y,Z);
      
      table2.precision(3);
      table2 << fixed << left << showpoint  
	     << setw( 10 ) << param[0]  
	     << setw( 10 ) << param[1] 
	     << setw( 10 ) << param[2]  
	     << setw( 10 ) << param[3] 
	     << setw( 10 ) << param[4]         
	     << setw( 10 ) << Z[0] << endl;
  
      outF << "xc" << iter << "=" << param[0] << endl
	   << "yc" << iter << "=" << param[1] << endl
	   << "a" << iter << "=" << param[2] << endl
	   << "b" << iter << "=" << param[3] << endl
	   << "theta" << iter << "=" << param[4] << endl << endl;
      
      outF << "x" << iter << "(t)=xc" << iter << "+a" << iter 
	   << "*cos(t)*cos(theta" << iter << ")-b" << iter 
	   << "*sin(t)*sin(theta" << iter << ")" << endl;
      
      outF << "y" << iter << "(t)=yc" << iter << "+b" << iter 
	   << "*sin(t)*cos(theta" << iter << ")+a" << iter 
	   << "*cos(t)*sin(theta" << iter 
	   << ")" << endl; 
      
      
      outF << "plot [0:2*pi] '60Co_2_5.tbl' w lp lt 3 ps 2 pt 3, ";
    }
  for(i=0;i<iter;i+=2)
    outF << "x" << i << "(t),y" << i << "(t) lt 1 lw 4,";

  outF << "x" << iter << "(t),y" << iter << "(t) lt 1 lw 4" << endl
       << "set term x11";

  // table2 << cent[0] << "\t "<< cent[1] << "\t " << cent[2] << endl << endl; 
 
  outF.close();
}

void LinearInp::distributionFit(ifstream& input1,float e1, ifstream& input2, float e2, float eInt, int M, ofstream& matrixFile)
{
  double xc, yc, a, b ,theta;
  double xc1, yc1, a1, b1 ,theta1;
  double xc2, yc2, a2, b2 ,theta2;
  double t_intr, ellk[360], ellH[360], p[6];
  float H1, k1,H2,k2,H,k;
  float c1, c2, c, cp;
  int i=0,j=0,kmtx=0,Hmtx=0,cmtx=0;
  e1=e1/1000;
  e2=e2/1000;
  eInt=eInt/1000;
  int matrix[60][60];

  for(i=0;i<60;i++)
    for(j=0;j<60;j++)
      matrix[i][j]=0;

  for(i=0;i<360;i++)
    {
      ellk[i]=0;
      ellH[i]=0;
    }
  while(input1)
    {
      input1 >> xc1 >> yc1 >> a1 >> b1 >> theta1 >> c1;
      input2 >> xc2 >> yc2 >> a2 >> b2 >> theta2 >> c2;
      xc=interpolation(xc1, e1, xc2, e2, eInt);
      yc=interpolation(yc1, e1, yc2, e2, eInt);
      a=interpolation(a1, e1, a2, e2, eInt);
      b=interpolation(b1, e1, b2, e2, eInt);
      theta=interpolation(theta1, e1, theta2, e2, eInt);
      c=interpolation(c1, e1, c2, e2, eInt);

      p[0]=xc;
      p[1]=yc;
      p[2]=a;
      p[3]=b;
      p[4]=theta;
      p[5]=c;

      for(i=0;i<360;i++)
	{
	  t_intr=i*3.14159/180;		      
	  ellk[i]=xc+a*cos(t_intr)*cos(theta)-b*sin(t_intr)*sin(theta);
	  ellH[i]=yc+b*sin(t_intr)*cos(theta)+a*cos(t_intr)*sin(theta);
	}
      
      for(i=0;i<360;i++)
	{
	  Hmtx=(int)(rint(ellH[i]));
	  kmtx=(int)(rint(ellk[i]));
	  cmtx=(int)(rint(c));
	  
	  if(matrix[kmtx][Hmtx]==0)
	    matrix[kmtx][Hmtx]=cmtx;
	  else
	    {
	      cp=matrix[kmtx][Hmtx];
	      matrix[kmtx][Hmtx]=(int)(rint((cmtx+cp)/2));
	    }
	}
    }

  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	for(cp=0;cp<matrix[i][j];cp++)
	  matrixFile << j*1000 << "\t " << i << "\t 0 \t 0 \t 0 \t 0" <<endl;
    }
}

double * LinearInp::ellipseFit(double *x,double *y, double *z)
{
  double sumx=0,sumy=0, sumx2=0, sumxy=0, xc=0, yc=0, theta=0;
  int numx=0,numy=0,i=0,n=0;
  double *centx, *centy, *px, *py;
  double xkf=0, ykf=0, a=0, b=0, afit=0, bfit=0;
  double ymax=0, xmax=0;

  for(i=0;i<185;i++)
    {
      if(x[i]!=0)
	{
	  n++;
	}
    }
  
  if(n!=0)
    {  

      n=n-1; //Because we always put the first data at the end
  
      centx=new double [n];
      centy=new double [n];
      px=new double [n];
      py=new double [n];
      
      //Search the geometrical center, calculating Mean 
      //Fit a line to the points to calculate the angle
      for(i=0;i<n;i++)
	{
	  sumx+=x[i];
	  sumy+=y[i];
	  sumx2+=x[i]*x[i];
	  sumxy+=x[i]*y[i];
	}
      xc=sumx/n;
      yc=sumy/n;
      
      //Translate the center to the origin
      for(i=0;i<n;i++)
	{
	  centx[i]=(x[i]-xc);
	  centy[i]=(y[i]-yc);
	}
      
      sumx=0;
      sumy=0;
      
      for(i=0;i<n;i++)
	{
	  sumx+=centx[i];
	  sumy+=centy[i];
	  sumx2+=centx[i]*centx[i];
	  sumxy+=centx[i]*centy[i];
	}
      
      afit=(sumy*sumx2-sumx*sumxy)/(n*sumx2-sumx*sumx);
      bfit=(n*sumxy-sumx*sumy)/(n*sumx2-sumx*sumx);
      
      //For x=1 then the angle will be
      theta=atan(afit+bfit);
      
      //Rotate the ellipse to theta degrees
      for(i=0;i<n;i++)
	{
	  px[i]=(centx[i]*cos(theta)+centy[i]*sin(theta));
	  py[i]=(-centx[i]*sin(theta)+centy[i]*cos(theta));
	}
      //Setting the initial parameters a and b ZERO ORDER APPROXIMATION 
      for(i=0;i<n;i++)
	{
	  if(fabs(px[i])>xmax)
	    {
	      xmax=fabs(px[i]);
	      a=fabs(px[i]);
	    }
	  if(fabs(py[i])>ymax)
	    {	
	      ymax=fabs(py[i]);
	      b=fabs(py[i]);
	    }
	}
      
      //Rotate and move back to the original position
      for(i=0;i<n;i++)
	{
	  xkf=(px[i]*cos(-theta)+py[i]*sin(-theta))+xc;
	  ykf=(-px[i]*sin(-theta)+py[i]*cos(-theta))+yc;
	  px[i]=xkf;
	  py[i]=ykf;
	}

      parEllipse[0]=xc;
      parEllipse[1]=yc;
      parEllipse[2]=a;
      parEllipse[3]=b;
      parEllipse[4]=theta;
      
      delete [] centx;
      delete [] centy;
      delete [] px;
      delete [] py;
    }  
  return parEllipse;
}

