set table "207Bi_10.tbl"
set contour base
unset surface
set cntrparam levels discrete 247,220,192,165,137,110,83,55,28
splot "207Bi_10.mtx" matrix w l
