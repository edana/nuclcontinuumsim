/******************************************************************************************/
/**** MAIN file                                                                        ****/
/**** The run format is ResFunc1                                                       ****/
/******************************************************************************************/

#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cmath>
#include<cstdlib>
#include "MomentsDist.h"
#include "LinearInp.h"
#include "MatrixPrint.h"

//The execution argument is just the input file name *.inp  
main()
{
  int gammaE, k, H, M, M1, M2;
  //  const int SIZE = 15; 
  string file,file1,file2;
  float E1=0, E2=100000, enerInt;

  ofstream maxFile("maxFile.dat", ios::out);

  for(M=50;M<51;M++)
    {
      for(enerInt=50000;enerInt<51000;enerInt+=1000)
	{
	  E1=0;
	  E2=100000;
	  M1=M2=0;
	  ifstream fileTable("table.dat", ios::in);
	  cout << "For M=" << M << " and H=" << enerInt << "(keV)" << endl;  
	  while(fileTable)
	    {
	      fileTable >> gammaE >> k >> H >> file;
	      if((H<enerInt&&H>E1)&&k==M)
		{
		  E1=H;
		  M1=k;
		  file1=file;
		}
	      if((H>enerInt&&H<E2)&&k==M)
		{
		  E2=H;
		  M2=k;
		  file2=file;
		}
	    }
	  fileTable.close();
	  cout << file1 << " " << file2 << endl;
	  
	  if((file1!=file2)&&M1==M2)
	    {
	      ifstream cascFile1(file1.c_str(), ios::in);
	      ifstream cascFile2(file2.c_str(), ios::in);
	      
	      cout << "Opening the input file " << file1 << endl; 
	      cout << "Opening the input file " << file2 << endl; 
	      
	      if(!cascFile1)
		{
		  perror ("The following error occurred with the file 1");
		  exit(1);
		}
	      if(!cascFile2)
		{
		  perror ("The following error occurred with the file 2");
		  exit(1);
		}
	      	      
	      string fileName1=file1;
	      int point1=fileName1.find_first_of(".");
	      string name1=fileName1.substr(0,point1);
	      int centroid1[3]={0,0,0};
	      
	      string fileName2=file2;
	      int point2=fileName2.find_first_of(".");
	      string name2=fileName2.substr(0,point2);
	      int centroid2[3]={0,0,0};
	      
	      string out( "_out" ); 
	      string gui( "_" ); 
	      string tableExt( ".tbl" ); 
	      string outExt( ".out" ); 
	      string proExt( ".pro" ); 
	      string mtxExt( ".mtx" ); 
	      
	      //cout << "*********** First ***********" << endl;  
	      ostringstream BigName1;
	      BigName1 << name1 << mtxExt;
	      string more1=BigName1.str();
	      ofstream Hk1(more1.c_str(), ios::out);
	      
	      ostringstream nameProj1;
	      nameProj1 << name1 << proExt;
	      string second1=nameProj1.str();
	      ofstream proj1(second1.c_str(), ios::out);
	      
	      MatrixPrint MatrixFile1;
	      int max1=MatrixFile1.print2D(cascFile1,Hk1,proj1,centroid1);
	      
	      cascFile1.close(); 
	      Hk1.close();
	      proj1.close();
	      
	      ifstream cascFile3(file1.c_str(), ios::in);
	      
	      MomentsDist MomentsAnalysis1(cascFile3);
	      float meanH1=MomentsAnalysis1.MomentsCalcH();
	      int meank1=MomentsAnalysis1.MomentsCalck();
	      cascFile3.close(); 
	      
	      //cout << "*********** Second ***********" << endl;  
	      ostringstream BigName2;
	      BigName2 << name2 << mtxExt;
	      string more2=BigName2.str();
	      ofstream Hk2(more2.c_str(), ios::out);
	      
	      ostringstream nameProj2;
	      nameProj2 << name2 << proExt;
	      string second2=nameProj2.str();
	      ofstream proj2(second2.c_str(), ios::out);
	      	
	      MatrixPrint MatrixFile2;
	      int max2=MatrixFile2.print2D(cascFile2,Hk2,proj2,centroid2); 

	      cascFile2.close(); 
	      Hk2.close();
	      proj2.close();
	      
	      ifstream cascFile4(file2.c_str(), ios::in);
	      MomentsDist MomentsAnalysis2(cascFile4);
	      float meanH2=MomentsAnalysis2.MomentsCalcH();
	      int meank2=MomentsAnalysis2.MomentsCalck();
	      cascFile4.close(); 
	      
	      /**************** Table of contour creation ***************************/
	      //Create the contour tables for each distribution
	      
	      LinearInp contDistr1;
	      contDistr1.contour(name1,max1);
	      
	      LinearInp contDistr2;
	      contDistr2.contour(name2,max2);
	      
	      /************* Selection of the new points in the contour *********/
	      
	      //Read the contour table file
	      ostringstream nameTable1;
	      nameTable1 << name1 <<tableExt;
	      string tableBigName1=nameTable1.str();
	      ifstream tableFile1(tableBigName1.c_str(), ios::in);
	      
	      ostringstream outTable1;
	      outTable1 << name1 << out <<tableExt;
	      string tableBigOut1=outTable1.str();
	      ofstream tableOut1(tableBigOut1.c_str(), ios::out);
	      
	      ostringstream nameTable2;
	      nameTable2 << name2 <<tableExt;
	      string tableBigName2=nameTable2.str();
	      ifstream tableFile2(tableBigName2.c_str(), ios::in);
	      
	      ostringstream outTable2;
	      outTable2 << name2 << out <<tableExt;
	      string tableBigOut2=outTable2.str();
	      ofstream tableOut2(tableBigOut2.c_str(), ios::out);
	      	      
	      LinearInp contFit1;
	      contFit1.ReadCntrTable(tableFile1,centroid1,tableOut1);
	      LinearInp contFit2;
	      contFit2.ReadCntrTable(tableFile2,centroid2,tableOut2);
	      
	      tableFile1.close();
	      tableFile2.close();
	      tableOut1.close();
	      tableOut2.close();
	      
	      
	      /*************** Distributions interpolation **************/
	      
	      ostringstream inpTable1;
	      inpTable1 << name1 << out <<tableExt;
	      string tableBigInp1=inpTable1.str();
	      ifstream tableInp1(tableBigInp1.c_str(), ios::in);
	      
	      ostringstream inpTable2;
	      inpTable2 << name2 << out <<tableExt;
	      string tableBigInp2=inpTable2.str();
	      ifstream tableInp2(tableBigInp2.c_str(), ios::in);
	      
	      ostringstream outMatrix;
	      outMatrix << enerInt << gui << M << outExt;
	      string matrixBigOut=outMatrix.str();
	      ofstream matrixOut(matrixBigOut.c_str(), ios::out);
	      
	      LinearInp mapFit1;
	      mapFit1.distributionFit(tableInp1,E1,tableInp2,E2,enerInt,M,matrixOut);
	      
	      tableInp1.close();
	      tableInp2.close();
	      matrixOut.close();
	      
	      int centroidT[3]={0,0,0};
	      ifstream cascFileT(matrixBigOut.c_str(), ios::in);
	      
	      ostringstream outMatrix1;
	      outMatrix1 << enerInt << gui << M << proExt;
	      string matrixBigOut1=outMatrix1.str();
	      ofstream projT(matrixBigOut1.c_str(), ios::out);
	      
	      ostringstream outMatrix2;
	      outMatrix2 << enerInt << gui << M << mtxExt;
	      string matrixBigOut2=outMatrix2.str();
	      ofstream HkT(matrixBigOut2.c_str(), ios::out);
	    
	      MatrixPrint MatrixFileT;
	      double maxT=MatrixFileT.print2Dnorm(cascFileT,HkT,projT,centroidT); 
	  
	      cascFileT.close();
	      cascFileT.open(matrixBigOut.c_str(), ios::in);
	      
	      MomentsDist MomentsAnalysisT(cascFileT);
	      float meanHT=MomentsAnalysisT.MomentsCalcH();
	      int meankT=MomentsAnalysisT.MomentsCalck();
	      
	      ostringstream outMatrix3;
	      outMatrix3 << enerInt << gui << M;
	      string outNameT=outMatrix3.str();
	      LinearInp contDistriT;
	      contDistriT.contourDouble(outNameT,maxT);

	      maxFile << enerInt/1000 << "\t" << M << "\t" 
		      << centroidT[1] << "\t" << centroidT[0] << endl; 

	      cascFileT.close();
	      HkT.close();
	      projT.close();
	    }
	}
    }
  
  maxFile.close();
  return 0;
}
  

