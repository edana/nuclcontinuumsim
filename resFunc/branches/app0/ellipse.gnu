#set size square
unset key
set parametric
set xlabel 'k'
set ylabel 'H(MeV)'
#set term postscript eps color solid enhanced 'Helvetica' 21
#set out 'app0_207Bi_1_50.ps
xc0=40.8142
yc0=43.1316
a0=9.16824
b0=7.967
theta0=0.801969

x0(t)=xc0+a0*cos(t)*cos(theta0)-b0*sin(t)*sin(theta0)
y0(t)=yc0+b0*sin(t)*cos(theta0)+a0*cos(t)*sin(theta0)
xc2=40.932
yc2=42.8557
a2=6.0207
b2=5.01319
theta2=0.803994

x2(t)=xc2+a2*cos(t)*cos(theta2)-b2*sin(t)*sin(theta2)
y2(t)=yc2+b2*sin(t)*cos(theta2)+a2*cos(t)*sin(theta2)
xc4=40.4414
yc4=42.1721
a4=3.69488
b4=3.34976
theta4=0.804673

x4(t)=xc4+a4*cos(t)*cos(theta4)-b4*sin(t)*sin(theta4)
y4(t)=yc4+b4*sin(t)*cos(theta4)+a4*cos(t)*sin(theta4)
xc6=40.7629
yc6=42.7575
a6=2.62145
b6=1.59114
theta6=0.809281

x6(t)=xc6+a6*cos(t)*cos(theta6)-b6*sin(t)*sin(theta6)
y6(t)=yc6+b6*sin(t)*cos(theta6)+a6*cos(t)*sin(theta6)
xc8=40.0278
yc8=42.0578
a8=0.141483
b8=0.200803
theta8=0.810114

x8(t)=xc8+a8*cos(t)*cos(theta8)-b8*sin(t)*sin(theta8)
y8(t)=yc8+b8*sin(t)*cos(theta8)+a8*cos(t)*sin(theta8)
plot [0:2*pi] '207Bi_1_50.tbl' w lp lt 3 ps 2 pt 3, x0(t),y0(t) lt 1 lw 4,x2(t),y2(t) lt 1 lw 4,x4(t),y4(t) lt 1 lw 4,x6(t),y6(t) lt 1 lw 4,x8(t),y8(t) lt 1 lw 4
set term x11