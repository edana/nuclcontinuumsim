/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>


#include "MatrixPrint.h"

MatrixPrint::MatrixPrint()
{
    for(i=0;i<120;i++)
    {
	for(j=0;j<60;j++)
	    Hkmatrix[i][j]=0;
    }

    for(i=0;i<60;i++)
      {
	Hproj[i]=0;
	kproj[i]=0;
      }
    
}

int MatrixPrint::print2D(ifstream& input,ofstream& out,ofstream& projec, int *cent)
{
 while(input)
    {
      input >> H >> k >> Hge >> kge >> Hbgo >> kbgo;
      
      Hint=(int)(rint(H/1000));
      if(H>0&&k>0)
	Hkmatrix[k][Hint]+=1;
      
      H=0;k=0;
    }
  
  int max=0;
   for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  out << Hkmatrix[i][j] << "  ";
	  kproj[i]+=Hkmatrix[i][j];
	  if(Hkmatrix[i][j]>max)
	    {
	      max=Hkmatrix[i][j];
	      kmax=i;
	      Hmax=j;
	    }
	}
      out << endl;
    }
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  Hproj[j]+=Hkmatrix[i][j];
	}
    }
  
  
  for(i=0;i<60;i++)
    {
      projec << i << "\t" << Hproj[i] << "\t" << kproj[i] << "\t" 
	     << endl; 
    }
  cent[0]=kmax;
  cent[1]=Hmax;
  cent[2]=max;
  
  return max;
}

double MatrixPrint::print2Dnorm(ifstream& input,ofstream& out,ofstream& projec, int *cent)
{
  int totalEvents=0;
  int firstFlag=0, first=0, last=0, n1=0, n2=0, Hk1=0, Hk2=0;
  int zeros=0, mean=0, l=0, a=0, b=0;

  while(input)
    {
      input >> H >> k >> Hge >> kge >> Hbgo >> kbgo;
      
      Hint=(int)(rint(H/1000));
      if(H>0&&k>0)
	Hkmatrix[k][Hint]+=1;
      
      H=0;k=0;
    }

  for(j=0;j<60;j++)
    {
      firstFlag=first=last=n1=n2=Hk1=Hk2=0;
      mean=a=b=0;
      for(i=0;i<60;i++)
	{ 
	  if(Hkmatrix[i][j]!=0&&firstFlag==0)
	    {
	      firstFlag=1;
	      first=i;
	    }
	  if(Hkmatrix[i][j]!=0&&firstFlag==1)
	    {
	      last=i;
	    }
	}
      for(i=first;i<last;i++)
	{
	  while(Hkmatrix[i][j]==0)
	    {
	      if(Hkmatrix[i-a][j]!=0)
		{
		  Hk1=Hkmatrix[i-a][j];
		  n1=i;
		}
	      else 
		a++;
	      if(Hkmatrix[i+b][j]!=0)
		{
		  Hk2=Hkmatrix[i+b][j];
		  n2=i;
		}
	      else 
		b++;
	      mean=(int)((Hk2+Hk1)/2);
	      Hkmatrix[i][j]=mean;
	    }
	}
    }

  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{ 
	  totalEvents+=Hkmatrix[i][j];
	}
    }
  
  //cout << "Number of events in the matrix " << totalEvents << endl;
  
  double max=0;
  double Hproj_d[120], kproj_d[120];
  for(i=0;i<60;i++)
    {
      Hproj_d[i]=0;
      kproj_d[i]=0;
    }
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{ 
	  HkmtxNorm[i][j]=(double)Hkmatrix[i][j]/totalEvents;
	  out << HkmtxNorm[i][j] << "  ";
	  Hproj_d[j]+=HkmtxNorm[i][j];
	  kproj_d[i]+=HkmtxNorm[i][j];

	  if(HkmtxNorm[i][j]>max)
	    {
	      max=HkmtxNorm[i][j];
	      kmax=i;
	      Hmax=j;
	    }
	  
	}
      out << endl;
    }
  
  for(i=0;i<60;i++)
    {
      projec << i << "\t" << Hproj_d[i] << "\t" << kproj_d[i] << "\t" 
	     << endl; 
    }

  cent[0]=kmax;
  cent[1]=Hmax;
  cent[2]=max;

  return max;
}


double MatrixPrint::print2Dnorm2(ifstream& input,ofstream& out,ofstream& projec, int *cent)
{
  int totalEvents=0;
  int firstFlag=0, first=0, last=0, n1=0, n2=0, Hk1=0, Hk2=0;
  int zeros=0, mean=0, l=0, a=0, b=0;

  while(input)
    {
      input >> H >> k >> Hge >> kge >> Hbgo >> kbgo;
      
      Hint=(int)(rint(H/1000));
      if(H>0&&k>0)
	Hkmatrix[k][Hint]+=1;
      
      H=0;k=0;
    }

  for(j=0;j<60;j++)
    {
      firstFlag=first=last=n1=n2=Hk1=Hk2=0;
      mean=a=b=0;
      for(i=0;i<60;i++)
	{ 
	  if(Hkmatrix[i][j]!=0&&firstFlag==0)
	    {
	      firstFlag=1;
	      first=i;
	    }
	  if(Hkmatrix[i][j]!=0&&firstFlag==1)
	    {
	      last=i;
	    }
	}
      for(i=first;i<last;i++)
	{
	  while(Hkmatrix[i][j]==0)
	    {
	      if(Hkmatrix[i-a][j]!=0)
		{
		  Hk1=Hkmatrix[i-a][j];
		  n1=i;
		}
	      else 
		a++;
	      if(Hkmatrix[i+b][j]!=0)
		{
		  Hk2=Hkmatrix[i+b][j];
		  n2=i;
		}
	      else 
		b++;
	      mean=(int)((Hk2+Hk1)/2);
	      Hkmatrix[i][j]=mean;
	    }
	}
    }

  double maxG=0;
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{ 
	  if(Hkmatrix[i][j]>maxG)
	    {
	      maxG=Hkmatrix[i][j];
	    }
	}
    }
  
  //cout << "Number of events in the matrix " << totalEvents << endl;
  
  double max=0;
  double Hproj_d[120], kproj_d[120];
  for(i=0;i<60;i++)
    {
      Hproj_d[i]=0;
      kproj_d[i]=0;
    }

  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{ 
	  HkmtxNorm[i][j]=(double)Hkmatrix[i][j]/maxG; //Without normalization
	  out << HkmtxNorm[i][j] << "  ";
	  Hproj_d[j]+=HkmtxNorm[i][j];
	  kproj_d[i]+=HkmtxNorm[i][j];
	  
	  if(HkmtxNorm[i][j]>max)
	    {
	      max=HkmtxNorm[i][j];
	      kmax=i;
	      Hmax=j;
	    }
	  
	}
      out << endl;
    }


  for(i=0;i<60;i++)
    {
      projec << i << "\t" << Hproj_d[i] << "\t" << kproj_d[i] << "\t" 
	     << endl; 
    }
  
  cent[0]=kmax;
  cent[1]=Hmax;
  cent[2]=max;

  return max;
}

