/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed; 

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>

#include "MomentsDist.h"

MomentsDist::MomentsDist(ifstream& input) 
{
  ReadCasc(input);
}

void MomentsDist::ReadCasc(ifstream& input)
{
  int i=0;
  while(input)
    {
      input >> H >> k >> Hge >> kge >> Hbgo >>  kbgo;
      arrayH[i]=H;
      arrayk[i]=k;
      Hge_p[i]=Hge;
      kge_p[i]=kge;
      Hbgo_p[i]=Hbgo;
      kbgo_p[i]=kbgo;
      i++;
      if(H>0)
	Hnum++;
      if(k>0)
	knum++;
    }
}
/************************************************************/
float MomentsDist::MomentsCalcH()
{
  int i=0;
  /* Mean H*/
  for(i=0;i<100000;i++)
    {
      sumH+=arrayH[i];
    }
  meanH=sumH/(Hnum-1);  
  //cout << "mean H(keV) =  " << meanH  << endl;
  
  /*Variance H*/
  for(i=0;i<10000;i++)
    {
      if(arrayH[i]>0)
	varH1+=pow((arrayH[i]-meanH),2);
    }
  varH=sqrt(varH1/(Hnum-2));
  //cout << "variance H =  " << varH << endl;
  
  /* Skewness H */
  for(i=0;i<10000;i++)
    {
      if(arrayH[i]>0)
	skewH1+=pow((arrayH[i]-meanH),3);
    }
  skewH=skewH1/((Hnum-1)*pow(varH,3));
  //cout << "skewness H =  " << skewH << endl;
  
  /* Kurtosis H */
  for(i=0;i<10000;i++)
    {
      if(arrayH[i]>0)
	kurtH1+=pow((arrayH[i]-meanH),4);
    }
  kurtH=kurtH1/((Hnum-1)*pow(varH,4))-3;
  //cout << "kurtosis H =  " << kurtH << endl;

  return meanH;
}

/******************************************************************/

int MomentsDist::MomentsCalck()
{
  int i=0;
  /* Mean k*/
  for(i=0;i<100000;i++)
    {
      sumk+=arrayk[i];
    }
  meank=sumk/(knum-1);  
  //cout << "mean k =  " << meank  << endl;
  
  /*Variance k*/
  for(i=0;i<10000;i++)
    {
      if(arrayk[i]>0)
	vark1+=pow((arrayk[i]-meank),2);
    }
  vark=sqrt(vark1/(knum-2));
  //cout << "variance k =  " << vark << endl;
  
  /* Skewness k */
  for(i=0;i<10000;i++)
    {
      if(arrayk[i]>0)
	skewk1+=pow((arrayk[i]-meank),3);
    }
  skewk=skewk1/((knum-1)*pow(vark,3));
  //cout << "skewness k =  " << skewk << endl;
  
  /* Kurtosis k */
  for(i=0;i<10000;i++)
    {
      if(arrayk[i]>0)
	kurtk1+=pow((arrayk[i]-meank),4);
    }
  kurtk=kurtk1/((knum-1)*pow(vark,4))-3;
  //cout << "kurtosis k =  " << kurtk << endl;
  
  /******************************************************************/
  return meank;
}




