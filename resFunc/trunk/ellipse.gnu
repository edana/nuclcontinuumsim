#set size square
unset key
set parametric
set xlabel 'k'
set ylabel 'H(MeV)'
#set term postscript eps color solid enhanced 'Helvetica' 21
#set out '207Bi_1_50.ps
xc0=41.3323
yc0=42.7489
a0=4.29759
b0=7.80037
theta0=93.2692

x0(t)=xc0+a0*cos(t)*cos(theta0)-b0*sin(t)*sin(theta0)
y0(t)=yc0+b0*sin(t)*cos(theta0)+a0*cos(t)*sin(theta0)
xc1=40.6137
yc1=43.2571
a1=3.40963
b1=6.38633
theta1=1.5708

x1(t)=xc1+a1*cos(t)*cos(theta1)-b1*sin(t)*sin(theta1)
y1(t)=yc1+b1*sin(t)*cos(theta1)+a1*cos(t)*sin(theta1)
xc2=40.9361
yc2=42.9272
a2=3.44732
b2=3.89155
theta2=-20.586

x2(t)=xc2+a2*cos(t)*cos(theta2)-b2*sin(t)*sin(theta2)
y2(t)=yc2+b2*sin(t)*cos(theta2)+a2*cos(t)*sin(theta2)
xc3=41.4254
yc3=43.2758
a3=4.15199
b3=2.51222
theta3=1.03193

x3(t)=xc3+a3*cos(t)*cos(theta3)-b3*sin(t)*sin(theta3)
y3(t)=yc3+b3*sin(t)*cos(theta3)+a3*cos(t)*sin(theta3)
xc4=40.8325
yc4=42.5056
a4=3.36985
b4=2.04133
theta4=1.27039

x4(t)=xc4+a4*cos(t)*cos(theta4)-b4*sin(t)*sin(theta4)
y4(t)=yc4+b4*sin(t)*cos(theta4)+a4*cos(t)*sin(theta4)
xc5=40.5861
yc5=42.5726
a5=3.31813
b5=1.08455
theta5=-8.34273

x5(t)=xc5+a5*cos(t)*cos(theta5)-b5*sin(t)*sin(theta5)
y5(t)=yc5+b5*sin(t)*cos(theta5)+a5*cos(t)*sin(theta5)
xc6=40.7787
yc6=42.907
a6=1.90704
b6=0.778707
theta6=1.5708

x6(t)=xc6+a6*cos(t)*cos(theta6)-b6*sin(t)*sin(theta6)
y6(t)=yc6+b6*sin(t)*cos(theta6)+a6*cos(t)*sin(theta6)
xc7=40.3149
yc7=41.8427
a7=0.243459
b7=0.624933
theta7=1.14335

x7(t)=xc7+a7*cos(t)*cos(theta7)-b7*sin(t)*sin(theta7)
y7(t)=yc7+b7*sin(t)*cos(theta7)+a7*cos(t)*sin(theta7)
xc8=40.0278
yc8=42.0578
a8=0.16888
b8=0.08333
theta8=1.5708

x8(t)=xc8+a8*cos(t)*cos(theta8)-b8*sin(t)*sin(theta8)
y8(t)=yc8+b8*sin(t)*cos(theta8)+a8*cos(t)*sin(theta8)
plot [0:2*pi] '207Bi_1_50.tbl' w lp lt 3 ps 2 pt 3, x0(t),y0(t) lt 1 lw 4,x2(t),y2(t) lt 1 lw 4,x4(t),y4(t) lt 1 lw 4,x6(t),y6(t) lt 1 lw 4,x8(t),y8(t) lt 1 lw 4
set term x11