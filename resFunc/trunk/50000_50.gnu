set table "50000_50.tbl"
set contour base
unset surface
set cntrparam levels discrete 0.018,0.016, 0.014,0.012, 0.01,0.008, 0.006,0.004, 0.002
splot "50000_50.mtx" matrix w l
