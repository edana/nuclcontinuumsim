/********/
#ifndef MOMENTSDIST_H
#define MOMENTSDIST_H

class MomentsDist
{
 public:
  MomentsDist(ifstream&);
  void ReadCasc(ifstream&);
  float MomentsCalcH();
  int MomentsCalck();
 private:
  int Hnum, knum, num;  
  float arrayH[100000], arrayk[100000],Hge_p[100000],kge_p[100000];
  float Hbgo_p[100000],kbgo_p[100000];
  float Hproj_sum, H, k, Hge, kge, Hbgo, kbgo;
  float sumH, meanH, varH1, varH, skewH1, skewH, kurtH1 ,kurtH;
  float sumk, meank, vark1, vark, skewk1, skewk, kurtk1 ,kurtk;
};

#endif
