set table "207Bi_1_2.tbl"
set contour base
unset surface
set cntrparam levels discrete 2583,2296, 2009,1722, 1435,1148, 861,574, 287
splot "207Bi_1_2.mtx" matrix w l
