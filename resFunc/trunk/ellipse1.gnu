#set size square
unset key
set parametric
set xlabel 'k'
set ylabel 'H(MeV)'
set term postscript eps color solid enhanced 'Helvetica' 21
set out '60Co_1_5.ps'
xc0=6.04
yc0=5.94
a0=3.71
b0=2.02
theta0=46.1*3.1415/180

x0(t)=xc0+a0*cos(t)*cos(theta0)-b0*sin(t)*sin(theta0)
y0(t)=yc0+b0*sin(t)*cos(theta0)+a0*cos(t)*sin(theta0)
xc1=6.06
yc1=6.11
a1=1.78
b1=3.09
theta1=8.69639969

x1(t)=xc1+a1*cos(t)*cos(theta1)-b1*sin(t)*sin(theta1)
y1(t)=yc1+b1*sin(t)*cos(theta1)+a1*cos(t)*sin(theta1)
xc2=6.05
yc2=6.22
a2=1.56
b2=2.64
theta2=-0.7286

x2(t)=xc2+a2*cos(t)*cos(theta2)-b2*sin(t)*sin(theta2)
y2(t)=yc2+b2*sin(t)*cos(theta2)+a2*cos(t)*sin(theta2)

xc3=6.09
yc3=6.29
a3=2.36
b3=1.36
theta3=47.4*3.1415/180

x3(t)=xc3+a3*cos(t)*cos(theta3)-b3*sin(t)*sin(theta3)
y3(t)=yc3+b3*sin(t)*cos(theta3)+a3*cos(t)*sin(theta3)

xc4=6.1
yc4=6.31
a4=1.1
b4=2.1
theta4=2.40620637

x4(t)=xc4+a4*cos(t)*cos(theta4)-b4*sin(t)*sin(theta4)
y4(t)=yc4+b4*sin(t)*cos(theta4)+a4*cos(t)*sin(theta4)

xc5=6.06
yc5=6.35
a5=1.76
b5=0.937
theta5=0.90972

x5(t)=xc5+a5*cos(t)*cos(theta5)-b5*sin(t)*sin(theta5)
y5(t)=yc5+b5*sin(t)*cos(theta5)+a5*cos(t)*sin(theta5)

xc6=6.2
yc6=6.64
a6=0.772
b6=1.18
theta6=-13.1142

x6(t)=xc6+a6*cos(t)*cos(theta6)-b6*sin(t)*sin(theta6)
y6(t)=yc6+b6*sin(t)*cos(theta6)+a6*cos(t)*sin(theta6)

xc7=6.21
yc7=6.71
a7=0.851
b7=0.612
theta7=0.9628

x7(t)=xc7+a7*cos(t)*cos(theta7)-b7*sin(t)*sin(theta7)
y7(t)=yc7+b7*sin(t)*cos(theta7)+a7*cos(t)*sin(theta7)


xc8=6.09
yc8=6.82
a8=0.427
b8=0.233
theta8=81.7*3.1415/180

x8(t)=xc8+a8*cos(t)*cos(theta8)-b8*sin(t)*sin(theta8)
y8(t)=yc8+b8*sin(t)*cos(theta8)+a8*cos(t)*sin(theta8)
plot [0:2*pi] '60Co_1_5.tbl' w lp lt 3 ps 2 pt 3, x0(t),y0(t) lt 1 lw 4, x1(t),y1(t) lt 1 lw 4,x2(t),y2(t) lt 1 lw 4, x3(t),y3(t) lt 1 lw 4, x4(t),y4(t) lt 1 lw 4, x5(t),y5(t) lt 1 lw 4,x6(t),y6(t) lt 1 lw 4,x7(t),y7(t) lt 1 lw 4,x8(t),y8(t) lt 1 lw 4
set term x11