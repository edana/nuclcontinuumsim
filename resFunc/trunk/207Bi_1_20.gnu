set table "207Bi_1_20.tbl"
set contour base
unset surface
set cntrparam levels discrete 55,49, 43,37, 31,25, 19,13, 7
splot "207Bi_1_20.mtx" matrix w l
