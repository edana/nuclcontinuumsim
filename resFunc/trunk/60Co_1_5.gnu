set table "60Co_1_5.tbl"
set contour base
unset surface
set cntrparam levels discrete 313,278, 243,209, 174,139, 105,70, 35
splot "60Co_1_5.mtx" matrix w l
