set table "207Bi_1_50.tbl"
set contour base
unset surface
set cntrparam levels discrete 13,12, 10,9, 7,6, 5,3, 2
splot "207Bi_1_50.mtx" matrix w l
