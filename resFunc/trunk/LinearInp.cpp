/************/
//#include<iostream>
//using std::cerr;
//using std::cout;
//using std::cin;
//using std::endl;
//using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include <string> 
using std::string; 

#include <sstream> 
using std::ostringstream;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include <iomanip> 
using std::setw; 
using std::setprecision; 
using std::showpoint; 

#include "matrix.h"
#ifndef _NO_NAMESPACE 
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()	try {
#  define CATCHERROR()	} catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif
using std::fixed; 

typedef matrix<double> Matrix;

#include<stdlib.h>
#include<cstdlib>
#include<cmath>
#include "LinearInp.h"

using namespace std;

LinearInp::LinearInp() 
{
}

float LinearInp::interpolation(float p1, float ener1, float p2, float ener2, float ener)
{
  m=(p1-p2)/(ener1-ener2);
  b=p1-m*ener1;
  float p;
  p=m*ener+b;
  return p;
}

void LinearInp::contour(string st, int maxi)
{
  int ten=(int)(maxi-maxi*10/100);
  int twenty=(int)(maxi-maxi*20/100);
  int thirty=(int)(maxi-maxi*30/100);
  int forty=(int)(maxi-maxi*40/100);
  int fifhty=(int)(maxi-maxi*50/100);
  int sixty=(int)(maxi-maxi*60/100);
  int seventy=(int)(maxi-maxi*70/100);
  int eighty=(int)(maxi-maxi*80/100);
  int ninety=(int)(maxi-maxi*90/100);
 
  //Set the table file name just fot the gnuplot macro
  ostringstream tableBigName;
  string tableExt( ".tbl" ); 
  tableBigName << st << tableExt;
  string tableFile=tableBigName.str();

  //Set the matrix file name just fot the gnuplot macro
  ostringstream matrixBigName;
  string matrixExt( ".mtx" ); 
  matrixBigName << st << matrixExt;
  string matrixFile=matrixBigName.str();

  //Write the macro file for gnuplot 
  ostringstream gnuBigName;
  string gnuExt( ".gnu" ); 
  gnuBigName << st << gnuExt;
  string gnuFile=gnuBigName.str();
  ofstream gnuFileV(gnuFile.c_str(), ios::out);

  gnuFileV << "set table \"" << tableFile << "\"" << endl
	   << "set contour base" << endl 
	   << "unset surface" << endl
	   << "set cntrparam levels discrete " 
	   << ten << "," << twenty << ", " << thirty << "," << forty << ", " 
	   << fifhty << "," << sixty << ", "<< seventy << "," << eighty << ", " 
	   << ninety << endl
	   << "splot \"" << matrixFile << "\" matrix w l" << endl;

  //Execute the gnuplot macro file
  ostringstream gnuCommand;
  string gnu("gnuplot ");
  gnuCommand << gnu << st << gnuExt;
  string command(gnuCommand.str());
  int ret = system(command.c_str());
  gnuFileV.close();
}


void LinearInp::contourDouble(string st, double maxi)
{
  double ten=(maxi-maxi*10/100);
  double twenty=(maxi-maxi*20/100);
  double thirty=(maxi-maxi*30/100);
  double forty=(maxi-maxi*40/100);
  double fifhty=(maxi-maxi*50/100);
  double sixty=(maxi-maxi*60/100);
  double seventy=(maxi-maxi*70/100);
  double eighty=(maxi-maxi*80/100);
  double ninety=(maxi-maxi*90/100);
 
  //Set the table file name just fot the gnuplot macro
  ostringstream tableBigName;
  string tableExt( ".tbl" ); 
  tableBigName << st << tableExt;
  string tableFile=tableBigName.str();

  //Set the matrix file name just fot the gnuplot macro
  ostringstream matrixBigName;
  string matrixExt( ".mtx" ); 
  matrixBigName << st << matrixExt;
  string matrixFile=matrixBigName.str();

  //Write the macro file for gnuplot 
  ostringstream gnuBigName;
  string gnuExt( ".gnu" ); 
  gnuBigName << st << gnuExt;
  string gnuFile=gnuBigName.str();
  ofstream gnuFileV(gnuFile.c_str(), ios::out);

  gnuFileV << "set table \"" << tableFile << "\"" << endl
	   << "set contour base" << endl 
	   << "unset surface" << endl
	   << "set cntrparam levels discrete " 
	   << ten << "," << twenty << ", " << thirty << "," << forty << ", " 
	   << fifhty << "," << sixty << ", "<< seventy << "," << eighty << ", " 
	   << ninety << endl
	   << "splot \"" << matrixFile << "\" matrix w l" << endl;

  //Execute the gnuplot macro file
  ostringstream gnuCommand;
  string gnu("gnuplot ");
  gnuCommand << gnu << st << gnuExt;
  string command(gnuCommand.str());
  int ret = system(command.c_str());
  gnuFileV.close();
}

void  LinearInp::ReadCntrTable(ifstream& table1, int *cent,ofstream& table2)
{
  char c[35];
  double xa,ya,za,x2,y2,z2,x1,y1,z1,x,y,z;
  double m,b,m1,b1,m2,b2,t,c1,c2,c3;
  double xe1,xe2,ye1,ye2,phi,t_adj,cp;
  float angle=0, angle1=0;
  long pos;
  int p=0, iter=0, cnt_num=0;
  double X[185],Y[185],Z[185], lparam[5], ell1x[360], ell1y[360], ell2x[360], ell2y[360];
  int i=0,j=0,n=0, cntr=0;
  int k1=cent[0];
  int H1=cent[1];

  ofstream outF("ellipse.gnu",ios::out);

  for(i=0;i<=185;i++)
    {
      X[i]=0;
      Y[i]=0;
      Z[i]=0;
    }
    
   x2=y2=z2=x1=y1=z1=x=y=z=0;
  
  //Set the center of the new matrix
    
   outF << "#set size square" << endl
	<< "unset key" << endl
	<< "set parametric" << endl
	<< "set xlabel 'k'" << endl
	<< "set ylabel 'H(MeV)'" << endl
	<< "#set term postscript eps color solid enhanced 'Helvetica' 21" << endl
	<< "#set out 'app0_60Co_2_5.ps" << endl;
   
  while(table1)
    {
      table1.getline(c,35);
      table1 >> c;
      pos=table1.tellg();
      if(c[0]=='#')
	{
	  //cout << c << endl;
	  n=0;
	  angle1=0;
	  xa=ya=za=x2=y2=z2=x1=y1=z1=x=y=z=0;
	  if(X[0]!=0&&Y[0]!=0)
	    {
	      //Fit each contour to an ellipse
	      param=ellipseFit(X,Y,Z);
	      cnt_num++;

	      //Finding the intersection point of two ellipses in order to avoid overlaping
	      if(cnt_num>1)
		{
		  int g=0;
		  //while(g==0)
		  //{
		  for(i=0;i<360;i++)
		    {
		      t_adj=i*3.14159/180;		      
		      ell1x[i]=lparam[0]+lparam[2]*cos(t_adj)*cos(lparam[4])-lparam[3]*sin(t_adj)*sin(lparam[4]);
		      ell1y[i]=lparam[1]+lparam[3]*sin(t_adj)*cos(lparam[4])+lparam[2]*cos(t_adj)*sin(lparam[4]);
		      ell2x[i]=param[0]+param[2]*cos(t_adj)*cos(param[4])-param[3]*sin(t_adj)*sin(param[4]);
		      ell2y[i]=param[1]+param[3]*sin(t_adj)*cos(param[4])+param[2]*cos(t_adj)*sin(param[4]);
		    }
		  for(i=0;i<360;i++)
		    {
		      for(j=0;j<360;j++)
			{
			  if(fabs(ell1x[i]-ell2x[j])<0.001&&fabs(ell1y[i]-ell2y[j])<0.001)
			    {
			      g++;
			      cp=j;
			    }
			}
		    }
		  if(g>1)
		    {
		      cout << "Overlaping in contour " <<  cnt_num << " angle " << cp << endl;
		      /*if(cp>0||cp<45||cp>135||cp<225||cp>315||cp<360)
			param[2]=param[2]-0.01;
			else
			param[3]=param[3]-0.01;*/
		      g=0;
		    }
		  else
		    g=1;
		  //}
		}
	      lparam[0]=param[0];
	      lparam[1]=param[1];
	      lparam[2]=param[2];
	      lparam[3]=param[3];
	      lparam[4]=param[4];
	      
	      //     if(cnt_num%2!=0)
	      {
		table2.precision(3);
		table2 << fixed << left << showpoint  
		       << setw( 10 ) << param[0]  
		       << setw( 10 ) << param[1] 
		       << setw( 10 ) << param[2]  
		       << setw( 10 ) << param[3] 
		       << setw( 10 ) << param[4] 
		       << setw( 10 ) << Z[0] << endl;  
		
		outF << "xc" << iter << "=" << param[0] << endl
		     << "yc" << iter << "=" << param[1] << endl
		     << "a" << iter << "=" << param[2] << endl
		     << "b" << iter << "=" << param[3] << endl
		     << "theta" << iter << "=" << param[4] << endl << endl;
		
		outF << "x" << iter << "(t)=xc" << iter << "+a" << iter 
		     << "*cos(t)*cos(theta" << iter << ")-b" << iter 
		     << "*sin(t)*sin(theta" << iter << ")" << endl;
		
		outF << "y" << iter << "(t)=yc" << iter << "+b" << iter 
		     << "*sin(t)*cos(theta" << iter << ")+a" << iter 
		     << "*cos(t)*sin(theta" << iter 
		     << ")" << endl; 	  
		
	      }		
	      iter++;
	      
	    }
	      
	  for(i=0;i<=185;i++)
	    {
	      X[i]=0;
	      Y[i]=0;
	      Z[i]=0;
	    }
	   cntr=0;
	}
      else//Reads the contour 
	{
	  for(i=0;i<10;i++)
	    if(c[i]!='\0')
	      pos--;
	    else
	      i=10;
	  table1.seekg(pos);
	  table1 >> x >> y >> z;
	  //Saving one contour into an array set
	  X[cntr]=x;
	  Y[cntr]=y;
	  Z[cntr]=z;
	  cntr++;
	}
    }
  
  if(X[0]!=0&&Y[0]!=0)
    {
      param=ellipseFit(X,Y,Z);
      
      table2.precision(3);
      table2 << fixed << left << showpoint  
	     << setw( 10 ) << param[0]  
	     << setw( 10 ) << param[1] 
	     << setw( 10 ) << param[2]  
	     << setw( 10 ) << param[3] 
	     << setw( 10 ) << param[4]         
	     << setw( 10 ) << Z[0] << endl;
  
      outF << "xc" << iter << "=" << param[0] << endl
	   << "yc" << iter << "=" << param[1] << endl
	   << "a" << iter << "=" << param[2] << endl
	   << "b" << iter << "=" << param[3] << endl
	   << "theta" << iter << "=" << param[4] << endl << endl;
      
      outF << "x" << iter << "(t)=xc" << iter << "+a" << iter 
	   << "*cos(t)*cos(theta" << iter << ")-b" << iter 
	   << "*sin(t)*sin(theta" << iter << ")" << endl;
      
      outF << "y" << iter << "(t)=yc" << iter << "+b" << iter 
	   << "*sin(t)*cos(theta" << iter << ")+a" << iter 
	   << "*cos(t)*sin(theta" << iter 
	   << ")" << endl; 
      
      
      outF << "plot [0:2*pi] '207Bi_1_10.tbl' w lp lt 3 ps 2 pt 3, ";
    }
  for(i=0;i<iter;i++)
    outF << "x" << i << "(t),y" << i << "(t) lt 1 lw 4,";

  outF << "x" << iter << "(t),y" << iter << "(t) lt 1 lw 4" << endl
       << "set term x11";

  // table2 << cent[0] << "\t "<< cent[1] << "\t " << cent[2] << endl << endl; 
 
  outF.close();
}

void LinearInp::distributionFit(ifstream& input1,float e1, ifstream& input2, float e2, float eInt, int M, ofstream& matrixFile)
{
  double xc, yc, a, b ,theta;
  double xc1, yc1, a1, b1 ,theta1;
  double xc2, yc2, a2, b2 ,theta2;
  double t_intr, ellk[360], ellH[360], p[6];
  float H1, k1,H2,k2,H,k;
  float c1, c2, c, cp;
  int i=0,j=0,kmtx=0,Hmtx=0,cmtx=0;
  e1=e1/1000;
  e2=e2/1000;
  eInt=eInt/1000;
  int matrix[60][60];

  for(i=0;i<60;i++)
    for(j=0;j<60;j++)
      matrix[i][j]=0;

  for(i=0;i<360;i++)
    {
      ellk[i]=0;
      ellH[i]=0;
    }
  while(input1)
    {
      input1 >> xc1 >> yc1 >> a1 >> b1 >> theta1 >> c1;
      input2 >> xc2 >> yc2 >> a2 >> b2 >> theta2 >> c2;
      xc=interpolation(xc1, e1, xc2, e2, eInt);
      yc=interpolation(yc1, e1, yc2, e2, eInt);
      a=interpolation(a1, e1, a2, e2, eInt);
      b=interpolation(b1, e1, b2, e2, eInt);
      theta=interpolation(theta1, e1, theta2, e2, eInt);
      c=interpolation(c1, e1, c2, e2, eInt);

      p[0]=xc;
      p[1]=yc;
      p[2]=a;
      p[3]=b;
      p[4]=theta;
      p[5]=c;

      for(i=0;i<360;i++)
	{
	  t_intr=i*3.14159/180;		      
	  ellk[i]=xc+a*cos(t_intr)*cos(theta)-b*sin(t_intr)*sin(theta);
	  ellH[i]=yc+b*sin(t_intr)*cos(theta)+a*cos(t_intr)*sin(theta);
	}
      
      for(i=0;i<360;i++)
	{
	  Hmtx=(int)(rint(ellH[i]));
	  kmtx=(int)(rint(ellk[i]));
	  cmtx=(int)(rint(c));
	  
	  if(matrix[kmtx][Hmtx]==0)
	    matrix[kmtx][Hmtx]=cmtx;
	  else
	    {
	      cp=matrix[kmtx][Hmtx];
	      matrix[kmtx][Hmtx]=(int)(rint((cmtx+cp)/2));
	    }
	}
    }

  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	for(cp=0;cp<matrix[i][j];cp++)
	  matrixFile << j*1000 << "\t " << i << "\t 0 \t 0 \t 0 \t 0" <<endl;
    }
}

double * LinearInp::ellipseFit(double *X, double *Y, double *Z)
{
     
  /*****************************************************************************/
  /*****   Including the routine of LSP fitting                            *****/
  /*****************************************************************************/

  double sumX=0,sumY=0, A_last=999;
  double theta=90, d=0, dant=0;
  int iter=0, id=0, jd=0, repNum=0, repMax=0, validP=0;
  int i=0, j=0 ,n=0, m=0;
  double *px, *py, *xT, *yT, *centX, *centY, Xc=0, Yc=0;
  double Qdf1dx=0, Qdf1dy=0, Qdf2dx=0, Qdf2dy=0;
  double *OCPX, *OCPY, *OCPx, *OCPy;
  
  double a=1, b=2;
  double xk1=0, yk1=0, xk2=0, yk2=0, x=0, y=0;
  double ymin=100, xmin=100;
  double f1=0, f2=0;
  bool flag1=true;  

  theta=theta * 3.141592/180.;

  //We need to avoid the zero values in the contours
  for(i=0;i<100;i++)
    {
      if(X[i]!=0 || Y[i]!=0)
	{
	  n++;
	}
    }
  
  for(i=0;i<100;i++)   
    {
      if((X[i]==0||Y[i]==0)&&i<n)
	{
	  for(j=0;j<100;j++)   
	    {
	      X[j]=X[j+1];
	      Y[j]=Y[j+1];
	    }
	  i--;
	}
    }
  
  //We need to check if there are at least 5 different points in the contour
  //if not, we need to generate the suficient points

  while(flag1)
    {
      dant=d=repMax=0;
      for(i=0;i<n;i++)
	{
	  repNum=0;
	  for(j=0;j<n;j++)
	    {
	      if(X[j]==X[i]&&Y[j]==Y[i])
		repNum++;
	    }
	  if(repMax<repNum)
	    repMax=repNum;
	}
      
      //Number of valid points in the contour 
      validP=n-repMax+1;
      //cout << "Number of valid points " << n << " " << validP << endl;
      if(validP<6)
	{
	  for(i=0;i<(validP);i++)
	    {
	      X[n]=(X[i]+X[i+1])/2;
	      Y[n]=(Y[i]+Y[i+1])/2;
	      n++;
	    }
	  //for(i=0;i<n;i++)
	    //cout << X[i] << " " << Y[i] << endl;
	  
	}
      else
	flag1=false;
    }
  
  
  
  xT=new double [n];
  yT=new double [n];
  
  centX=new double [n];
  centY=new double [n];
  
  px=new double [n];
  py=new double [n];

  OCPX=new double [n];
  OCPY=new double [n];
  
  OCPx=new double [n];
  OCPy=new double [n];
  
  //Search the geometrical center, calculating Mean 
  for(i=0; i<n; i++)
    {
      sumX+=X[i];
      sumY+=Y[i];
    }
  Xc=sumX/double(n);
  Yc=sumY/double(n);
  
  //Translate the center to the origin =
  // translate the data points to the center of the 'xy' system
  for(i=0;i<n;i++)
    {
      xT[i]=(X[i]-Xc);
      yT[i]=(Y[i]-Yc);
    }

  // Rotate the data points (-theta) degrees =
  // this completes placing the points in the 'xy' system
  // i.e., (px[i],py[i])= (x_i,y_i) of Fig.3 in Ahn's paper (2001) 
  for(i=0;i<n;i++)
    {
      px[i] = xT[i]*cos(-theta) - yT[i]*sin(-theta);
      py[i] = xT[i]*sin(-theta) + yT[i]*cos(-theta);
    }
  
  if(n!=0)
    {       
      Matrix Q(2,2);
      Matrix deltaX(2,1);
      Matrix f(2,1);

      // fixing initial a,b 
      for(i=0;i<n;i++)
	{
	  if(fabs(px[i])<fabs(xmin))
	    {
	      xmin=px[i];
	      b=fabs(py[i]);
	    }
	  if(fabs(py[i])<fabs(ymin))
	    {	
	      ymin=py[i];
	      a=fabs(px[i]);
	    }
	}
      
      
      // Initial parameters from arithmetical fitting: (a,b,Xc,Yc) 
      // and proposed theta
      
            
      double A_min=9999.;
      float Xc_min, Yc_min, a_min, b_min, theta_min;
      int iter_min=0;
	
      double delta_Xc=0, delta_Yc=0, delta_a=0, delta_b=0, delta_theta=0;
      int l=0, j=0;
      int m=2*n;

      Matrix R(2,2);
      Matrix B1(2,1);
      Matrix B2(2,1);
      Matrix B3(2,1);
      Matrix B4(2,1);
      Matrix B5(2,1);
      Matrix j1(2,1);
      Matrix j2(2,1);
      Matrix j3(2,1);
      Matrix j4(2,1);
      Matrix j5(2,1);
      Matrix delta(5,1);	      
      Matrix error(m,1);
      Matrix jac(m,5);

      for(i=0;i<5;i++)
	delta(i,0)=0;
      
      for(i=0;i<m;i++)
	error(i,0)=0;

      for(i=0;i<m;i++)
	{
	  for(j=0;j<5;j++)
	    {
	      jac(i,j)=0;
	    }
	}
      
      //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      // The iteration procedure starts here
      //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      for(int k=0;k<50;k++){ 

	//Translate the center to the origin =
	// translate the data points to the center of the 'xy' system
	for(i=0;i<n;i++)
	  {
	    xT[i]=(X[i]-Xc);
	    yT[i]=(Y[i]-Yc);
	  }

	// Rotate the data points -theta degrees =
	// this completes placing the points in the 'xy' system
	for(i=0;i<n;i++)
	  {
	    px[i] = xT[i]*cos(-theta) - yT[i]*sin(-theta);
	    py[i] = xT[i]*sin(-theta) + yT[i]*cos(-theta);
	  }
	
	// fixing initial points for generalized Newton to find orthogonal
	// contacting points OCP_i = (x'_i,y'_i)
	for(int j=0;j<n;j++)
	  {
	    //Finding \vec{x}_k1 for each point
	    xk1=px[j]*a*b/(pow(b*b*px[j]*px[j]+a*a*py[j]*py[j],0.5));
	    yk1=py[j]*a*b/(pow(b*b*px[j]*px[j]+a*a*py[j]*py[j],0.5));
	    
	    //Finding \vec{x}_k2 for each point
	    if(fabs(px[j])<a)
	      {
		xk2=px[j];
		if(py[j]<0)
		  yk2=-b/a*pow(a*a-px[j]*px[j],0.5);
		else
		  yk2=b/a*pow(a*a-px[j]*px[j],0.5);
	      }
	    else
	      {
		if(px[j]<0)
		  xk2=-a;
		else
		  xk2=a;
		yk2=0;
	      }
	    // These are the first (in each iteration) approximation to the 
	    // OCP -> (x,y)
	    x=0.5*(xk1+xk2);
	    y=0.5*(yk1+yk2); 

	    //Finding (xk,y)=OCP in xy system (Generalized Newton method)
	    for(int k=0;k<10;k++)
	      {
		Qdf1dx=b*b*x;
		Qdf1dy=a*a*y;
		Qdf2dx=(a*a-b*b)*y+b*b*py[j];
		Qdf2dy=(a*a-b*b)*x-a*a*px[j];	   
		f1=0.5*(a*a*y*y+b*b*x*x-a*a*b*b);
		f2=b*b*x*(py[j]-y)-a*a*y*(px[j]-x);
		
		Q(0,0)=Qdf1dx;
		Q(0,1)=Qdf1dy;
		Q(1,0)=Qdf2dx;
		Q(1,1)=Qdf2dy;
		
		f(0,0)=f1;
		f(1,0)=f2;
		
		deltaX=-!(~Q*Q)*(~Q*f);
		
		if(fabs(deltaX(0,0))<0.001 && fabs(deltaX(1,0))<0.001)
		  goto ok;
		
		x+=deltaX(0,0);
		y+=deltaX(1,0);
	      }
	  ok:
	    
	    //Orthogonal contacting points in xy system = (x_i',y_i') = (x,y)
	    // in eqs. (26). (x,y) is the result of the iterative algorithm
	    // eqss. (27,28)
	    OCPx[j]=x;
	    OCPy[j]=y;

	    //Rotate and move back to the original position
	    OCPX[j] = x*cos(theta) - y*sin(theta) + Xc;
	    OCPY[j] = x*sin(theta) + y*cos(theta) + Yc;
	  }
	
	TRYBEGIN()
	  {	     	    
	    l=0;
	    for(i=0;i<n;i++)
	      {
		error(l,0)=X[i]-OCPX[i];
		l++;
		error(l,0)=Y[i]-OCPY[i];
		l++;
	      }
	  }
	CATCHERROR();
	
	l=0;     
	for(i=0;i<n;i++)
	  {
	    x=OCPx[i];
	    y=OCPy[i];
	    
	    TRYBEGIN()
	      {
		R(0,0)=cos(theta);
		R(0,1)=sin(theta);
		R(1,0)=-sin(theta);
		R(1,1)=cos(theta);
		
		Q(0,0)=b*b*x;
		Q(0,1)=a*a*y;		  
		Q(1,0)=(a*a-b*b)*y+b*b*py[i];
		Q(1,1)=(a*a-b*b)*x-a*a*px[i];    
		
		B1(0,0)=b*b*x*cos(theta)-a*a*y*sin(theta);
		B1(1,0)=b*b*(py[i]-y)*cos(theta)+a*a*(px[i]-x)*sin(theta);
		B2(0,0)=b*b*x*sin(theta)+a*a*y*cos(theta);
		B2(1,0)=b*b*(py[i]-y)*sin(theta)-a*a*(px[i]-x)*cos(theta);
		B3(0,0)=a*(b*b-y*y);
		B3(1,0)=2*a*y*(px[i]-x);
		B4(0,0)=b*(a*a-x*x);
		B4(1,0)=-2*b*x*(py[i]-y);
		B5(0,0)=(a*a-b*b)*x*y;
		B5(1,0)=(a*a-b*b)*(x*x-y*y-x*px[i]+y*py[i]);
		
		j1=!R*!Q*B1;
		j2=!R*!Q*B2;
		j3=!R*!Q*B3;
		j4=!R*!Q*B4;
		j5=!R*!Q*B5;
		
		jac(l,0)=j1(0,0);
		jac(l,1)=j2(0,0);
		jac(l,2)=j3(0,0);
		jac(l,3)=j4(0,0);
		jac(l,4)=j5(0,0);
		l++;
		jac(l,0)=j1(1,0);
		jac(l,1)=j2(1,0);
		jac(l,2)=j3(1,0);
		jac(l,3)=j4(1,0);
		jac(l,4)=j5(1,0);
		l++;
	      }
	    CATCHERROR();	      
	  }
	
	TRYBEGIN()
	  {
	    delta=!(~jac*jac)*(~jac*error);
	  }
	CATCHERROR();
	
	iter++;
	
	double A = delta.Norm();
	//cout << "The norm of the parameter vector is: " << A << endl;
	
	if (A<A_min){
	  iter_min = iter-1;
	  A_min=A;
	  Xc_min=Xc;
	  Yc_min=Yc;
	  a_min=a;
	  b_min=b;
	  theta_min=theta;
	}
	else {
	  if(A>A_last)
	    goto end;
	  A_last=A_min;
	}
	
	if(A<0.0001)
	  goto end;
	
	Xc+=delta(0,0);
	Yc+=delta(1,0);
	a+=delta(2,0);
	b+=delta(3,0);
	theta+=delta(4,0);
      }
    end:
      
      /* cout.precision(3);
      cout << endl;
      cout << "   -- results for minimum ---" << endl;
      cout << "  A_min= " << A_min << "  at iteration " << iter_min +1 << endl;
      
      
      cout << "    Xc_min=" << Xc_min << endl
	   << "    Yc_min=" << Yc_min << endl
	   << "    a_min=" << a_min << endl
	   << "    b_min=" << b_min << endl
	   << "    theta_min=" << theta_min*180/3.1415 << "*pi/180" 
	   << endl << endl;
      */
      parEllipse[0]=Xc_min;
      parEllipse[1]=Yc_min;
      parEllipse[2]=a_min;
      parEllipse[3]=b_min;
      parEllipse[4]=theta_min;
      
      delete [] xT;
      delete [] yT;
      delete [] px;
      delete [] py;
    }  
  return parEllipse;
}

