/********/
#ifndef READEVENTFILE_H
#define READEVENTFILE_H

class ReadEventFile
{
 public:
  ReadEventFile(ifstream&, ifstream&, ofstream&, ofstream&);

  void search_$(ifstream& );
  void EventAnalysis(ifstream&, ifstream&, ofstream&, ofstream&);
  void histograming(ifstream&, ofstream&);

 private:
  char dato;  
  int det,number, det_num, Rnumber;
  double eDep, psi, theta, phi, energy_ref;  
  double H[120], Energy, EnergyT, Htotal,Hge,Hbgo;
  int i,M,k,kge,kbgo,n;
  int Hmax1,Hmax2,Hmax3;
  int N1[200], N2[200], N3[200];
  
};

#endif
