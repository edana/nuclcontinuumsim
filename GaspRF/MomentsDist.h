/********/
#ifndef MOMENTSDIST_H
#define MOMENTSDIST_H

class MomentsDist
{
 public:
  MomentsDist(ifstream&);
  void ReadProj(ifstream&);
  
 private:
  int num;  
  float Hproj[60], kproj[60],Hge_p[60], kge_p[60],Hbgo_p[60], kbgo_p[60];
  float meanH, varianceH, skewH, kurtH;
  float Hproj_sum, H, k, Hge, kge, Hbgo, kbgo;
};

#endif
