/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>

#include "ReadEventFile.h"

ReadEventFile::ReadEventFile(ifstream& EventFile, ifstream& RealEvent, ofstream& Output, ofstream& Nfile)
{
}

void ReadEventFile::search_$(ifstream& EventFile)
{
  while (dato!='$')
    {
      EventFile >> dato;
    }
}

//Read the events
void ReadEventFile::EventAnalysis(ifstream& EventFile, ifstream& RealEvent, ofstream& Output, ofstream& Nfile)
{    
  
  energy_ref=0;  
  EnergyT=0; Htotal=0; Hge=0; Hbgo=0;
  i=0; M=0; k=0; kge=0; kbgo=0;
  
  for(i=0;i<120;i++)
    H[i]=0;
  
  while(EventFile)
    {
      EventFile >> det >> eDep >> psi >> theta >> phi >> number;
      
      if(det==-1)//end of gamma interaction
	{
	  RealEvent >> Rnumber >> Energy; //Reading the total energy for the input file 
	  
	  if(energy_ref<Energy) //Cascade end
	    {
	      if(M>0)
		  Output << EnergyT << "\t" << M << "\t"
			 << Htotal << "\t" << k << "\t"
			 << Hge << "\t" << kge << "\t"
			 << Hbgo << " \t" << kbgo << endl;
	      
	      M=0;
	      EnergyT=0;
	      k=0; kge=0; kbgo=0;
	      Htotal=0; Hge=0; Hbgo=0;
	      
	      for(i=0;i<120;i++)
		H[i]=0;
	      
	      n=0;
	      Hmax1=0;Hmax2=0,Hmax3=0;
	    }

	  M++;//Adding the multiplicity
	  EnergyT+=Energy; //Adding the real total energy
	  //cout << EnergyT << " " << Energy << endl;
	  energy_ref=Energy;
	  //Making the Hk 
	  for(i=0;i<120;i++)
	    {
	      if(H[i]>0)
		{
		  k+=1;
		  Htotal+=H[i];
		  //For Ge detectors
		  if (i<40)
		    {
		      kge+=1;
		      Hge+=H[i];
		    }
		  //For BGO detectors
		  else
		    {
		      kbgo+=1;
		      Hbgo+=H[i];
		    }
		}
	    }



	  n++;
	  if(n==2)
	    {
	      Hmax1=(int)(rint(Htotal/100));
	      Nfile << Hmax1;
	    }

	  if(n==3)
	    {
	      Hmax2=(int)(rint(Htotal/100))-Hmax1;
	      Nfile << "\t" << Hmax2;
	    }
	  
	  if(n==4)
	    {
	      Hmax3=(int)(rint(Htotal/100))-Hmax2-Hmax1;
	      Nfile << "\t" << Hmax3 << endl;
	    }



	  for(i=0;i<120;i++)
	    H[i]=0;
	}
      else if(det>1000&&det<3000)//
	{
	  //For Ge detectors
	  if (det<2000)
	    det_num=det-1001;
	  //For BGO detectors
	  else
	    det_num=det-2000+40;		
	  H[det_num]+=eDep;

	}
    }
  Output << EnergyT << "\t" << M << "\t"
	 << Htotal << "\t" << k << "\t"
	 << Hge << "\t" << kge << "\t"
	 << Hbgo << " \t" << kbgo << endl;
  
 
}


void ReadEventFile::histograming(ifstream& Nfile, ofstream& EOSfile)
{
  while(Nfile)
    {  
      Nfile >> Hmax1 >> Hmax2 >> Hmax3;
      
      if(Hmax1>200||Hmax2>200||Hmax2>200)
	{
	  cout << "Error the energy is out of the spectrum > 20Mev" << endl;
	  cout << Hmax1*100 << "(keV)" << endl;
	  cout << Hmax2*100 << "(keV)" << endl;
	  cout << Hmax3*100 << "(keV)" << endl;
	  exit(1);
	}
      
      N1[Hmax1]++;
      N2[Hmax2]++;
      N3[Hmax3]++;
    }
  for(i=1;i<200;i++)
    {
      EOSfile << i*100 << "\t" << N1[i] 
	    << "\t" << N2[i] 
	    << "\t" << N3[i] << endl;
    } 

}


