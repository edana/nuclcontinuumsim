/**************************************************************************************//**
@brief Read the \c Gasp output 
@file GaspRF/main.cpp
 *
 * This code read the output files of \c Gasp in the standard output.\n                
 * The run format is Analysis "*.inp" \n                                                  
 * The input file *.inp is the file wich contains the gamma events as they come from 
 * \Gamble, it can have any name but its name will be used to some of the outputs. Other
 * file needed is GammaEvents.000*, the program ask the number of files of this type that
 * will be included. \n
 *
 * You can choose the analysis you want to make" 
 * <ol type="0">
 * <li> Efficiencys calculation. </li>
 * <li> Make the EMHK file. </li>
 * <li> Make the Hk matrix and projections"  </li>
 * <li> Make the Hk selection to build the IE matrix"  </li>
 * <li> All the analysis." </li>
 * </ol>
******************************************************************************************/

#include<iostream>
using std::cerr; 
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<string>
using std::string;

#include<cstdlib>
#include "ArrayProp.h"
#include "ReadEventFile.h"
#include "MatrixPrint.h"
#include "Region.h"


/// The execution argument is the input file name *.inp  
main(int argc, char * argv[])
{
  //Cheking the number of arguments 
  if(argc!=2)
    {
      cerr << "Error -- Use ./Analysis *.inp " << endl;
      exit(1);
    }

  cout << "Choose the analysis you want to make" << endl
       << "0. Efficiencys calculation" << endl
       << "1. Make the EMHK file." << endl
       << "2. Make the Hk matrix and projections" << endl
       << "3. Make the Hk selection to build the IE matrix" << endl
       << "4. All the analysis." << endl;
  int type;
  cin >> type;

   
  ifstream IE("IE.dat", ios::in);
  /**Up to six files can be read numbered:\n
   *GammaEvents.0000\n
   *GammaEvents.0001\n
   *GammaEvents.0002\n
   *GammaEvents.0003\n
   *GammaEvents.0004\n   
   *GammaEvents.0005\n
   */
  if(type==1||type==4)
    {
       cout << "Do you have the GammaEvents.0001 file? \n yes -> 1 no -> 0" << endl;
      int yes1;
      cin >> yes1;
      cout << "Do you have the GammaEvents.00002 file? \n yes -> 1 no -> 0" << endl;
      int yes2;
      cin >> yes2;
      cout << "Do you have the GammaEvents.00003 file? \n yes -> 1 no -> 0" << endl;
      int yes3;
      cin >> yes3;
      cout << "Do you have the GammaEvents.00004 file? \n yes -> 1 no -> 0" << endl;
      int yes4;
      cin >> yes4;

      cout << "Do you have the GammaEvents.00005 file? \n yes -> 1 no -> 0" << endl;
      int yes5;
      cin >> yes5;

      //Declaring the input files 
      cout << "Opening the input file " << argv[1] << endl; 
      ifstream input(argv[1], ios::in);
      cout << "Opening the input file GammaEvents.0000" << endl; 
      ifstream events("GammaEvents.0000",ios::in);
      
      //Opening the input file *.inp
      
      if(!input)
	{
	  cerr << "Input error with the *.inp file \n";
	  exit(1);
	}
      
      //Opening the input file GammaEvents.0000
      
      if(!events)
	{
	  cerr << "Input error with the GammaEvents.0000 file\n";
	  exit(1);
	}
      
      //Declaring the output files 
      
      ofstream EMHko("EMHk.dat", ios::out);
      
      if(!EMHko)
	{
	  cerr << "Error\n";
	  exit(1);
	}

      ofstream EOS("EOS.spc", ios::out);
      ifstream EOSi("EOS.spc", ios::in);
      ofstream order("order.spc", ios::out);

      ReadEventFile GammaFile(events, input, EMHko, EOS);
      GammaFile.search_$(events);
      GammaFile.EventAnalysis(events, input, EMHko, EOS);
           
      if(yes1==1)
	{
	  cout << "Opening the input file GammaEvents.0001" << endl; 
	  ifstream events01("GammaEvents.0001",ios::in);
	  
	  ReadEventFile GammaFile1(events01, input, EMHko, EOS);
	  GammaFile1.search_$(events01);
	  GammaFile1.EventAnalysis(events01, input, EMHko, EOS);
	}
      if(yes2==1)
	{
	  cout << "Opening the input file GammaEvents.0002" << endl; 
	  ifstream events02("GammaEvents.0002",ios::in);
	  
	  ReadEventFile GammaFile2(events02, input, EMHko, EOS);
	  GammaFile2.search_$(events02);
	  GammaFile2.EventAnalysis(events02, input, EMHko, EOS);
	}
      if(yes3==1)
	{
	  cout << "Opening the input file GammaEvents.0003" << endl; 
	  ifstream events03("GammaEvents.0003",ios::in);
	  
	  ReadEventFile GammaFile3(events03, input, EMHko, EOS);
	  GammaFile3.search_$(events03);
	  GammaFile3.EventAnalysis(events03, input, EMHko, EOS);
	}
      if(yes4==1)
	{
	  cout << "Opening the input file GammaEvents.0004" << endl; 
	  ifstream events04("GammaEvents.0004",ios::in);
	  
	  ReadEventFile GammaFile4(events04, input, EMHko, EOS);
	  GammaFile4.search_$(events04);
	  GammaFile4.EventAnalysis(events04, input, EMHko, EOS);
	}
      if(yes5==1)
	{
	  cout << "Opening the input file GammaEvents.0005" << endl; 
	  ifstream events05("GammaEvents.0005",ios::in);
	  
	  ReadEventFile GammaFile5(events05, input, EMHko, EOS);
	  GammaFile5.search_$(events05);
	  GammaFile5.EventAnalysis(events05, input, EMHko, EOS);
	}
      GammaFile.histograming(EOSi,order);

    }

 ifstream EMHki("EMHk.dat", ios::in);
  
  if(!EMHki)
    {
      cerr << "Error\n";
      //exit(1);
    }

 if(type==2||type==4)
    {
      ofstream Hk("Hk.mtx", ios::out);
      ofstream Hkge("Hkge.mtx", ios::out);
      ofstream Hkbgo("Hkbgo.mtx", ios::out);
      ofstream Hkproj("Hk.pro", ios::out);
  
      if(!Hk)
	{
	  cerr << "Error\n";
	  exit(1);
	}

      //Opening the output files
      if(!Hkproj)
	{
	  cerr << "Error\n";
	  exit(1);
	}
      
      MatrixPrint MatrixFile(EMHki,Hk,Hkge,Hkbgo,Hkproj);
      MatrixFile.print2D(EMHki,Hk,Hkge,Hkbgo,Hkproj);
      
    }

 if(type==3||type==4)
   {
     //cout << "Type the output file name" << endl;
     //char * name;
     //cin >> name;
     ifstream EMHk2("EMHk.dat", ios::in);
     ofstream Fname("IE.mtx", ios::out);
     ofstream Pname("IE.pro", ios::out);
     
     Region RegionAnalysis(EMHk2,IE,Fname,Pname);
     RegionAnalysis.selection(EMHk2,IE,Fname,Pname);
     
   }


 if(type==0)
   {
     cout << "Do you have the GammaEvents.0000.01 file? \n yes -> 1 no -> 0" << endl;
     int yes1;
     cin >> yes1;
     cout << "Do you have the GammaEvents.0000.02 file? \n yes -> 1 no -> 0" << endl;
     int yes2;
     cin >> yes2;
     cout << "Do you have the GammaEvents.0000.03 file? \n yes -> 1 no -> 0" << endl;
     int yes3;
     cin >> yes3;
     cout << "Do you have the GammaEvents.0000.04 file? \n yes -> 1 no -> 0" << endl;
     int yes4;
     cin >> yes4;
     
     cout << "Do you have the GammaEvents.0000.05 file? \n yes -> 1 no -> 0" << endl;
     int yes5;
     cin >> yes5;
     
     //Declaring the input files 
     cout << "Opening the input file " << argv[1] << endl; 
     ifstream input(argv[1], ios::in);
     cout << "Opening the input file GammaEvents.0000" << endl; 
     ifstream events("GammaEvents.0000",ios::in);
     
     //Opening the input file *.inp
     
     if(!input)
	{
	  cerr << "Input error with the *.inp file \n";
	  exit(1);
	}
     
     //Opening the input file GammaEvents.0000
     
      if(!events)
	{
	  cerr << "Input error with the GammaEvents.0000 file\n";
	  exit(1);
	}
      
      //Declaring the output files 
      int efficy[120], i=0;
      for(i=0;i<120;i++)
	efficy[i]=0;
      
      ArrayProp GaspFile(events, efficy);
      GaspFile.search_$(events);
      GaspFile.Efficiency(events, efficy);

      if(yes1==1)
	{
	  cout << "Opening the input file GammaEvents.0000.01" << endl; 
	  ifstream events01("GammaEvents.0000.01",ios::in);
	  
	  ArrayProp GaspFile1(events01, efficy);
	  GaspFile1.search_$(events01);
	  GaspFile1.Efficiency(events01, efficy);
	}
      if(yes2==1)
	{
	  cout << "Opening the input file GammaEvents.0000.02" << endl; 
	  ifstream events02("GammaEvents.0000.02",ios::in);
	  
	  ArrayProp GaspFile2(events02, efficy);
	  GaspFile2.search_$(events02);
	  GaspFile2.Efficiency(events02, efficy);
	}
      if(yes3==1)
	{
	  cout << "Opening the input file GammaEvents.0000.03" << endl; 
	  ifstream events03("GammaEvents.0000.03",ios::in);
	  
	  ArrayProp GaspFile3(events03, efficy);
	  GaspFile3.search_$(events03);
	  GaspFile3.Efficiency(events03, efficy);
	}
      if(yes4==1)
	{
	  cout << "Opening the input file GammaEvents.0000.04" << endl; 
	  ifstream events04("GammaEvents.0000.04",ios::in);
	  
	  ArrayProp GaspFile4(events04, efficy);
	  GaspFile4.search_$(events04);
	  GaspFile4.Efficiency(events04, efficy);
	}
      if(yes5==1)
	{
	  cout << "Opening the input file GammaEvents.0000.05" << endl; 
	  ifstream events05("GammaEvents.0000.05",ios::in);
	  
	  ArrayProp GaspFile5(events05, efficy);
	  GaspFile5.search_$(events05);
	  GaspFile5.Efficiency(events05, efficy); 
	}
      
      int effiT=0;
      for(i=0;i<120;i++)
	effiT+=efficy[i];
      
      cout << "The total trigering efficiency is: " << effiT << endl
	   << "The non-zero partial effitiencies are:" << endl; 
      
      for(i=0;i<120;i++)
	if(efficy[i]>0)
	  cout << i << ": " << efficy[i] << endl;
      
    }


    return 0;  
}


