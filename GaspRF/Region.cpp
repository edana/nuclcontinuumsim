/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>

#include "Region.h"

Region::Region(ifstream& EMHkfile, ifstream& IEfile, ofstream& Output, ofstream& Proj)
{
}

void Region::selection(ifstream& EMHkfile, ifstream& IEfile, ofstream& Output, ofstream& Proj)
{    
  cout << "Select the H(kev) and dH(keV)" << endl;
  cin >> H >> dH;
  
  cout << "Select the k and dk" << endl;
  cin >> k >> dk;
  
  for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	IE[i][j]=0;
    }
  
  for(i=0;i<100000;i++)
    {
      arrayE[i]=0;
    }
  
  int Enum=0, Inum=0;
  while(EMHkfile)
    {
      E=0;Eint=0;
      I=0;
      EMHkfile >> EnergyT >> M >> Htotal >> ktotal >> Hge >> kge >> Hbgo >> kbgo;
      IEfile >> I >> E;
     
      
      if(Htotal<=H+dH && Htotal>=H-dH && ktotal<=k+dk && ktotal>=k-dk)
	{
	  Eint=(int)((rint)(E/1000));
	  IE[I][Eint]+=1;
	  arrayE[Enum]=Eint;
	  arrayI[Inum]=I;
	  Enum++;
	  Inum++;
	  //cout << Enum << endl;	  
	  //cout << I << "  " << E << ";" << Htotal << "  " << ktotal << endl;
	}

      if(Hge<=H+dH && Hge>=H-dH && kge<=k+dk && kge>=k-dk)
	{
	  Eint=(int)((rint)(E/1000));
	  IEge[I][Eint]+=1;
	  
	  //cout << I << "  " << Eint << ";" << Htotal << "  " << ktotal << endl;
	}
      
      if(Hbgo<=H+dH && Hbgo>=H-dH && kbgo<=k+dk && kbgo>=k-dk)
	{
	  Eint=(int)((rint)(E/1000));
	  IEbgo[I][Eint]+=1;
	  
	  //cout << I << "  " << Eint << ";" << Htotal << "  " << ktotal << endl;
	}

    }
  /************************************************************/
  /* Mean E*/
  for(i=0;i<100000;i++)
    {
      sumE+=arrayE[i];
    }
  meanE=sumE/(Enum-1);  
  cout << "mean E(MeV) =  " << meanE  << endl;

  /*Variance E*/
  for(i=0;i<10000;i++)
    {
      if(arrayE[i]>0)
	varE1+=pow((arrayE[i]-meanE),2);
    }
  varE=sqrt(varE1/(Enum-2));
  cout << "variance E =  " << varE << endl;

  /* Skewness E */
  for(i=0;i<10000;i++)
    {
      if(arrayE[i]>0)
	skewE1+=pow((arrayE[i]-meanE),3);
    }
  skewE=skewE1/((Enum-1)*pow(varE,3));
  cout << "skewness E =  " << skewE << endl;

 /* Kurtosis E */
  for(i=0;i<10000;i++)
    {
      if(arrayE[i]>0)
	kurtE1+=pow((arrayE[i]-meanE),4);
    }
  kurtE=kurtE1/((Enum-1)*pow(varE,4))-3;
  cout << "kurtosis E =  " << kurtE << endl;
  /******************************************************************/

/* Mean I*/
  for(i=0;i<100000;i++)
    {
      sumI+=arrayI[i];
    }
  meanI=sumI/(Inum-1);  
  cout << "mean I =  " << meanI  << endl;

  /*Variance I*/
  for(i=0;i<10000;i++)
    {
      if(arrayI[i]>0)
	varI1+=pow((arrayI[i]-meanI),2);
    }
  varI=sqrt(varI1/(Inum-2));
  cout << "variance I =  " << varI << endl;

  /* Skewness I */
  for(i=0;i<10000;i++)
    {
      if(arrayI[i]>0)
	skewI1+=pow((arrayI[i]-meanI),3);
    }
  skewI=skewI1/((Inum-1)*pow(varI,3));
  cout << "skewness I =  " << skewI << endl;

 /* Kurtosis I */
  for(i=0;i<10000;i++)
    {
      if(arrayI[i]>0)
	kurtI1+=pow((arrayI[i]-meanI),4);
    }
  kurtI=kurtI1/((Inum-1)*pow(varI,4))-3;
  cout << "kurtosis I =  " << kurtI << endl;

  /******************************************************************/




  //For the Htotal and ktotal
  for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	{
	  Iproj[i]+=IE[i][j];
	}
    }

  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  Eproj[j]+=IE[i][j];
	}
    }
  
  Imax=0;
  for(i=0;i<60;i++)
    {
      if(Iproj[i]>Imax)
	Imax=Iproj[i];
    }

  Emax=0;
  for(i=0;i<60;i++)
    {
      if(Eproj[i]>Emax)
	Emax=Eproj[i];
    }

   for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	Output << IE[i][j] << "  ";
      Output << endl;
    }

  // For the Hge and kge
  
  for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	{
	  Ige_proj[i]+=IEge[i][j];
	}
    }
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  Ege_proj[j]+=IEge[i][j];
	}
    }
  
  Ige_max=0;
  for(i=0;i<60;i++)
    {
      if(Iproj[i]>Ige_max)
	Ige_max=Ige_proj[i];
    }
  
  Ege_max=0;
  for(i=0;i<60;i++)
    {
      if(Ege_proj[i]>Ege_max)
	Ege_max=Ege_proj[i];
    }
  
  ofstream Output_ge("IEge.mtx", ios::out);
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	Output_ge << IEge[i][j] << "  ";
      Output_ge << endl;
    }
  
  //For the Hbgo and Hbgo
  
 for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	{
	  Ibgo_proj[i]+=IEbgo[i][j];
	}
    }

  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  Ebgo_proj[j]+=IEbgo[i][j];
	}
    }
  
  Ibgo_max=0;
  for(i=0;i<60;i++)
    {
      if(Ibgo_proj[i]>Ibgo_max)
	Ibgo_max=Ibgo_proj[i];
    }

  Ebgo_max=0;
  for(i=0;i<60;i++)
    {
      if(Ebgo_proj[i]>Ebgo_max)
	Ebgo_max=Ebgo_proj[i];
    }

  ofstream Output_bgo("IEbgo.mtx", ios::out);
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	Output_bgo << IEbgo[i][j] << "  ";
      Output_bgo << endl;
    }
  
  //Writing the projections for the 3 matrix
  
  for(i=0;i<60;i++)
    {
      Proj << i << "  " << Iproj[i]/Imax << "  " << Eproj[i]/Emax 
	   << "  " << Ige_proj[i]/Ige_max << "  " << Ege_proj[i]/Ege_max
	   << "  " << Ibgo_proj[i]/Ibgo_max << "  " << Ebgo_proj[i]/Ebgo_max << endl;

    }
  
}

