/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>
#include<cmath>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matrix/include/matrix.h"
#include "MomentsDistNormal.h"

#ifndef _NO_NAMESPACE
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()    try {
#  define CATCHERROR()  } catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif

typedef matrix<double> Matrix;


MomentsDistNormal::MomentsDistNormal(Matrix input) 
{
  for(j=0;j<60;j++) {
    Hproj[j]=0;
    kproj[j]=0;
  }
  
  ReadMatrix(input);
  MomentsCalcH();
  MomentsCalck();
}

void MomentsDistNormal::ReadMatrix(Matrix input)
{
  for(j=0;j<60;j++) { 
    for(i=0;i<60;i++) {  
      Hproj[j]+=input(j,i);
    }
  }
  
  for(i=0;i<60;i++) {
    for(j=0;j<60;j++) {
      kproj[i]+=input(j,i);
    }
  }
}

/************************************************************/
void MomentsDistNormal::MomentsCalcH()
{
  sumH=meanH=varH=sigmaH=0;
  
  /* Mean H*/
  for(i=0;i<60;i++)
    sumH+=Hproj[i]*i;
  
  meanH=sumH;  
  cout << "mean H(keV) =  " << meanH  << endl;
  
  /*Variance H*/
  for(i=0;i<60;i++)
    varH+=Hproj[i]*pow((i-meanH),2);
    
  sigmaH=sqrt(varH);
  cout << "sigma H(keV) =  " << sigmaH << endl;
}

/******************************************************************/

void MomentsDistNormal::MomentsCalck()
{
  sumk=meank=vark=sigmak=0;

  /* Mean k*/
  for(i=0;i<60;i++)
    sumk+=kproj[i]*i;
    
  meank=sumk;  
  cout << "mean k =  " << meank  << endl;
  
  /*Variance k*/
  for(i=0;i<60;i++)
    vark+=kproj[i]*pow((i-meank),2);
 
  sigmak=sqrt(vark);
  cout << "sigma k =  " << sigmak << endl;
  
   
  /******************************************************************/
}




