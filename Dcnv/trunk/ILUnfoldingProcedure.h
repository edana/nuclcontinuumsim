/********/
#ifndef ILUNFOLDINGPROCEDURE_H
#define ILUNFOLDINGPROCEDURE_H

class ILUnfoldingProcedure
{
 public:
   ILUnfoldingProcedure();

  void fitHkdistr(int *, double *, int *, double *, int);
  void InvTransT(int, int, int *);
 private:
  double maxQ, maxR1, sumR1, maxP2;
  int Emax, Mmax, Hmax, kmax, HEdif, Mkdif;
  int i,j,s,l, numEvt;
  int Mint,Eint,Hint,kint;
  int Mant; 
  double Egamma, Eant; 
  double var1,var2,var3;
};

#endif
