#ifndef MOMENTSDISTNORMAL_H
#define MOMENTSDISTNORMAL_H

#include "matrix/include/matrix.h"

#ifndef _NO_NAMESPACE
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()    try {
#  define CATCHERROR()  } catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif

typedef matrix<double> Matrix;

class MomentsDistNormal
{
 public:
  MomentsDistNormal(Matrix);
  void ReadMatrix(Matrix);
  void MomentsCalcH();
  void MomentsCalck();
 private:
  int i, j;
  float Hproj[60], kproj[60];
  float sumH, meanH, varH, sigmaH;
  float sumk, meank, vark, sigmak;
};

#endif
