/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>
#include<cmath>

#include "MtxNorm.h"
#include "matrix/include/matrix.h"

#ifndef _NO_NAMESPACE
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()    try {
#  define CATCHERROR()  } catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif

typedef matrix<double> Matrix;


MtxNorm::MtxNorm()
{
    for(i=0;i<60;i++)
    {
	for(j=0;j<60;j++)
	    Hkmatrix[i][j]=0;
    }
}

int * MtxNorm::maxFind(ifstream& input)
{
    kmax=Hmax=0;
    maxMtx=0;
    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  input >> Hkmatrix[i][j];
      }

    for(j=0;j<60;j++)
    {
	for(i=0;i<60;i++)
	{
	    if(Hkmatrix[i][j]>maxMtx)
	    {
		maxMtx=Hkmatrix[i][j];
		kmax=i;
		Hmax=j;
	    }
	}
    }
    
    cent[0]=kmax;
    cent[1]=Hmax;
    cent[2]=(int)maxMtx;

    centPtr=cent;

    input.seekg(0, ios::beg);

    return centPtr;
}

void MtxNorm::subsMtx(ifstream& input1, ifstream& input2, ofstream& output)
{
    
    Matrix mtxIn1(60,60);
    Matrix mtxIn2(60,60);
    Matrix mtxOut(60,60);

    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  mtxOut(i,j)=0;
      }
        
    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  input1 >> mtxIn1(i,j);
      }
    
    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  input2 >> mtxIn2(i,j);
      }
    
    TRYBEGIN()
        {
	    mtxOut=mtxIn1-mtxIn2;
	}
    CATCHERROR();
    
    for(j=0;j<60;j++)
    {
	for(i=0;i<60;i++)
	    if(mtxOut(i,j)<10)
		mtxOut(i,j)=0;
    }

    
    for(j=0;j<60;j++)
    {
	for(i=0;i<60;i++)
	    output << mtxOut(i,j) << " ";
	output << endl;
    }


}


bool MtxNorm::nullFind(ifstream& input)
{
  Matrix mtx1(60,60);
  bool nullInfo;
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	input >> mtx1(i,j);
    }
  
  nullInfo=mtx1.IsNull();
    
  return nullInfo;
}


void MtxNorm::fitHkdistr(int *k, double *H, int *M, double *E, int num)
{
  double maxEM, maxHk ;
  int Emax, Mmax, Hmax, kmax, HEdif, Mkdif;
  int i,j,s,l, numEvt;
  int Mint,Eint,Hint,kint;
  
  Matrix R1(60,60);
  Matrix Q(60,60);
  Matrix Hk2distr(60,60);
  Matrix Pmtx(60,60);
  Matrix P2mtx(60,60);
  Matrix Fmtx(60,60);
  Matrix Rmtx(60,60);
  Matrix R2mtx(60,60);
  Matrix chiHk(60,60);
  
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      Q(i,j)=0;
      Hk2distr(i,j)=0;
      R1(i,j)=0;
      Pmtx(i,j)=0;
      P2mtx(i,j)=0;
      Fmtx(i,j)=0;
      Rmtx(i,j)=0;
      R2mtx(i,j)=0;
      chiHk(i,j)=0;
    }
  }
  
  //Building the matrices from the arrays.
  
  for(int l=0;l<num;l++) {
    Eint=(int)(rint(E[l]/1000));
    Mint=M[l];
    if(Eint>0&&Mint>0) 
      EMdistr(Mint,Eint)+=1;
    Mint=0;Eint=0;
  }
  
  for(int l=0;l<num;l++) {
    Hint=(int)(rint(H[l]/1000));
    kint=k[l];
    if(Hint>0&&kint>0) 
      Hkdistr(kint,Hint)+=1;
    kint=0;Hint=0;
  }

}

