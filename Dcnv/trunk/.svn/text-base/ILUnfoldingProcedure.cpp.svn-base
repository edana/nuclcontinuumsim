/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>
#include<cmath>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "ILUnfoldingProcedure.h"
#include "matrix/include/matrix.h"
#include "FirstTrial.h"
#include "MomentsDistNormal.h"

#ifndef _NO_NAMESPACE
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()    try {
#  define CATCHERROR()  } catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif

typedef matrix<double> Matrix;


ILUnfoldingProcedure::ILUnfoldingProcedure() 
{
  srand ( time(NULL) );
}

void ILUnfoldingProcedure::fitHkdistr(int *k, double *H, int *M, double *E, int num)
{
  Matrix R1mtx(60,60);
  Matrix Rp1mtx(60,60);
  Matrix R1amtx(60,60);
  Matrix Qmtx(60,60);
  Matrix Qpmtx(60,60);
  Matrix Hk2distr(60,60);
  Matrix Pmtx(60,60);
  Matrix P1mtx(60,60);
  Matrix P2mtx(60,60);
  Matrix P2cent(60,60);
  Matrix Fmtx(60,60);
  Matrix B1mtx(60,60);
  Matrix R2mtx(60,60);
  Matrix F2mtx(60,60);

  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      Qmtx(j,i)=0;
      Qpmtx(j,i)=0;
      Hk2distr(j,i)=0;
      R1mtx(j,i)=0;
      Rp1mtx(j,i)=0;
      R1amtx(j,i)=0;
      Pmtx(j,i)=0;
      P1mtx(j,i)=0;
      P2mtx(j,i)=0;
      Fmtx(j,i)=0;
      B1mtx(j,i)=0;
      R2mtx(j,i)=0;
      F2mtx(j,i)=0;
    }
  }
  
  //Building the matrices from the arrays.
  
  for(i=0;i<num;i++) {
    Eint=(int)(rint(E[i]/1000));
    Mint=M[i];
    if(Eint>0&&Mint>0&&Eint<60&&Mint<60) 
      R1mtx(Eint,Mint)+=1; /////////////////
    Mint=0;Eint=0;
  }
  
  for(i=0;i<num;i++) {
    Hint=(int)(rint(H[i]/1000));
    kint=k[i];
    if(Hint>0&&kint>0&&Eint<60&&Mint<60) 
      Qmtx(Hint,kint)+=1;
    kint=0;Hint=0;
  }
  
  //Normalization is important to operate later 
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      maxQ+=Qmtx(j,i);
      maxR1+=R1mtx(j,i); 
      sumR1+=R1mtx(j,i);    //Sum of the R1 probabilities 
      }
    }
  
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      Qmtx(j,i)=Qmtx(j,i)/maxQ;
      R1mtx(j,i)=R1mtx(j,i)/maxR1;
    }
  }


  // ************ First Fit **************

  //Open the P(EM,Hk) files for all M>2 and E>2MeV 
  
  string gui( "_" );
  string mtxExt( ".mtx" );
  string rrfExt( ".rrf" );
  int Ener=0;   
  
  for(s=2;s<51;s++) {
    for(l=2;l<51;l++) {
      Ener=s*1000;
      ostringstream BigName;
      BigName << Ener << gui << l << mtxExt;
      string more=BigName.str();
      ifstream PFile(more.c_str(), ios::in);
      PFile.seekg(0);
      
      if(PFile)	{
	for(j=0;j<60;j++)
	  for(i=0;i<60;i++) 
	    PFile >> Pmtx(j,i);
	
	TRYBEGIN()
	  {
	    Fmtx+=Pmtx*R1mtx(s,l);
	  }
	CATCHERROR();	
      }
    }
  }

  double maxF=0;
  //Normalization of Fmtx
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      maxF+=Fmtx(j,i);
    }
  }
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      Fmtx(j,i)=Fmtx(j,i)/maxF;
    }
  }

  //***************** Start the iteration process ...... ****************************
  int w=0;
  for(w=0;w<1;w++) {
    
    // ************ Calculate the ratio matrix **************
    // The ratio should be calculated using F1(k,H) in the (M,E) space.
    
    //Transforming R'1(M,E)=F1(k,H)
    int *newArray = new int[2];
    int ip=0,jp=0,p=0, numDistr=4;
    double maxRp1=0, maxR1a=0;
    
    newArray[0]=0;
    newArray[1]=0;

    for(j=1;j<60;j++) {
      for(i=1;i<60;i++) { 
	if(Fmtx(j,i)!=0) {
	  InvTransT(i,j,newArray);
	  ip=newArray[0];
	  jp=newArray[1];
	  
	  if((ip>0&&ip<60)&&(jp>=0&&jp<60))
	    Rp1mtx(jp,ip)+=Fmtx(j,i)/numDistr;
	  
	  for(p=0;p<numDistr;p++) {
	    ip=newArray[0];
	    jp=newArray[1];
	    //Distribute the events with a Gaussian bivariate funtion non-correlated
	    /* initialize random seed: */ 
	    double Enew=0, sigmaE=0.8, fcE=0, cte=0;
	    double Mnew=0, sigmaM=0.8, fcM=0, rE=0, rM=0;  
	    rE=(double)(rand()%100)/(double)100;
	    rM=(double)(rand()%100)/(double)100;
	    
	    while(fcE<rE) {
	      cte=1/(8*sqrt(2*3.14159)*sigmaE);
	      fcE+=cte*exp(-pow((Enew-jp),2)/(2*sigmaE*sigmaE));
	      Enew+=0.01;
	    }
	    jp=(int)(floor(Enew+0.5));
	    
	    while(fcM<rM) {
	      cte=1/(8*sqrt(2*3.14159)*sigmaM);
	      fcM+=cte*exp(-pow((Mnew-ip),2)/(2*sigmaM*sigmaM));
	      Mnew+=0.01;
	    }
	    ip=(int)(floor(Mnew+0.5));
	    
	    if((ip>0&&ip<60)&&(jp>0&&jp<60))
	      Rp1mtx(jp,ip)+=Fmtx(j,i)/numDistr;
	  }
	}
      }
    }
    
    //Normalization of Rp1
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	maxRp1+=Rp1mtx(j,i);
      }
    }
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	Rp1mtx(j,i)=Rp1mtx(j,i)/maxRp1;
      }
    }
    
    TRYBEGIN()
      {
	//Calculate the ratio
	for(j=0;j<60;j++) {
	  for(i=0;i<60;i++) { 
	    if(Rp1mtx(j,i)!=0&&R1mtx(j,i)!=0)
	      B1mtx(j,i)=R1mtx(j,i)/Rp1mtx(j,i);
	  }
	}
	
	//Calculate the first inprovement R1a
	for(j=0;j<60;j++) {
	  for(i=0;i<60;i++) { 
	    R1amtx(j,i)=B1mtx(j,i)*R1mtx(j,i);
	  }
	}
      }
    CATCHERROR();	
    
    //Normalization of R1a
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	maxR1a+=R1amtx(j,i);
      }
    }
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	R1amtx(j,i)=R1amtx(j,i)/maxR1a;
      }
    }
    
    // ************ First Estimate **************
    //Open the P'(Hk,EM) files for all M>2 and E>2MeV 
    ofstream PFile2("Pprime1.tbl", ios::out);
    
    // cout << " k\tH\t <M>\t<E>" << endl;
    for(s=2;s<51;s++) {
      for(l=2;l<51;l++) {
	Ener=s*1000;
	ostringstream BigName1;
	BigName1 << Ener << gui << l << gui << "Hk" << mtxExt;
	string more1=BigName1.str();
	ifstream PFile1(more1.c_str(), ios::in);
	PFile1.seekg(0);
	
	for(j=0;j<60;j++)
	  for(i=0;i<60;i++)
	    P1mtx(j,i)=0;
	
	if(PFile1){
	  for(j=0;j<60;j++)
	    for(i=0;i<60;i++) 
	      PFile1 >> P1mtx(j,i);
	  
	  for(j=0;j<60;j++)
	    for(i=0;i<60;i++) 
	      P2mtx(j,i)=R1mtx(j,i)*P1mtx(j,i);
	  
	  
	  //Create a table in a file with the mean values of (M,E) for each (k,H)
	  /*Calculating the mean values of P2mtx*/
	  double Msum=0,Esum=0;	
	  double Mxpt=0, Expt=0;
	  double Marray[60],Earray[60];
	  for(i=0;i<60;i++) { 
	    Marray[i]=0;
	    Earray[i]=0;
	  }
	  
	  for(j=0;j<60;j++) {
	    for(i=0;i<60;i++) { 
	      Marray[i]+=P2mtx(j,i);
	      Earray[j]+=P2mtx(j,i);
	    }
	  }
	  
	  for(i=0;i<60;i++) { 
	    Msum+=Marray[i];
	    Esum+=Earray[i];
	    Mxpt+=i*Marray[i];
	    Expt+=i*Earray[i];
	  }
	  
	  Mxpt=Mxpt/Msum;
	  Expt=Expt/Esum;
	  
	  if((Msum>0)&&(Esum>0))	
	    PFile2 << l << "\t" << s  << "\t " << floor(Mxpt+0.5) << "\t" << floor(Expt) << endl;
	}
      }
    }
    

    PFile2.close();
    
    // ************ Second Estimate **************
    // Transforming Q(k,H) using the table P'1(kH<-ME)
    ifstream PFileT("Pprime1.tbl", ios::in);
    int ktbl=0,Htbl=0,Mtbl=0,Etbl=0, ok=0;
    
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) { 
	if(Qmtx(j,i)>0) {
	  PFileT.seekg(0);
	  ok=0;
	  while(ok==0) {
	    PFileT >> ktbl >> Htbl >> Mtbl >> Etbl;
	    if((ktbl==j)&&(Htbl==i)) { 
	      Qpmtx(Mtbl,Etbl)=Qmtx(j,i);
	      ok=1;
	    }
	    if(Htbl>i)
	      ok=1;
	  }
	}
      }
    }
    
    PFileT.close();
    
    //cout << "ok" << endl;

    //The multiplication between the data and the ratio matrix point by point 
    for(j=0;j<60;j++)
      for(i=0;i<60;i++) 
	R2mtx(j,i)=Qpmtx(j,i)*B1mtx(j,i);  

    double sumR2=0;
    //Normalization of R2mtx
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	sumR2+=R2mtx(j,i);
      }
    }

    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	R2mtx(j,i)=R2mtx(j,i)/sumR2;
      }
    }
    
    //Apply the response funtion P(M,E) on R2(M,E)
    for(s=2;s<51;s++) {
      for(l=2;l<51;l++) {
	Ener=s*1000;
	ostringstream BigName;
	BigName << Ener << gui << l << mtxExt;
	string more=BigName.str();
	ifstream PFile3(more.c_str(), ios::in);
	PFile3.seekg(0);
	
	if(PFile3)	{
	  for(j=0;j<60;j++)
	    for(i=0;i<60;i++) 
	      PFile3 >> Pmtx(j,i);
	  
	  TRYBEGIN()
	    {
	      F2mtx+=Pmtx*R2mtx(s,l);
	    }
	  CATCHERROR();	
	}
      }
    }
    
    //Normalization of F2mtx
    double sumF2=0;
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	sumF2+=F2mtx(j,i);
      }
    }

    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	F2mtx(j,i)=F2mtx(j,i)/sumF2;
      }
    }

    //Calculate chi square
    double chisqr=0, chisqr_old=1,delta_chisqr=1;
    
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) { 
	chisqr+=pow(F2mtx(j,i)-Qmtx(j,i),2);
      }
    }
    
    delta_chisqr=fabs(chisqr-chisqr_old)/chisqr_old;
    chisqr_old=chisqr;
    cout << "chisqr=" << chisqr << endl;    

    TRYBEGIN()
      {
	Fmtx=F2mtx;
	R1mtx=R2mtx;
      }
    CATCHERROR();
  }
  //cout << F2mtx;

  //Calculate the distribution moments
  cout << "Distribution moments for Q" << endl;
  MomentsDistNormal Qmts(Qmtx);
  cout << endl;
  cout << "Distribution moments for R2" << endl;
  MomentsDistNormal R2mts(R2mtx); 
  cout << endl;
  cout << "Distribution moments for F2" << endl;
  MomentsDistNormal F2mts(F2mtx);
  
}

//Inverse Transformation (T^{-1}) to use in (M,E)=T^{-1}(k,H)
void ILUnfoldingProcedure::InvTransT(int kp,int Hp,int *MpEpArray)
{
  int Mp=MpEpArray[0];
  double Ep=MpEpArray[1];
  const int numDet = 120;
  Mp=kp;
  Ep=static_cast<int>(floor((Hp/0.75)+0.5));
  Egamma=Ep/static_cast<double>(Mp); //?????????? This is not true in general
  
  //Call the corrected efficiency funcion 
  FirstTrial cEffi;
  double correcEffi = cEffi.CorrectedEfficiency(Egamma);
  //Call the generalized efficiency funcion
  double generEffi = cEffi.GeneralizedEfficiency(Egamma);
  
  //******* i-th trial ********
  while((Mp-Mant)>0||(Ep-Eant)>0.01){   
    Mant=Mp;
    Eant=Ep;
    var1=(1-static_cast<double>(kp)/static_cast<double>(numDet));
    var2=static_cast<double>(1-correcEffi/numDet);
    var3=log(var1)/log(var2);
    Mp=static_cast<int>(floor(var3+0.5));
    Ep=Hp/generEffi;
    
    Egamma=Ep/static_cast<double>(Mp); //?????????? This is not true in general
    correcEffi=cEffi.CorrectedEfficiency(Egamma);
    generEffi=cEffi.GeneralizedEfficiency(Egamma);
  }

  MpEpArray[0]=Mp;
  MpEpArray[1]=(int)Ep;
}


	
