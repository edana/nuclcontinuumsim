/******************************************************************************************//**
 * @file /home/edana/gfnun/MySoftware2/Dcnv/trunk/main.cpp
 * @brief Main file of the deconvolution code
 *
 * MAIN file, the run format is > Dcnv *.out it deconvolutes the file *.out    
 */

#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>
#include "FirstTrial.h"
#include "ILUnfoldingProcedure.h"

main(int argc, char * argv[])
{
  ifstream dcnvFile(argv[1], ios::in);
  
  int numCasc=0,i,k,gek,bgok;
  double H,geH,bgoH;

  const int SIZE = 50; 
  char buffer[ SIZE  ]; 

  //Cheking the number of arguments
  if(argc!=2) {
    cerr << "Error -- Use Dcnv *.out" << endl;
    exit(1);
  }
  
  //Cheking the files 
  if(!dcnvFile) {
    cerr << "Error opening the file" << endl;
    exit(1);
  } 
  
  //Reading the number of cascades in the distribution
  while(!dcnvFile.eof()) {
    dcnvFile.getline( buffer, SIZE );
    numCasc++;
  }
  
  //Correction of the zero
  numCasc=numCasc-1;
  
  dcnvFile.close();
  //cout << "Number of cascades: " << numCasc << endl;
  
  //Storage arrays for each cascade
  int *karray = new int[ numCasc ];
  double *Harray = new double[ numCasc ];
  int *Marray = new int[ numCasc ];
  double *Earray = new double[ numCasc ];

  ifstream dcnvFile1(argv[1], ios::in); 
  
  for(i=0;i<numCasc;i++) {
    dcnvFile1 >> H >> k >> geH >> gek >> bgoH >> bgok;
    karray[i]=k;
    Harray[i]=H;
  }
  
  //***************************     1    ******************************//
  // Using the reverse mapping (k,H)->(<M>,<E>) as an initial estimate //

  FirstTrial first;
  first.general(karray,Harray,Marray,Earray,numCasc);
 
   //****************************     2    *******************************//
  // Solving the system by an Iterative Least-square Unfolding Procedure  //
  
  ILUnfoldingProcedure unfold;
  unfold.fitHkdistr(karray,Harray,Marray,Earray,numCasc); 
  
  // ofstream EMFile("EM.mtx", ios::out); 
  //MatrixPrint printEMmatrix;
  //printEMmatrix.print2D(numCasc, Marray, Earray, EMFile);
  
  // EMFile.close();
  dcnvFile1.close();
  return 0;
}
  

