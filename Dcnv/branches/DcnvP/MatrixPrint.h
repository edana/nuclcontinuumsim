/********/
#ifndef MATRIXPRINT_H
#define MATRIXPRINT_H

class MatrixPrint
{
 public:
  MatrixPrint();

  int print2D(ifstream&, ofstream&, ofstream&, int *);

 private:
  double H,Hge,Hbgo;
  int i,j,k,kge,kbgo,Hint,Hge_int,Hbgo_int;
  int Hkmatrix[120][60];
  int Hproj[120], kproj[60];
  int kmax, Hmax;
  
};

#endif
