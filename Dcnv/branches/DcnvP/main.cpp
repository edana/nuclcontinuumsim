/******************************************************************************************/
/**** MAIN file                                                                        ****/
/**** The run format is > DcnvP                                                        ****/
/**** Find the total projections of (M,E) for a given (k,H) pair                       ****/
/******************************************************************************************/

#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>

main()
{
  int i=0,j=0,H=0,k=0,E1=0,M1=0;
  double Mproj[60], Eproj[60];
  double mtxIn[60][60];
  double mtxOut[60][60];

  for(k=2;k<51;k++) {
    for(H=2;H<51;H+=1) {

      cout << "For: (" << k << "," << H << ")" << endl;
      double numEvents=0;
      string gui( "_" );
      string Hkstring( "_Hk" );
      string mtxExt( ".mtx" );
      ostringstream nameMtx;
      nameMtx << H*1000 << gui << k << Hkstring << mtxExt;
      string more=nameMtx.str();
      ofstream EMFile(more.c_str(), ios::out);
      if(!EMFile) {
	cerr << "Error opening the matrix output file " << endl;
	exit(1);
      }
      
      ostringstream nameProj;
      string proExt( ".pro" ); 
      nameProj << H*1000 << gui << k << Hkstring << proExt;
      string second=nameProj.str();
      ofstream proj(second.c_str(), ios::out);
      if(!proj) {
	cerr << "Error opening the projection output file " << endl;
	exit(1);
      }
      
      int EMdistr[60][60];
      for(j=0;j<60;j++) {
	for(i=0;i<60;i++) {
	  EMdistr[i][j]=0;
	  mtxOut[i][j]=0;
	}
      }
      
      for(i=0;i<60;i++) {
	Mproj[i]=0;
	Eproj[i]=0;
      }
      
      
      for(j=2;j<51;j++) {
	for(i=2;i<51;i++) {
	  int enerF=j*1000;
	  ostringstream BigName1;
	  BigName1 << enerF << gui << i << mtxExt;
	  string more1=BigName1.str();
	  ifstream HkFile(more1.c_str(), ios::in);
	  
	  if(HkFile) {
	    for(int m=0;m<60;m++)	{
	      for(int l=0;l<60;l++)
		HkFile >> mtxIn[l][m];
	    }
	    
	    if((mtxIn[k][H])>0) { 
	      mtxOut[i][j]+=mtxIn[k][H]*i;
	      numEvents+=mtxIn[k][H]*i;
	    }
	    HkFile.close();    
	  }
	}
      }
      
      EMFile.seekp(0);
      for(j=0;j<60;j++) {
	for(i=0;i<60;i++) {
	  if(numEvents!=0) {
	    EMFile << mtxOut[i][j]/numEvents << "  ";
	    Mproj[i]+=mtxOut[i][j]/numEvents;
	    Eproj[j]+=mtxOut[i][j]/numEvents;
	  }
	}
	EMFile << endl;
      }
      
      proj.seekp(0);
      for(i=0;i<60;i++)
	{
	  proj << i << "\t" << Eproj[i] << "\t" << Mproj[i] << "\t" 
	       << endl; 
	}
      EMFile.close();
      proj.close();
    }
  }
  return 0;
}
