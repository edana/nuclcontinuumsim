/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>


#include "MatrixPrint.h"

MatrixPrint::MatrixPrint()
{
}

int MatrixPrint::print2D(ifstream& input,ofstream& out,ofstream& projec, int *cent)
{
  for(i=0;i<120;i++)
    {
      for(j=0;j<60;j++)
	Hkmatrix[i][j]=0;
    }

  while(input)
    {
      input >> H >> k >> Hge >> kge >> Hbgo >> kbgo;
      
      Hint=(int)(rint(H/1000));
      if(H>0&&k>0)
	Hkmatrix[k][Hint]+=1;
      
      H=0;k=0;
    }
  
  int max=0;
   for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  out << Hkmatrix[i][j] << "  ";
	  kproj[i]+=Hkmatrix[i][j];
	  if(Hkmatrix[i][j]>max)
	    {
	      max=Hkmatrix[i][j];
	      kmax=i;
	      Hmax=j;
	    }
	}
      out << endl;
    }
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  Hproj[j]+=Hkmatrix[i][j];
	}
    }
  
  
  for(i=0;i<60;i++)
    {
      projec << i << "\t" << Hproj[i] << "\t" << kproj[i] << "\t" 
	     << endl; 
    }
  cent[0]=kmax;
  cent[1]=Hmax;
  cent[2]=max;
  
  return max;
}




