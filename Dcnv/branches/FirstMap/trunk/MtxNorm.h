/********/
#ifndef MTXNORM_H
#define MTXNORM_H

class MtxNorm
{
 public:
  MtxNorm();

  int *maxFind(ifstream&);
  void subsMtx(ifstream&, ifstream&, ofstream&);
  bool nullFind(ifstream& input);
  void fitHkdistr(ifstream&, ifstream&);

 private:
  int i,j;
  double Hkmatrix[60][60];
  int kmax, Hmax;
  double maxMtx;
  int cent[3], *centPtr;
};

#endif
