/******************************************************************************************/
/**** MAIN file                                                                        ****/
/**** The run format is > Dcnv *.mtx                                                   ****/
/**** Deconvolutes the file *.mtx                                                      ****/
/******************************************************************************************/

#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;
using std::getline;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>
#include<math.h>
#include "MtxNorm.h"
#include "MatrixPrint.h"

double CorrectedEfficiency(double);
double GeneralizedEfficiency(double);

main(int argc, char * argv[])
{

  ifstream dcnvFile(argv[1], ios::in); 
  
  int i=0,j=0, numCasc=0;
  int k,gek,bgok, Mant=0; 
  double H,geH,bgoH, Egamma, Eant=0; 
  double var1,var2,var3;
  const int numDet = 120;
  const int SIZE = 50; 
  char buffer[ SIZE  ]; 

 //Cheking the number of arguments
  if(argc!=2)
    {
      cerr << "Error -- Use Dcnv *.mtx" << endl;
      exit(1);
    }  
  
  //Cheking the files 
  if(!dcnvFile)
    {
      cerr << "Error opening the file" << endl;
      exit(1);
    } 

  //Reading the number of cascades in the distribution
  while(!dcnvFile.eof())
    {
      dcnvFile.getline( buffer, SIZE );
      numCasc++;
    }
  //Correction of the zero
  numCasc=numCasc-1;
  
  dcnvFile.close();
  cout << "Number of cascades: " << numCasc << endl;
  
  //Creating two array with k and H of each cascade
  int *karray = new int[ numCasc ];
  double *Harray = new double[ numCasc ];
  int *Marray = new int[ numCasc ];
  double *Earray = new double[ numCasc ];
  
  /***************** Mapping (k,H)->(<M>,<E>) ******************************/
  
  ifstream dcnvFile1(argv[1], ios::in); 
  
  for(i=0;i<numCasc;i++) {
    dcnvFile1 >> H >> k >> geH >> gek >> bgoH >> bgok;
    karray[i]=k;
    Harray[i]=H;
  }
  
  //******* First trial *********
  for(i=0;i<numCasc;i++) {
    Marray[i]=karray[i];
    Earray[i]=Harray[i]/0.75;
    Egamma=Earray[i]/static_cast<double>(Marray[i]); //?????????? This is not true in general

    //Call the corrected efficiency funcion 
    double correcEffi= CorrectedEfficiency(Egamma);
    //Call the generalized efficiency funcion
    double generEffi=GeneralizedEfficiency(Egamma);
    
    //  cout << Egamma << "\t" << generEffi << endl;
    
    //******* i-th trial ********
    while((Marray[i]-Mant)>0||(Earray[i]-Eant)>0.01){   
      Mant=Marray[i];
      Eant=Earray[i];
      var1=(1-static_cast<double>(karray[i])/static_cast<double>(numDet));
      var2=static_cast<double>(1-correcEffi/numDet);
      var3=log(var1)/log(var2);
      Marray[i]=static_cast<int>(var3);
      Earray[i]=Harray[i]/generEffi;
      
      Egamma=Earray[i]/static_cast<double>(Marray[i]); //?????????? This is not true in general
      correcEffi= CorrectedEfficiency(Egamma);
      generEffi=GeneralizedEfficiency(Egamma);
      
    }
  }
  ofstream EMFile("EM.mtx", ios::out); 
  MatrixPrint printEMmatrix;
  printEMmatrix.print2D(numCasc, Marray, Earray, EMFile);

  EMFile.close();
  dcnvFile1.close();
  return 0;
}
  
double CorrectedEfficiency(double energy)
{
  double omega1,omega2,omega3,omega4,omega5,CE;

  energy=energy/1000;
  
  omega1=-0.1614*log10(energy)+0.7537;
  omega2=-0.3206*exp(-energy/2.20)+0.320;
  omega3=0.0126*energy-0.0062;
  omega4=0.0034*energy-0.0052;
  omega5=0.00071*energy -0.0013;
  
  if(omega1<0)
    omega1=0;
  if(omega2<0)
    omega2=0;
  if(omega3<0)
    omega3=0;
  if(omega4<0)
    omega4=0;
  if(omega5<0)
    omega5=0;
  
  CE=omega1+2*omega2+3*omega3+4*omega4+5*omega5;
  
  return CE;
}

double GeneralizedEfficiency(double energy)
{
  double GE;

  energy=energy/1000;
  if(energy<2000){
    GE=-0.0749*energy+0.8135;
  }
  else{
    GE=0.00215*energy+0.6404;
  }
  
  return GE;
}

