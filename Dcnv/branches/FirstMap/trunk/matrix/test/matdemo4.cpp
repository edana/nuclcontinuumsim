//////////////////////////////////////////////////////
// File: MatDemo3.cpp
// Copyright (c) 1997-2002 Techsoft Private Limited
// Purpose: Demonstrates how to use Matrix TCL Lite v1.13
// Web: http://www.techsoftpl.com/matrix/
// Email: matrix@techsoftpl.com
//

#include<string>

#include <stdexcept> 
using std::logic_error;

#include "matrix.h"
 
////////////////////////////////
// Note: The following conditional compilation statements are included
//       so that you can (likely to) compile this sample, without any 
//       modification, using a compiler which does not support any or 
//       all of the ANSI C++ features like NAMEPACE, TEMPLATE, and 
//       EXCEPTION, used in this class.
//
//       If you have a compiler, such as C++ Builder, Borland C++ 5.0,
//       MS Visual C++ 5.0 or higher, etc., which supports most of the ANSI 
//       C++ features used in this class, you do not need to include these
//       statements in your program.
//

#ifndef _NO_NAMESPACE
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()	try {
#  define CATCHERROR()	} catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif

typedef matrix<double> Matrix;

int main ()
{
  int a=4;
  Matrix m(a,a);
  Matrix m1(a,a);
  
  TRYBEGIN()
    {
      for(int i=0;i<a;i++)
	for(int j=0;j<a;j++)
	  m(i,j)=i^2+4;
      
      cout << m;
      cout << endl;

      m1 = ~m;

      cout << m1;

    }
  CATCHERROR();
  
  return 0;
}
