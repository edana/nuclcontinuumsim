/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;
using std::fixed;

#include<fstream>
using std::ifstream;
using std::ofstream;
using std::fstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>
#include<cmath>

#include "MtxNorm.h"
#include "matrix/include/matrix.h"

#ifndef _NO_NAMESPACE
using namespace std;
using namespace math;
#define STD std
#else
#define STD
#endif

#ifndef _NO_EXCEPTION
#  define TRYBEGIN()    try {
#  define CATCHERROR()  } catch (const STD::exception& e) { \
                     cerr << "Error: " << e.what() << endl; }
#else
#  define TRYBEGIN()
#  define CATCHERROR()
#endif

typedef matrix<double> Matrix;


MtxNorm::MtxNorm()
{
    for(i=0;i<60;i++)
    {
	for(j=0;j<60;j++)
	    Hkmatrix[i][j]=0;
    }
}

int * MtxNorm::maxFind(ifstream& input)
{
    kmax=Hmax=0;
    maxMtx=0;
    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  input >> Hkmatrix[i][j];
      }

    for(j=0;j<60;j++)
    {
	for(i=0;i<60;i++)
	{
	    if(Hkmatrix[i][j]>maxMtx)
	    {
		maxMtx=Hkmatrix[i][j];
		kmax=i;
		Hmax=j;
	    }
	}
    }
    
    cent[0]=kmax;
    cent[1]=Hmax;
    cent[2]=(int)maxMtx;

    centPtr=cent;

    input.seekg(0, ios::beg);

    return centPtr;
}

void MtxNorm::subsMtx(ifstream& input1, ifstream& input2, ofstream& output)
{
    
    Matrix mtxIn1(60,60);
    Matrix mtxIn2(60,60);
    Matrix mtxOut(60,60);

    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  mtxOut(i,j)=0;
      }
        
    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  input1 >> mtxIn1(i,j);
      }
    
    for(j=0;j<60;j++)
      {
	for(i=0;i<60;i++)
	  input2 >> mtxIn2(i,j);
      }
    
    TRYBEGIN()
        {
	    mtxOut=mtxIn1-mtxIn2;
	}
    CATCHERROR();
    
    for(j=0;j<60;j++)
    {
	for(i=0;i<60;i++)
	    if(mtxOut(i,j)<10)
		mtxOut(i,j)=0;
    }

    
    for(j=0;j<60;j++)
    {
	for(i=0;i<60;i++)
	    output << mtxOut(i,j) << " ";
	output << endl;
    }


}


bool MtxNorm::nullFind(ifstream& input)
{
  Matrix mtx1(60,60);
  bool nullInfo;
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	input >> mtx1(i,j);
    }
  
  nullInfo=mtx1.IsNull();
    
  return nullInfo;
}


void MtxNorm::fitHkdistr(ifstream& Hk, ifstream& EM)
{
  double maxEM, maxHk ;
  int Emax, Mmax, Hmax, kmax, HEdif, Mkdif;
  int i,j,s,l, numEvt;

  Matrix EMdistr(60,60);
  Matrix Hkdistr(60,60);
  Matrix Hk2distr(60,60);
  Matrix Hkshift(60,60);
  Matrix Pmtx(60,60);
  Matrix P2mtx(60,60);
  Matrix Fmtx(60,60);
  Matrix Rmtx(60,60);
  Matrix R2mtx(60,60);
  Matrix chiHk(60,60);
  
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      Hkdistr(i,j)=0;
      Hk2distr(i,j)=0;
      EMdistr(i,j)=0;
      Hkshift(i,j)=0;
      Pmtx(i,j)=0;
      P2mtx(i,j)=0;
      Fmtx(i,j)=0;
      Rmtx(i,j)=0;
      R2mtx(i,j)=0;
      chiHk(i,j)=0;
    }
  }
  
  Hk.seekg(0);
  EM.seekg(0);
  
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      Hk >> Hkdistr(i,j);
      EM >> EMdistr(i,j);
    }
  }

  //Now we will use just the maximun value of the (M,E) distribution to shift the 
  //"experimental" (k,H) distribution 
    
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      if(EMdistr(i,j)>maxEM) {
	maxEM=EMdistr(i,j);
	Mmax=i;
	Emax=j;
      }
    }
  }

  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      if(Hkdistr(i,j)>maxHk) {
	maxHk=Hkdistr(i,j);
	kmax=i;
	Hmax=j;
      }
    }
  }

  //Normalization, just in case
  if(maxHk!=1) {
    for(j=0;j<60;j++) {
      for(i=0;i<60;i++) {
	Hkdistr(i,j)=Hkdistr(i,j)/maxHk;
      }
    }
  }
  
  
  HEdif=Hmax-Emax;
  Mkdif=kmax-Mmax;

  cout << "The differences: " << endl
       << Hmax << "-" << Emax << endl
       << kmax << "-" << Mmax << endl; 

  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      if((i-Mkdif)>0&&(j-Mkdif)>0&&(i-Mkdif)<60&&(j-Mkdif)<60)
	Hkshift(i,j)=Hkdistr(i-Mkdif,j-Mkdif);
    }
  }

  for(j=0;j<60;j++) {
    for(i=0;i<60;i++) {
      numEvt+=Hkshift(i,j);
    }
  }

  string gui( "_" );
  string mtxExt( ".mtx" );
  string rrfExt( ".rrf" );
  int Ener=0;  

  for(s=2;s<51;s++) {
    for(l=2;l<51;l++) {
      Ener=s*1000;
      ostringstream BigName1;
      BigName1 << Ener << gui << l << mtxExt;
      string more1=BigName1.str();
      //cout << "Opening the input file " << more1 << endl; 
      ifstream PFile(more1.c_str(), ios::in);
      PFile.seekg(0);

      if(PFile)	{
	for(j=0;j<60;j++)
	  for(i=0;i<60;i++) 
	    PFile >> Pmtx(i,j);


	TRYBEGIN()
	  {
	    Fmtx+=Pmtx*Hkshift;
	    
	    //New reverese response 
	    P2mtx=Hkshift*Pmtx;
	    for(j=0;j<60;j++)
	      for(i=0;i<60;i++) 
		P2mtx(i,j)=P2mtx(i,j)/numEvt;
	  }
	CATCHERROR();	

	ostringstream BigName2;
	BigName2 << Ener << gui << l << rrfExt;
	string more2=BigName2.str();
	ofstream PFileOut(more2.c_str(), ios::out);
	PFileOut.seekp(0);
	PFileOut << P2mtx;
	PFileOut.close();
      }
      PFile.close();
    }
  }

  TRYBEGIN()
    {
      //Calculate the ratio
      for(j=0;j<60;j++)
	for(i=0;i<60;i++) 
	  if(Fmtx(i,j)!=0)
	    Rmtx(i,j)=Hkshift(i,j)/Fmtx(i,j);
      
      //cout << "R matrix" << endl
      //	   << Rmtx << endl;
      
      //The second estimate
      R2mtx=Hkdistr*Rmtx;
      
    }
  CATCHERROR();	

  ofstream R2out("OUT.mtx", ios::out);  
  R2out << R2mtx;
  
  //Mapping 
  for(s=2;s<51;s++) {
    for(l=2;l<51;l++) {
      if(R2mtx(l,s)!=0) { 
	Ener=s*1000;
	ostringstream BigName4;
	BigName4 << Ener << gui << l << mtxExt;
	string more4=BigName4.str();
	ifstream PFile(more4.c_str(), ios::in);
	PFile.seekg(0);

	if(PFile) {
	  for(j=0;j<60;j++)
	    for(i=0;i<60;i++) 
	      PFile >> Pmtx(i,j);
	  Hk2distr+=Pmtx*R2mtx(l,s);
	}
	PFile.close();
      }
    }
  }
  
  //Finding chi-square
  for(j=0;j<60;j++) 
    for(i=0;i<60;i++)  
      if(Hkdistr(i,j)!=0)
	chiHk(i,j)=pow(Hk2distr(i,j)-Hkdistr(i,j),2)/Hkdistr(i,j);
  
  ofstream chiOut("chi.mtx", ios::out);  
  chiOut << chiHk;
   
  /********************* Second fit *************************/

  for(s=2;s<51;s++) {
    for(l=2;l<51;l++) {
      Ener=s*1000;
      ostringstream BigName3;
      BigName3 << Ener << gui << l << rrfExt;
      string more3=BigName3.str();
      //cout << "Opening the input file " << more1 << endl; 
      ifstream PFile(more3.c_str(), ios::in);
      PFile.seekg(0);

      if(PFile)	{
	for(j=0;j<60;j++)
	  for(i=0;i<60;i++) 
	    PFile >> Pmtx(i,j);
	
	
	TRYBEGIN()
	  {
	    Fmtx+=Pmtx*Hkshift;
	    
	    //New reverese response 
	    P2mtx=Hkshift*Pmtx;
	    for(j=0;j<60;j++)
	      for(i=0;i<60;i++) 
		P2mtx(i,j)=P2mtx(i,j)/numEvt;
	  }
	CATCHERROR();	

      }
      PFile.close();
    }
  }
  
  TRYBEGIN()
    {
      //Calculate the ratio
      for(j=0;j<60;j++)
	for(i=0;i<60;i++) 
	  if(Fmtx(i,j)!=0)
	    Rmtx(i,j)=Hkshift(i,j)/Fmtx(i,j);
      
      //cout << "R matrix" << endl
      //	   << Rmtx << endl;
      
      //The second estimate
      R2mtx=Hkdistr*Rmtx;
      
    }
  CATCHERROR();	

  R2out.seekp(0);
  R2out << R2mtx;

  //Mapping 
  for(s=2;s<51;s++) {
    for(l=2;l<51;l++) {
      if(R2mtx(l,s)!=0) { 
	Ener=s*1000;
	ostringstream BigName4;
	BigName4 << Ener << gui << l << mtxExt;
	string more4=BigName4.str();
	ifstream PFile(more4.c_str(), ios::in);
	PFile.seekg(0);

	if(PFile) {
	  for(j=0;j<60;j++)
	    for(i=0;i<60;i++) 
	      PFile >> Pmtx(i,j);
	  Hk2distr+=Pmtx*R2mtx(l,s);
	}
	PFile.close();
      }
    }
  }
  
  //Finding chi-square
  for(j=0;j<60;j++) 
    for(i=0;i<60;i++) 
      if(Hkdistr(i,j)!=0)
	chiHk(i,j)=pow(Hk2distr(i,j)-Hkdistr(i,j),2)/Hkdistr(i,j);
  
  chiOut.seekp(0);
  chiOut << chiHk;
   
  chiOut.close();
  R2out.close();
}

