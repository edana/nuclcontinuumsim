/**
 * @file /home/edana/gfnun/MySoftware2/Analysis/trunk/Region.cpp
 * @brief (M,E) selection 
 *
 **/#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<string>
using std::string;

#include <sstream> 
using std::ostringstream;

#include<cstdlib>
#include<cmath>

#include "Region.h"

RegionHk::RegionHk()
{
}

/// This function allows the user to select a small region in the \f$(k,H)\f$ by setting a \f$H\f$ and
/// \f$k\f$ values and their delta. This will take a region of \f$H\pm\Delta H\f$ 
void RegionHk::selectionHk(ifstream& EMHkfile, ifstream& IEfile, ifstream& EOSi)
{    
  cout << "Select the H(kev) and dH(keV)" << endl;
  cin >> H >> dH;
  
  cout << "Select the k and dk" << endl;
  cin >> k >> dk;

  string Hchar("H");
  string kchar("k");
  string gui("_");
  string eosExt(".eos");
  string mtxExt(".mtx");
  string proExt(".pro");

  ostringstream matrixFile;
  matrixFile << Hchar << H/1000 << gui << kchar << k << mtxExt;
  string mtxF=matrixFile.str();
  ofstream Output(mtxF.c_str(), ios::out);

  ostringstream eosFile;
  eosFile << Hchar << H/1000 << gui << kchar << k << eosExt;
  string eosF=eosFile.str();
  ofstream EOSo(eosF.c_str(), ios::out);
  
  ostringstream proFile;
  proFile << Hchar << H/1000 << gui << kchar << k << proExt;
  string proF=proFile.str();
  ofstream Proj(proF.c_str(), ios::out);

  //ofstream EOSo("EOS.spc", ios::out);
  //ofstream Output("IE.mtx", ios::out);
  //ofstream Proj("IE.pro", ios::out);  
 
  for(i=0;i<60;i++)
    for(j=0;j<60;j++)
      IE[i][j]=0;
  
  for(i=0;i<100000;i++)
    arrayE[i]=0;
  
  for(i=1;i<2000;i++)
    N1[i]=0;

  int Enum=0, Inum=0, max=0, numHighE=0, numLowE=0;
  int highE[15], lowE[15];
  
  for(i=0;i<15;i++) {
    highE[i]=0;
    lowE[i]=0;
 }
  
  while(EMHkfile) {
    E=0;Eint=0;
    I=0;
    EMHkfile >> EnergyT >> M >> Htotal >> ktotal >> Hge >> kge >> Hbgo >> kbgo >> numHighE >> numLowE;
    IEfile >> I >> E;
    EOSi >> max;

    if(Htotal<=H+dH && Htotal>=H-dH && ktotal<=k+dk && ktotal>=k-dk) {
      Eint=(int)((rint)(E/1000));
      IE[I][Eint]+=1;
      arrayE[Enum]=Eint;
      arrayI[Inum]=I;
      Enum++;
      Inum++;
      //For the EOS spectrum of the selected region
      max=int(floor(((max/10)+0.5)));
      if(max>2000) {
	cout << "Error the energy is out of the spectrum > 20Mev" << endl;
	cout << max*10 << "(keV)" << endl;
	//exit(1);
      }
      N1[max]++;
      highE[numHighE]++;
      lowE[numLowE]++;
      //cout << Enum << "\r";	  
      //cout << I << "  " << E << ";" << Htotal << "  " << ktotal << endl;
    }
    
    if(Hge<=H+dH && Hge>=H-dH && kge<=k+dk && kge>=k-dk) {
      Eint=(int)((rint)(E/1000));
      IEge[I][Eint]+=1;
      
      //cout << I << "  " << Eint << ";" << Htotal << "  " << ktotal << endl;
    }
    
    if(Hbgo<=H+dH && Hbgo>=H-dH && kbgo<=k+dk && kbgo>=k-dk) {
      Eint=(int)((rint)(E/1000));
      IEbgo[I][Eint]+=1;
      
      //cout << I << "  " << Eint << ";" << Htotal << "  " << ktotal << endl;
    }
  }
  //cout << endl;


  //Efficiency correction 
  float N1_ceff[2000], Eff=0;
  for(i=0;i<2000;i++) 
    N1_ceff[i]=0;
  
  for(i=1;i<2000;i++) {
    if(i<=200) 
      Eff=(-0.075*i/100)+0.81;
    else 
      Eff=(0.002*i/100)+0.64;
    N1_ceff[i]=N1[i]/Eff;
  }
  
  //Write the EOS spectrum
  for(i=1;i<2000;i++)
    EOSo << i*10 << "\t" << N1_ceff[i] << endl;
  

  /************************************************************/
  /* Mean E*/
  for(i=0;i<100000;i++)
    sumE+=arrayE[i];
  
  meanE=sumE/(Enum-1);  
  cout << "mean E(MeV) =  " << meanE  << endl;
  
  /*Variance E*/
  for(i=0;i<10000;i++)
    if(arrayE[i]>0)
      varE1+=pow((arrayE[i]-meanE),2);
    
  varE=sqrt(varE1/(Enum-2));
  cout << "variance E =  " << varE << endl;

  /* Skewness E */
  for(i=0;i<10000;i++)
    if(arrayE[i]>0)
      skewE1+=pow((arrayE[i]-meanE),3);
    
  skewE=skewE1/((Enum-1)*pow(varE,3));
  cout << "skewness E =  " << skewE << endl;

 /* Kurtosis E */
  for(i=0;i<10000;i++)
    if(arrayE[i]>0)
      kurtE1+=pow((arrayE[i]-meanE),4);
    
  kurtE=kurtE1/((Enum-1)*pow(varE,4))-3;
  cout << "kurtosis E =  " << kurtE << endl;
  /******************************************************************/

  /* Mean I*/
  for(i=0;i<100000;i++)
    sumI+=arrayI[i];
  
  meanI=sumI/(Inum-1);  
  cout << "mean I =  " << meanI  << endl;
  
  /*Variance I*/
  for(i=0;i<10000;i++)
    if(arrayI[i]>0)
      varI1+=pow((arrayI[i]-meanI),2);
    
  varI=sqrt(varI1/(Inum-2));
  cout << "variance I =  " << varI << endl;

  /* Skewness I */
  for(i=0;i<10000;i++)
    if(arrayI[i]>0)
      skewI1+=pow((arrayI[i]-meanI),3);
  
  skewI=skewI1/((Inum-1)*pow(varI,3));
  cout << "skewness I =  " << skewI << endl;
  
  /* Kurtosis I */
  for(i=0;i<10000;i++)
    if(arrayI[i]>0)
      kurtI1+=pow((arrayI[i]-meanI),4);
  
  kurtI=kurtI1/((Inum-1)*pow(varI,4))-3;
  cout << "kurtosis I =  " << kurtI << endl;
  
  /******************************************************************/
  
  //For the Htotal and ktotal
  for(i=0;i<60;i++)
    for(j=0;j<60;j++)
      Iproj[i]+=IE[i][j];
  
  for(j=0;j<60;j++)
    for(i=0;i<60;i++)
      Eproj[j]+=IE[i][j];
  
  Imax=0;
  for(i=0;i<60;i++)
    if(Iproj[i]>Imax)
      Imax=Iproj[i];
  
  Emax=0;
  for(i=0;i<60;i++)
    if(Eproj[i]>Emax)
      Emax=Eproj[i];
    
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++)
      Output << IE[i][j] << "  ";
    Output << endl;
  }

  // For the Hge and kge
  
  for(i=0;i<60;i++)
    for(j=0;j<60;j++)
      Ige_proj[i]+=IEge[i][j];
	  
  for(j=0;j<60;j++)
    for(i=0;i<60;i++)
      Ege_proj[j]+=IEge[i][j];
  
  Ige_max=0;
  for(i=0;i<60;i++)
    if(Iproj[i]>Ige_max)
      Ige_max=Ige_proj[i];
  
  Ege_max=0;
  for(i=0;i<60;i++)
    if(Ege_proj[i]>Ege_max)
      Ege_max=Ege_proj[i];
  
  ofstream Output_ge("IEge.mtx", ios::out);
  
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++)
      Output_ge << IEge[i][j] << "  ";
    Output_ge << endl;
  }
  
  //For the Hbgo and Hbgo
  
  for(i=0;i<60;i++)
    for(j=0;j<60;j++)
      Ibgo_proj[i]+=IEbgo[i][j];
	
  for(j=0;j<60;j++)
    for(i=0;i<60;i++)
      Ebgo_proj[j]+=IEbgo[i][j];
	  
  Ibgo_max=0;
  for(i=0;i<60;i++)
    if(Ibgo_proj[i]>Ibgo_max)
	Ibgo_max=Ibgo_proj[i];
  
  Ebgo_max=0;
  for(i=0;i<60;i++)
    if(Ebgo_proj[i]>Ebgo_max)
      Ebgo_max=Ebgo_proj[i];
  
  ofstream Output_bgo("IEbgo.mtx", ios::out);
  
  for(j=0;j<60;j++) {
    for(i=0;i<60;i++)
      Output_bgo << IEbgo[i][j] << "  ";
    Output_bgo << endl;
  }
  
  //Writing the projections for the 3 matrix
  
  for(i=0;i<60;i++)
    Proj << i << "  " << Iproj[i]/Imax << "  " << Eproj[i]/Emax 
	 << "  " << Ige_proj[i]/Ige_max << "  " << Ege_proj[i]/Ege_max
	 << "  " << Ibgo_proj[i]/Ibgo_max << "  " << Ebgo_proj[i]/Ebgo_max << endl;

  //Writing the high energy array
  
  cout << "The number of high energy E>2MeV in each cascade" << endl; 
  for(i=0;i<15;i++)
    cout << i << "\t" << highE[i] << "\t" << lowE[i] << endl;
  
}

