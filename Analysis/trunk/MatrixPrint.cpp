/**
 * @file /home/edana/gfnun/MySoftware2/Analysis/trunk/MatrixPrint.cpp
 * @brief Build and print the Hk matrix to a file
 *
 * This class reads the EMHk file and build the Hk matrix 
 **/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>


#include "MatrixPrint.h"

MatrixPrint::MatrixPrint(ifstream& input,ofstream& out,ofstream& outge,ofstream& outbgo,ofstream& projec) 
{
}

/// The input file is the EMHk.dat file, the outputs are the Hk matrix generated with both detectors types
/// and two other file for the Hk matrix generated with each detector type. Also there is a file that
/// contains the proyections of all tree matrices.  
void MatrixPrint::print2D(ifstream& input,ofstream& out,ofstream& outge,ofstream& outbgo,ofstream& projec)
{
  for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	Hkmatrix[i][j]=0;
    }

  int a1,a2;
  while(input)
    {
      input >> E >> M >> H >> k >> Hge >> kge >> Hbgo >> kbgo >> a1 >> a2;

      if(E>60000||M>60||H>60000||k>60)
	cout << "Error: " << E << " " << M  << " " << H << " " << k << " " << endl;

      Hint=(int)(rint(H/1000));
      if(H>0&&k>0)
	Hkmatrix[k][Hint]+=1;
      
      Hge_int=(int)(rint(Hge/1000)); 
      if(Hge>0&&kge>0)
	Hkge[kge][Hge_int]+=1;

      Hbgo_int=(int)(rint(Hbgo/1000));
      if(Hbgo>0&&kbgo>0)
	Hkbgo[kbgo][Hbgo_int]+=1;

      H=0;k=0;
      Hge=0;kge=0;
      Hbgo=0;kbgo=0;
    }
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  out << Hkmatrix[i][j] << "  ";
	  Hproj[j]+=Hkmatrix[i][j];	  
	}
      out << endl;
    }
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  outge << Hkge[i][j] << "  ";
	  Hge_p[j]+=Hkmatrix[i][j];
	}
      outge << endl;
    }
  
  for(j=0;j<60;j++)
    {
      for(i=0;i<60;i++)
	{
	  outbgo << Hkbgo[i][j] << "  ";
	  Hbgo_p[j]+=Hkmatrix[i][j];
	}
      outbgo << endl;
    }
  
  for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	{
	  kproj[i]+=Hkmatrix[i][j];
	}
    }
  
  for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	{
	  kge_p[i]+=Hkmatrix[i][j];
	}
    }
  
  for(i=0;i<60;i++)
    {
      for(j=0;j<60;j++)
	{
	  kbgo_p[i]+=Hkmatrix[i][j];
	}
    }

  for(i=0;i<60;i++)
    {
      projec << i << "\t" << Hproj[i] << "\t" << kproj[i] << "\t" 
	     << Hge_p[i] << "\t" << kge_p[i] << "\t" 
	     << Hbgo_p[i] << "\t" << kbgo_p[i] 
	     << endl; 
    }
  
}




