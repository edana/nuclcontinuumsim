/**
* @file /home/edana/gfnun/MySoftware2/Analysis/trunk/ReadEventFile.h
* @brief Reads the \c Gasp output file.2
*
* The \c Gasp output file has usually the following format: 2
**/
#ifndef READEVENTFILE_H
#define READEVENTFILE_H

/**
 * @class ReadEventFile
 * @brief Reads the \c Gasp output files
 *
 * This clas has two main functions they allows to read data from gamma and neutron interaction
 **/
class ReadEventFile
{
 public:
  ReadEventFile(ifstream&);
  void search_$(ifstream&);
  int EventAnalysis(ifstream&, ifstream&, ofstream&, ofstream&, double *, int *, int);
  int NeutronEventAnalysis(ifstream&, ofstream&, double *, int *, int);
  void histograming(ifstream&, ofstream&);
  
 private:
  char dato;  
  int det,number, det_num, Rnumber, gateFlag, nNum;
  double eDep, psi, theta, phi, energy_ref, gateH[40];  
  double H[120], Energy, EnergyT, Htotal,Hge,Hbgo,gate, sigma;
  int i,M,k,kge,kbgo,n;
  int Hmax1,Hmax2,Hmax3;
  int N1[2000];
  
};

#endif
