/******************************************************************************************//**
 * @file /home/edana/gfnun/MySoftware2/Analysis/trunk/main.cpp
 * @brief main file of the Analysis code
 *
 * MAIN file 
 */

#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<string>
using std::string;

#include<cstdlib>
#include "ReadEventFile.h"
#include "MatrixPrint.h"
#include "Region.h"


///The execution argument is the input file name *.inp  
main(int argc, char * argv[]) //! The input file is the \c Gamble output file.
{
  //Cheking the number of arguments 
  if(argc!=2)
    {
      cerr << "Error -- Use ./Analysis *.inp " << endl;
      exit(1);
    }

  cout << "Choose the analysis you want to make" << endl
       << "1. Make the EMHK file." << endl
       << "2. Make the Hk matrix and projections" << endl
       << "3. Make the Hk selection to build the IE matrix" << endl
       << "4. All the analysis." << endl;
  int type;
  cin >> type;

  ifstream IE("IE.dat", ios::in);

  int nFiles=0;
  if(type==1||type==4) {
    
    /**
     * The program ask. "How many neutron files do you want to include? " \n
     * It reads a entire number from 1 to 10
     */
    cout << "How many neutron files do you want to include? " << endl;
    cin >> nFiles;
    
    /// It is necesary to set the number of events in the code. 
    /// "Number of events (decaying nucleii)" \n
    cout << "Number of events (decaying nucleii)" << endl;
    int numEvnts=0;
    cin >> numEvnts;
    double *evntArrayH;
    int *evntArrayk;
    evntArrayH=new double [numEvnts];
    evntArrayk=new int [numEvnts];
    
    for(int i=0;i<numEvnts;i++) {
      evntArrayH[i]=0;
      evntArrayk[i]=0;
    }

    /**
     * The program ask. "How many GammaEvents.* files do you have?" \n
     * It reads a entire number from 1 to 6
     */
    int yesNum;
    cout << "How many GammaEvents.* files do you have?" << endl;
    cin >> yesNum;
    
    //Declaring the input files 
    cout << "Opening the input file " << argv[1] << endl; 
    ifstream input(argv[1], ios::in);
      
    //Opening the input file *.inp
    if(!input) {
      cerr << "Input error with the *.inp file \n";
      exit(1);
    }
    
    //Declaring the output files 
    ofstream EMHko("EMHk.dat", ios::out);
    if(!EMHko) {	
      cerr << "Error\n";
      exit(1);
    }
    //Declaring the output files 
    ofstream nEMHko("nEMHk.dat", ios::out);
    if(!nEMHko) {	
      cerr << "Error\n";
      exit(1);
    }
    
    ofstream EOS("EOS.dat", ios::out);
    ifstream EOSi("EOS.dat", ios::in);
    ofstream order("order.spc", ios::out);
    int ev=0;
    
    if(nFiles>0) {
      cout << "Opening the input file NeutronEvents.0000" << endl; 
      ifstream nevents("NeutronEvents.0000",ios::in);
      //Opening the input file NeutronEvents.0000
      if(!nevents) {
	cerr << "Input error with the NeutronEvents.0000 file\n";
	exit(1);
      }
     
      /// Now it use the NeutronEventAnalysis() function.
      ReadEventFile NeutronFile(nevents);
      NeutronFile.search_$(nevents);
      ev=NeutronFile.NeutronEventAnalysis(nevents, nEMHko, evntArrayH, evntArrayk, ev);
         
      if(nFiles>1) {
	cout << "Opening the input file NeutronEvents.0001" << endl; 
	ifstream nevents1("NeutronEvents.0001",ios::in);
	//Opening the input file NeutronEvents.0001
	if(!nevents1) {
	  cerr << "Input error with the NeutronEvents.0002 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile1(nevents1);
	NeutronFile1.search_$(nevents1);
	ev=NeutronFile1.NeutronEventAnalysis(nevents1, nEMHko, evntArrayH, evntArrayk, ev);
      }

      if(nFiles>2) {
	cout << "Opening the input file NeutronEvents.0002" << endl; 
	ifstream nevents2("NeutronEvents.0002",ios::in);
	//Opening the input file NeutronEvents.0002
	if(!nevents2) {
	  cerr << "Input error with the NeutronEvents.0002 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile2(nevents2);
	NeutronFile2.search_$(nevents2);
	ev=NeutronFile2.NeutronEventAnalysis(nevents2, nEMHko, evntArrayH, evntArrayk, ev);
      }

      if(nFiles>3) {
	cout << "Opening the input file NeutronEvents.0003" << endl; 
	ifstream nevents3("NeutronEvents.0003",ios::in);
	//Opening the input file NeutronEvents.0003
	if(!nevents3) {
	  cerr << "Input error with the NeutronEvents.0003 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile3(nevents3);
	NeutronFile3.search_$(nevents3);
	ev=NeutronFile3.NeutronEventAnalysis(nevents3, nEMHko, evntArrayH, evntArrayk, ev);
      }

      if(nFiles>4) {
	cout << "Opening the input file NeutronEvents.0004" << endl; 
	ifstream nevents4("NeutronEvents.0004",ios::in);
	//Opening the input file NeutronEvents.0004
	if(!nevents4) {
	  cerr << "Input error with the NeutronEvents.0004 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile4(nevents4);
	NeutronFile4.search_$(nevents4);
	ev=NeutronFile4.NeutronEventAnalysis(nevents4, nEMHko, evntArrayH, evntArrayk, ev);
      }
      if(nFiles>5) {
	cout << "Opening the input file NeutronEvents.0005" << endl; 
	ifstream nevents5("NeutronEvents.0005",ios::in);
	//Opening the input file NeutronEvents.0005
	if(!nevents5) {
	  cerr << "Input error with the NeutronEvents.0005 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile5(nevents5);
	NeutronFile5.search_$(nevents5);
	ev=NeutronFile5.NeutronEventAnalysis(nevents5, nEMHko, evntArrayH, evntArrayk, ev);
      }
      if(nFiles>6) {
	cout << "Opening the input file NeutronEvents.0006" << endl; 
	ifstream nevents6("NeutronEvents.0006",ios::in);
	//Opening the input file NeutronEvents.0006
	if(!nevents6) {
	  cerr << "Input error with the NeutronEvents.0006 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile6(nevents6);
	NeutronFile6.search_$(nevents6);
	ev=NeutronFile6.NeutronEventAnalysis(nevents6, nEMHko, evntArrayH, evntArrayk, ev);
      }
      if(nFiles>7) {
	cout << "Opening the input file NeutronEvents.0007" << endl; 
	ifstream nevents7("NeutronEvents.0007",ios::in);
	//Opening the input file NeutronEvents.0007
	if(!nevents7) {
	  cerr << "Input error with the NeutronEvents.0007 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile7(nevents7);
	NeutronFile7.search_$(nevents7);
	ev=NeutronFile7.NeutronEventAnalysis(nevents7, nEMHko, evntArrayH, evntArrayk, ev);
      }
      if(nFiles>8) {
	cout << "Opening the input file NeutronEvents.0008" << endl; 
	ifstream nevents8("NeutronEvents.0008",ios::in);
	//Opening the input file NeutronEvents.0008
	if(!nevents8) {
	  cerr << "Input error with the NeutronEvents.0008 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile8(nevents8);
	NeutronFile8.search_$(nevents8);
	ev=NeutronFile8.NeutronEventAnalysis(nevents8, nEMHko, evntArrayH, evntArrayk, ev);
      }
      if(nFiles>9) {
	cout << "Opening the input file NeutronEvents.0009" << endl; 
	ifstream nevents9("NeutronEvents.0009",ios::in);
	//Opening the input file NeutronEvents.0009
	if(!nevents9) {
	  cerr << "Input error with the NeutronEvents.0009 file\n";
	  exit(1);
	}
	ReadEventFile NeutronFile9(nevents9);
	NeutronFile9.search_$(nevents9);
	ev=NeutronFile9.NeutronEventAnalysis(nevents9, nEMHko, evntArrayH, evntArrayk, ev);
      }
    }
    ev=0;
    //  Opening the input file GammaEvents.0000
    cout << "Opening the input file GammaEvents.0000" << endl; 
    ifstream events("GammaEvents.0000",ios::in);
    if(!events) {
      cerr << "Input error with the GammaEvents.0000 file\n";
      exit(1);
    }
    ReadEventFile GammaFile(events);
    GammaFile.search_$(events);
    ev=GammaFile.EventAnalysis(events, input, EMHko, EOS, evntArrayH, evntArrayk, ev);
 
    if(yesNum>1) {
      cout << "Opening the input file GammaEvents.0001" << endl; 
      ifstream events01("GammaEvents.0001",ios::in);
      if(!events01) {
	cerr << "Input error with the GammaEvents.0001 file\n";
	exit(1);
      }  
      ReadEventFile GammaFile1(events01);
      GammaFile1.search_$(events01);
      ev=GammaFile1.EventAnalysis(events01, input, EMHko, EOS, evntArrayH, evntArrayk, ev);
    }
    if(yesNum>2) {
      cout << "Opening the input file GammaEvents.0002" << endl; 
      ifstream events02("GammaEvents.0002",ios::in);
      if(!events02) {
	cerr << "Input error with the GammaEvents.0002 file\n";
	exit(1);
      }  
      ReadEventFile GammaFile2(events02);
      GammaFile2.search_$(events02);
      ev=GammaFile2.EventAnalysis(events02, input, EMHko, EOS, evntArrayH, evntArrayk, ev);
    }
    if(yesNum>3) {
      cout << "Opening the input file GammaEvents.0003" << endl; 
      ifstream events03("GammaEvents.0003",ios::in);
      if(!events03) {
	cerr << "Input error with the GammaEvents.0003 file\n";
	exit(1);
      }  
      ReadEventFile GammaFile3(events03);
      GammaFile3.search_$(events03);
      ev=GammaFile3.EventAnalysis(events03, input, EMHko, EOS, evntArrayH, evntArrayk, ev);
    }
    if(yesNum>4) {
      cout << "Opening the input file GammaEvents.0004" << endl; 
      ifstream events04("GammaEvents.0004",ios::in);
      if(!events04) {
	cerr << "Input error with the GammaEvents.0004 file\n";
	exit(1);
      }  
      ReadEventFile GammaFile4(events04);
      GammaFile4.search_$(events04);
      ev=GammaFile4.EventAnalysis(events04, input, EMHko, EOS, evntArrayH, evntArrayk, ev);
    }
    if(yesNum>5) {
      cout << "Opening the input file GammaEvents.0005" << endl; 
      ifstream events05("GammaEvents.0005",ios::in);
      if(!events05) {
	cerr << "Input error with the GammaEvents.0005 file\n";
	exit(1);
      }  
      ReadEventFile GammaFile5(events05);
      GammaFile5.search_$(events05);
      ev=GammaFile5.EventAnalysis(events05, input, EMHko, EOS, evntArrayH, evntArrayk, ev);
    }
    GammaFile.histograming(EOSi,order);
  }
  
  ifstream EMHki("EMHk.dat", ios::in);
  
  if(!EMHki) {
    cerr << "Error\n";
    exit(1);
  }

  if(type==2||type==4) {
    ofstream Hk("Hk.mtx", ios::out);
    ofstream Hkge("Hkge.mtx", ios::out);
    ofstream Hkbgo("Hkbgo.mtx", ios::out);
    ofstream Hkproj("Hk.pro", ios::out);
    
    if(!Hk) {
      cerr << "Error\n";
      exit(1);
    }
    
    //Opening the output files
    if(!Hkproj) {
      cerr << "Error\n";
      exit(1);
    }
    MatrixPrint MatrixFile(EMHki,Hk,Hkge,Hkbgo,Hkproj);
    MatrixFile.print2D(EMHki,Hk,Hkge,Hkbgo,Hkproj);
  }
  
  if(type==3||type==4) {
    //cout << "Type the output file name" << endl;
    //char * name;
    //cin >> name;
    ifstream EMHk2("EMHk.dat", ios::in);
    ifstream EOSFile("EOS.dat", ios::in);
    ofstream EOSname("EOS.spc", ios::out);
    ofstream Fname("IE.mtx", ios::out);
    ofstream Pname("IE.pro", ios::out);
    
    RegionHk RegionAnalysis;
    RegionAnalysis.selectionHk(EMHk2,IE,EOSFile);
  }
  return 0;  
}


