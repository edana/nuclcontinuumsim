/**
 * @file /home/edana/gfnun/MySoftware2/Analysis/trunk/ReadEventFile.cpp
 * @brief Reads the \c Gasp output file.
 *
 * Essencially there are two types of files that can be read with this class, they are very similar but
 * are used to different interaction types: gammas and neutrons. The EventAnalysis() function is used
 * for the gamma interaction files and the NeutronEventAnalysis() does the same for the neutron interaction
 * files.
 */
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>

#include "ReadEventFile.h"

ReadEventFile::ReadEventFile(ifstream& EventFile)
{
}

/// Finds the simbol $ in the input file, this flag is used to find the begining of the data. After 
/// the $ character is found the variable is turned to 0. 
void ReadEventFile::search_$(ifstream& EventFile)
{
  while (dato!='$')
    {
      EventFile >> dato;
    }
  dato='0';
}

/// This function reads the gamma interaction line by line from the files GammaEvents.*
/// The \c Gasp output file has usually the following format:
/// \image html /home/edana/gfnun/Doc/Thesis1/Figures/ver.jpg "The format of the output file"  
/// \image latex /home/edana/gfnun/Doc/Thesis1/Figures/ver.pdf "The format of the output file" width=12cm 
/// Each \f$\gamma\f$-event begins with a line that have the information about the emitted gamma, it starts 
/// with -1 and has the energy, position and a number that identify the \f$\gamma\f$-event. The input
/// file must be organized by cascade events, where the the \f$\gamma\f$-rays of the same cascade are 
/// sorted by they energy. This is necessary because the code calculates the energy difference of two 
/// concecutive \f$\gamma\f$-rays, if this difference is negative it means that it was a cascade change.\n
/// For each gamma interaction the file contains the energy left in the Ge, BGO detectors or the shell,
/// then two arrays are builded independently for k and H and each detector. 
/// It is possible to make a selection of the higher or lower gamma energies to make an extra analysis of 
/// the gamma rays in the cascades.  The number of \f$\gamma\f$-rays higher than 2 MeV is printed to the 
/// console as well as the number of \f$\gamma\f$-rays between 50-310 keV.
/// It is passible also to make a gate selection in the energy, to build the H and k arrays with the selected 
/// events, but this  should be uncommented in the file. 
int ReadEventFile::EventAnalysis(ifstream& EventFile, ifstream& RealEvent, ofstream& Output, ofstream& Nfile, double *ArrayH, int *Arrayk, int ev)
{    
  int numHighEner=0, numLowEner=0;
  energy_ref=999999, gateFlag=0, gate=0, sigma=0; 
  EnergyT=0; Htotal=0; Hge=0; Hbgo=0,Hmax1=0;
  i=0; M=0; k=0; kge=0; kbgo=0;
  
  for(i=0;i<120;i++)
    H[i]=0;

  for(i=0;i<40;i++)
    gateH[i]=0;
  
  while(EventFile)
    {
      EventFile >> det >> eDep >> psi >> theta >> phi >> number;

      if(det==-1) {//end of gamma interaction
	RealEvent >> Rnumber >> Energy; //Reading the total energy for the input file 
	  
	//Making the Hk 
	for(i=0;i<120;i++) {
	  if(H[i]>0) {
	    k+=1;
	    Htotal+=H[i];
	    //For Ge detectors
	    if (i<40) {
	      kge+=1;
	      Hge+=H[i];
	    }
	    //For BGO detectors
	    else {
	      kbgo+=1;
	      Hbgo+=H[i];
	    }
	    if(Hmax1<H[i]) 
	      Hmax1=(int)(rint(H[i]));
	  }
	}

	//Selection of higher energies
	for(i=0;i<120;i++) { 
	  if(gateH[i]>2000)
	    numHighEner++;
	}

	//Selection of lower energies
	for(i=0;i<120;i++) { 
	  if(gateH[i]<310&&gateH[i]>50)
	    numLowEner++;
	}
	//Gate selection
	/*for(i=0;i<40;i++) { 
	//Set the resolution
	sigma=1.07*sqrt(gateH[i])/2.3548;
	if(fabs(gateH[i]-334)<sigma)//GATE at 334keV 
	gateFlag=1; 
	if(fabs(gateH[i]-412)<sigma)//GATE at 412keV 
	gateFlag=1; 
	if(fabs(gateH[i]-477)<2*sigma)//GATE at 477keV 
	gateFlag=1; 
	if(fabs(gateH[i]-524)<2*sigma)//GATE at 524keV 
	gateFlag=1; 
	if(fabs(gateH[i]-557)<2*sigma)//GATE at 557keV 
	gateFlag=1; 
	if(fabs(gateH[i]-588)<2*sigma)//GATE at 588keV 
	gateFlag=1;  
	}*/
	
	if(energy_ref<Energy) { //Cascade end 
	  //if(gateFlag==1&&M>0) {
	  Output << EnergyT << "  " << M << "\t"
		 << ArrayH[ev-1]+Htotal << "  " << Arrayk[ev-1]+k << "\t"
		 << Hge << "  " << kge << "\t"
		 << Hbgo << "  " << kbgo << "\t"
		 << numHighEner << "  " << numLowEner << endl;

	  cout << number << "\r";	  

	  Nfile << Hmax1 << endl;
	  Hmax1=0;
	  //}
	  gateFlag=0;
	  M=0;
	  EnergyT=0;
	  k=0; kge=0; kbgo=0;
	  Htotal=0; Hge=0; Hbgo=0;
	  numHighEner=0;
	  numLowEner=0;

	  for(i=0;i<120;i++)
	    H[i]=0;
	  for(i=0;i<40;i++)
	    gateH[i]=0;
	  ev++;
	  n=0;
	}
	
	M++;//Adding the multiplicity
	EnergyT+=Energy; //Adding the real total energy
	//cout << EnergyT << " " << Energy << endl;
	energy_ref=Energy;
	
	n++;
	for(i=0;i<120;i++)
	  H[i]=0;
	
	for(i=0;i<120;i++)
	  gateH[i]=0;
      }
      else if(det>1000&&det<3000) {
	//For Ge detectors
	if (det<2000) {
	  det_num=det-1001;
	  gateH[det_num]+=eDep; //To identify monoenergetic gammas to gate in them
	}
	//For BGO detectors
	else { 
	  det_num=det-2000+40;		
	  gateH[det_num]+eDep;
	}
	H[det_num]+=eDep;
      }
    }
  //if(gateFlag==1&&M>0)
  Output << EnergyT << "  " << M << "\t"
	 << ArrayH[ev-1]+Htotal << "  " << Arrayk[ev-1]+k << "\t"
	 << Hge << "  " << kge << "\t"
	 << Hbgo << "  " << kbgo << "\t"
	 << numHighEner << "  " << numLowEner << endl;

  cout << number << "\r";	  

  Nfile << Hmax1 << endl;
  Hmax1=0;
  numHighEner=0;
  numLowEner=0;
  cout << endl;
  return ev;
}

/// Reads the neuton interaction one by one from the files NeutronEvents.* The format of the file is the 
/// same as the gamma files, but the neutron event initiallzation flag is -2. For each event, four neutrons
/// are emitted and four flags are counted to fill the k and H arrays.  
int ReadEventFile::NeutronEventAnalysis(ifstream& EventFile, ofstream& Output, double *ArrayH, int *Arrayk, int j)
{    
  Htotal=0; Hge=0; Hbgo=0;
  i=0; M=0; k=0; kge=0; kbgo=0, nNum=0;
  
  for(i=0;i<120;i++)
    H[i]=0;
  
  while(EventFile)
    {
      EventFile >> det >> eDep >> psi >> theta >> phi >> number;
      if(det==-2) { //end of neutron interaction
	//Making the Hk 
	for(i=0;i<120;i++) {
	  if(H[i]>0) {
	    k+=1;
	    Htotal+=H[i];
	    //For Ge detectors
	    if (i<40) {
	      kge+=1;
	      Hge+=H[i];
	    }
	    //For BGO detectors
	    else {
	      kbgo+=1;
	      Hbgo+=H[i];
	    }
	  }
	}
	
	nNum++;
	
	if(nNum==4) { //Four neutrons
	  if(Htotal>0) {
	    Output << Htotal << "  " << k << "\t"
		   << Hge << "  " << kge << "\t"
		   << Hbgo << "  " << kbgo << endl;
	    ArrayH[j]=Htotal;
	    Arrayk[j]=k;
	    j++;
	  }
	  k=0; kge=0; kbgo=0;
	  Htotal=0; Hge=0; Hbgo=0;
	  n=0;
	  nNum=0;
	}
	for(i=0;i<120;i++)
	  H[i]=0;
      }
      else if(det>1000&&det<3000) {
	//For Ge detectors
	if (det<2000) 
	  det_num=det-1001;
	//For BGO detectors
	else 
	  det_num=det-2000+40;		
	H[det_num]+=eDep;
      }
    }
  if(Htotal>0) {
    Output << Htotal << "  " << k << "\t"
	   << Hge << "  " << kge << "\t"
	   << Hbgo << "  " << kbgo << endl;
    ArrayH[j]=Htotal;
    Arrayk[j]=k;
    j++;
  }
  return j;
}

/// Construct a histogram from a set of data in a file and write it to a file.
void ReadEventFile::histograming(ifstream& Nfile, ofstream& EOSfile)
{
  for(i=1;i<2000;i++)
    N1[i]=0;

  while(Nfile) {  
    Nfile >> Hmax1 ;
    
    Hmax1=int(floor(((Hmax1/10)+0.5)));
    if(Hmax1>2000) {
      cout << "Error the energy is out of the spectrum > 20Mev" << endl;
      cout << Hmax1*10 << "(keV)" << endl;
      //exit(1);
    }
    
    N1[Hmax1]++;
    
  }

  //Efficiency correction 
  float N1_ceff[2000], Eff=0;
  for(i=0;i<2000;i++) 
    N1_ceff[i]=0;
  
  for(i=1;i<2000;i++) {
    if(i<=200) 
      Eff=(-0.075*i/100)+0.81;
    else 
      Eff=(0.002*i/100)+0.64;
    N1_ceff[i]=N1[i]/Eff;
  }
  
  for(i=1;i<2000;i++)
    EOSfile << i*10 << "\t" << N1_ceff[i] << endl;
  
}
