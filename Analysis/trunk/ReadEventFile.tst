/************/
#include<iostream>
using std::cerr;
using std::cout;
using std::cin;
using std::endl;
using std::ios;

#include<fstream>
using std::ifstream;
using std::ofstream;

#include<cstdlib>
#include<cmath>

#include "ReadEventFile.h"

ReadEventFile::ReadEventFile(ifstream& EventFile)
{
}

void ReadEventFile::search_$(ifstream& EventFile)
{
  while (dato!='$') {
    EventFile >> dato;
  }
  dato='0';
}

//Read the events
int ReadEventFile::EventAnalysis(ifstream& EventFile, ifstream& RealEvent, ofstream& Output, ofstream& Nfile, double *ArrayH, int *Arrayk, int ev)
{    
 
  energy_ref=999999, gateFlag=0, gate=0, sigma=0;
  EnergyT=0; Htotal=0; Hge=0; Hbgo=0,Hmax1=0;
  i=0; M=0; k=0; kge=0; kbgo=0;
  
  for(i=0;i<120;i++)
    H[i]=0;
  
 for(i=0;i<40;i++)
    gateH[i]=0;
  
  while(EventFile)
    {
      EventFile >> det >> eDep >> psi >> theta >> phi >> number;
      
      if(det==-1) {//end of gamma interaction
	RealEvent >> Rnumber >> Energy; //Reading the total energy for the input file 
	
	//Making the Hk 
	for(i=0;i<120;i++) {
	  if(H[i]>0) {
	    k+=1;
	    Htotal+=H[i];
	    //For Ge detectors
	    if (i<40) {
	      kge+=1;
	      Hge+=H[i];
	    }
	    //For BGO detectors
	    else {
	      kbgo+=1;
	      Hbgo+=H[i];
	    }
	    if(Hmax1<H[i]) 
	      Hmax1=(int)(rint(H[i]));
	  }
	}
	
	//Gate selection
	/*for(i=0;i<40;i++) { 
	//Set the resolution
	sigma=1.07*sqrt(gateH[i])/2.3548;
	if(fabs(gateH[i]-334)<sigma)//GATE at 334keV 
	gateFlag=1; 
	if(fabs(gateH[i]-412)<sigma)//GATE at 412keV 
	gateFlag=1; 
	if(fabs(gateH[i]-477)<2*sigma)//GATE at 477keV 
	gateFlag=1; 
	if(fabs(gateH[i]-524)<2*sigma)//GATE at 524keV 
	gateFlag=1; 
	if(fabs(gateH[i]-557)<2*sigma)//GATE at 557keV 
	gateFlag=1; 
	if(fabs(gateH[i]-588)<2*sigma)//GATE at 588keV 
	gateFlag=1;  
	}*/

	if(energy_ref<Energy) //Cascade end
	    {
	      //if(gateFlag==1&&M>0) {
	      Output << EnergyT << "\t" << M << "\t"
		    << ArrayH[ev-1]+Htotal << "  " << Arrayk[ev-1]+k << "\t"
		     << Hge << "\t" << kge << "\t"
		     << Hbgo << " \t" << kbgo << endl;
	      
	      Nfile << Hmax1 << endl;
	      Hmax1=0;
	      //}	      
	      gateFlag=0;
	      M=0;
	      EnergyT=0;
	      k=0; kge=0; kbgo=0;
	      Htotal=0; Hge=0; Hbgo=0;
	      
	      for(i=0;i<120;i++)
		H[i]=0;
	      for(i=0;i<40;i++)
		gateH[i]=0;
	      ev++;
	      n=0;
	    }

	  M++;//Adding the multiplicity
	  EnergyT+=Energy; //Adding the real total energy
	  //cout << EnergyT << " " << Energy << endl;
	  energy_ref=Energy;
	 
	  n++;
	  for(i=0;i<120;i++)
	    H[i]=0;
	  
	  for(i=0;i<40;i++)
	    gateH[i]=0;
	}
      else if(det>1000&&det<3000) {
	//For Ge detectors
	if (det<2000) {
	  det_num=det-1001;
	  gateH[det_num]+=eDep; //To identify monoenergetic gammas to gate in them
	}
	//For BGO detectors
	else
	  det_num=det-2000+40;		
	H[det_num]+=eDep;
	
      }
    }
  //if(gateFlag==1&&M>0)
  Output << EnergyT << "\t" << M << "\t"
	 << ArrayH[ev-1]+Htotal << "  " << Arrayk[ev-1]+k << "\t"
	 << Hge << "\t" << kge << "\t"
	 << Hbgo << " \t" << kbgo << endl;
  
  Nfile << Hmax1 << endl;
  Hmax1=0;
  
  return ev;
}

int ReadEventFile::NeutronEventAnalysis(ifstream& EventFile, ofstream& Output, double *ArrayH, int *Arrayk, int j)
{ 
  Htotal=0; Hge=0; Hbgo=0;
  i=0; M=0; k=0; kge=0; kbgo=0, nNum=0;
  
  for(i=0;i<120;i++)
    H[i]=0;
  
  while(EventFile)
    {
      EventFile >> det >> eDep >> psi >> theta >> phi >> number;
      if(det==-2) { //end of neutron interaction
	//Making the Hk 
	for(i=0;i<120;i++) {
	  if(H[i]>0) {
	    k+=1;
	    Htotal+=H[i];
	    //For Ge detectors
	    if (i<40) {
	      kge+=1;
	      Hge+=H[i];
	    }
	    //For BGO detectors
	    else {
	      kbgo+=1;
	      Hbgo+=H[i];
	    }
	  }
	}
	
	if(nNum==6) { //Four neutrons
	  if(Htotal>0) {
	    Output << Htotal << "  " << k << "\t"
		   << Hge << "  " << kge << "\t"
		   << Hbgo << "  " << kbgo << endl;
	    ArrayH[j]=Htotal;
	    Arrayk[j]=k;
	    j++;
	  }
	  k=0; kge=0; kbgo=0;
	  Htotal=0; Hge=0; Hbgo=0;
	  n=0;
	  nNum=0;
	}

	nNum++;
	for(i=0;i<120;i++)
	  H[i]=0;
      }
      else if(det>1000&&det<3000) {
	//For Ge detectors
	if (det<2000) 
	  det_num=det-1001;
	//For BGO detectors
	else 
	  det_num=det-2000+40;		
	H[det_num]+=eDep;
      }
    }
  if(Htotal>0) {
    Output << Htotal << "  " << k << "\t"
	   << Hge << "  " << kge << "\t"
	   << Hbgo << "  " << kbgo << endl;
    ArrayH[j]=Htotal;
    Arrayk[j]=k;
    j++;
  }
  return j;
}

void ReadEventFile::histograming(ifstream& Nfile, ofstream& EOSfile)
{
  for(i=1;i<2000;i++)
    N1[i]=0;
  
  while(Nfile) {  
    Nfile >> Hmax1
      
      Hmax1=int(floor(((Hmax1/10)+0.5)));
    if(Hmax1>2000) {
      cout << "Error the energy is out of the spectrum > 20Mev" << endl;
      cout << Hmax1*10 << "(keV)" << endl;
      //exit(1);
    }
    
    N1[Hmax1]++;
  }
  
  for(i=1;i<200;i++)
    EOSfile << i*100 << "\t" << N1[i] << endl;
}
