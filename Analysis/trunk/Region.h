/**
* @file /home/edana/gfnun/MySoftware2/Analysis/trunk/Region.h
**/
#ifndef REGIONHK_H
#define REGIONHK_H

/**
 * @class RegionHk
 * @brief Makes the (I,E) selection
 *
 **/
class RegionHK
{
 public:
  Region();

  void selectionHk(ifstream&, ifstream&, ifstream&);

 private:
  double EnergyT, Htotal,Hge, Hbgo, E, dH, H;
  int M, ktotal, kge, kbgo, I, dk, k;
  int IE[60][60], IEge[60][60], IEbgo[60][60], Eint, i, j;
  double Eproj[60], Iproj[60],Imax, Emax;
  double Ege_proj[60], Ige_proj[60],Ige_max, Ege_max;
  double Ebgo_proj[60], Ibgo_proj[60],Ibgo_max, Ebgo_max;
  double sumE, meanE, varE1, varE, skewE1, skewE, kurtE1 ,kurtE;
  double sumI, meanI, varI1, varI, skewI1, skewI, kurtI1 ,kurtI;
  int arrayE[100000], arrayI[100000], N1[2000];
};

#endif
