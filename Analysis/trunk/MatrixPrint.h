/**
* @file /home/edana/gfnun/MySoftware2/Analysis/trunk/MatrixPrint.h
**/
#ifndef MATRIXPRINT_H
#define MATRIXPRINT_H

/**
 * @class MatrixPrint
 * @brief Build and print te Hk matrix to a file
 *
 **/
class MatrixPrint
{
 public:
  MatrixPrint(ifstream&, ofstream&, ofstream&, ofstream&, ofstream&);

  void print2D(ifstream&, ofstream&, ofstream&, ofstream&, ofstream&);
  // void 1Dprint(ifstream& );
  
 private:
  double E,H,Hge,Hbgo;
  int i,j,M,k,kge,kbgo,Hint,Hge_int,Hbgo_int;
  int Hkmatrix[60][60], Hkge[60][60], Hkbgo[60][60];
  int Hproj[60], kproj[60],Hge_p[60], kge_p[60],Hbgo_p[60], kbgo_p[60];
};

#endif
